<?php
require_once "vendor/autoload.php";
$app = require __DIR__ . '/tests/app/bootstrap/app.php';
$app->make(\Illuminate\Foundation\Console\Kernel::class)->bootstrap();