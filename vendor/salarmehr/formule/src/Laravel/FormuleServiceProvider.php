<?php

namespace Formule\Laravel;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class FormuleServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'formule');

    // SEOTools
    $this->app->register(\Artesaos\SEOTools\Providers\SEOToolsServiceProvider::class);

    $loader = \Illuminate\Foundation\AliasLoader::getInstance();
    $loader->alias('SEO', \Artesaos\SEOTools\Facades\SEOTools::class);

    $this->publishes([
      __DIR__ . '/../../resources/config/formule.php' => config_path('formule.php'),
      __DIR__ . '/../../resources/composer.json'      => base_path('composer.json'),
    ]);

    $this->publishes([
      __DIR__ . '/../../resources/assets/' => public_path('vendor/formule'),
    ], 'assets');

    $this->publishes([
      __DIR__ . '/../../resources/init/' => base_path(),
    ], 'init');

    $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'formule');

    $this->publishes([
      __DIR__ . '/../../resources/lang' => resource_path('lang/vendor/formule'),
    ], 'lang');

    $this->loadMigrationsFrom(__DIR__ . '/../../resources/migrations');

    if ($this->app->runningInConsole()) {
      $this->commands([
        Commands\CreateDatabase::class,
        Commands\RefreshViews::class,
      ]);
    }

    $this->blade();
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {

  }

  private function blade()
  {
    Blade::directive('dd', function ($expression) {
      return "<?php dd($expression); ?>";
    });

    Blade::directive('dump', function ($expression) {
      return "<?php dump($expression); ?>";
    });

    Blade::directive('debug', function ($expression) {
      return "<?php xdebug_break(); ?>";
    });

    Blade::directive('css', function ($expression) {
      return <<<eos
            <?php \$__env->startPush('styles'); ?>
              <link rel="stylesheet" href="<?php echo e(asset({$expression})); ?>"/>
            <?php \$__env->stopPush(); ?>
eos;
    });

    Blade::directive('js', function ($expression) {
      return <<<eos
            <?php \$__env->startPush('scripts'); ?>
              <script src="<?php echo e(asset({$expression})); ?>"></script>
            <?php \$__env->stopPush(); ?>
eos;
    });

    Blade::directive('vendor', function ($expression) {
      return <<<eos
            <?php \$__env->startPush('vendor'); ?>
              <script src="<?php echo e(bower({$expression})); ?>"></script>
            <?php \$__env->stopPush(); ?>
eos;
    });
  }
}
