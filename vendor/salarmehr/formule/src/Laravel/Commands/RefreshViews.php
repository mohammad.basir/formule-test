<?php

namespace Formule\Laravel\Commands;

use Illuminate\Console\Command;

class RefreshViews extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'db:refreshViews';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    try {
      $sql = file_get_contents(database_path('migrations/views.sql'));

      return \DB::unprepared($sql);
    } catch (\PDOException $e) {
      die($e->getMessage());
    };
  }
}
