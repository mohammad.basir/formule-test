<?php

namespace Formule\Laravel\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateDatabase extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'db:create {--d|database=} {--rm}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $database = $this->option('database');
    if (!$database) {
      $database = mb_strtolower(str_random(5));
    }
    $this->info('The new database name:' . $database);
    $result = \Illuminate\Support\Facades\DB::statement("CREATE DATABASE IF NOT EXISTS `$database` /*!40100 DEFAULT CHARACTER SET utf8mb4  COLLATE utf8mb4_unicode_ci */;");

    $config = [
      'driver'    => 'mysql',
      'charset'   => 'utf8mb4',
      'collation' => "utf8mb4_unicode_ci",
      'prefix'    => '',
      'strict'    => false,
      'database'  => $database,
      'username'  => config('database.username'),
      'password'  => config('database.password'),
      'host'      => config('database.host'),
    ];

    app()->make('config')->set('database.connections.' . $database, $config);
    $this->call('migrate', ['--database' => $database]);

    if ($this->option('rm')) {
      DB::statement('drop database ' . $database);
      $this->info('The database is removed:' . $database);
    }
  }
}
