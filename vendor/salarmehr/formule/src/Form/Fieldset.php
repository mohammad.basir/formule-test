<?php
namespace Formule\Form;


class Fieldset extends Container
{
  public function __construct($field)
  {
    parent::__construct($field);
  }

  public function render()
  {
    //todo why do I have to place this here, placing this in view cause repetition of field type according to the first item
    $this->itemsHtml = $this->form->subRender($this);
    if ($this->itemsHtml) {
      return view('formule::fieldset', ['field' => $this])->render();
    }
  }
}