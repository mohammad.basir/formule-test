<?php namespace Formule\Form;


class Help extends Html
{
    public function render()
    {
        return view('formule::help', ['field' => $this])->render();
    }
}