<?php namespace Formule\Form;


class Textarea extends Field
{
  public function __construct($field)
  {
    parent::__construct($field);
  }

  public function render()
  {
    return view('formule::textarea', ['field' => $this])->render();
  }

  public function renderCell($value = null)
  {
    $val = $value ?: $this->value;

    return \f\str::ellipsis(strip_tags($val), 150);

  }

  public function renderFilter()
  {
    $this->type = 'text';

    $field = field($this);
    $field->setName($this->name);

    return $field->render();
  }

}
