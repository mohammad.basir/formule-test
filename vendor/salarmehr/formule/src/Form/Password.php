<?php
namespace Formule\Form;


class Password extends Text
{
  public function __construct($field)
  {
    parent::__construct($field);
    $this->gridable = false;
    $this->searchable = false;
  }

  public function render()
  {
    return parent::render();
  }
}