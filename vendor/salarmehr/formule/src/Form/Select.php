<?php namespace Formule\Form;

class Select extends Field
{

  public function __construct($field)
  {
    if ($field->mode == 'filter') {
      $field->multiple = true;
    }
    parent::__construct($field);

    $this['items'] = $this['items'] ?? [];

    if (count($this['items'])) {
      $this->validator[] = "in:" . Form::inValidator($this);
      if ($this->multiple) {
        $this->validator[] = 'array';
      }
      //      $this->validator[] = "in:" . Form::inValidator(array_filter($this['items'], function ($key) {
      //          return $key != ''; // filtering نامشخص items
      //        }, ARRAY_FILTER_USE_KEY));
      //    }

      if ($this->simple) {
        $this['items'] = array_combine($this['items'], $this['items']);
      }

      // adding an empty item if emptyItem property is not set or is set to a truly value and field is not multiple
      if (!$this->has('emptyItem') && ($this->emptyItem !== false) && !$this->multiple && !in_array('', array_keys($this['items'])) && $this->type == 'select') {
        $this['items'] = array_prepend($this['items'], '', '');
      }

      if ($this->rich || count($this['items']) > 20) {
        $this->cls[] = 'rich';
      }
      if ($this->except == 'this') {
        $this->except = $this->id;
      }
    }
  }

  public function renderFilter()
  {
    if ($this->multiple === null) {
      $this->attrs['multiple'] = true;
      $this->attrs['name'] = $this->attrs['name'] . '[]';
      $this->multiple = true;
    }

    return $this->render();
  }

  public function render()
  {
    return view('formule::select', ['field' => $this])->render();
  }

  public function renderCell($value = null)
  {
    $results = [];
    $values = (array)($value !== null ? $value : $this->value);

    foreach ($values as $value) {
      $results[] = $this->renderCellSingle($value);
    }

    return implode($results, '، ');
    //    return $this->renderValue($value, true);
  }

  public function renderValue($value = null, $cell = false)
  {
    $results = [];
    $values = (array)($value !== null ? $value : $this->value);

    foreach ($values as $value) {
      $results[] = $this->renderValueSingle($value);
    }

    return implode($results, '، ');
  }

  protected function renderValueSingle($value)
  {
    if (isset($this['items'][$value])) {
      return $this['items'][$value];
    }
  }

}