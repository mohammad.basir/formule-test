<?php namespace Formule\Form;


class Radio extends Select
{

  public function __construct($field)
  {
    parent::__construct($field);
    if (count($this['items'])) {
      $this->validator[] = "in:" . Form::inValidator($this);
    }
  }

  public function render()
  {
    //    if (is_array($this->value)) {
    //      $this->value = $this->get('value')[0];
    //    }
    //    //??
    //    $val = $this->get('value') !== null ? $this->get('value') : $this->default;
    //    ~r($this->all());
    return view('formule::radio', ['field' => $this])->render();
  }

  public function renderFilter()
  {
    $this->type = 'select';
    $select = field($this->all());
    //setting id and name of select field not to use prefix twice
    $select->setName($this->attrs['name']);
    //when the items has prefix and a new instance is created from that the id will be duplicated.
    $select->id = $this->id;

    return $select->renderFilter();
  }

  //  public function renderValue($value = null)
  //  {
  //    $val = $value ?: $this->value;
  //    if ($val && isset($this['items'][$val])) {
  //      return $this['items'][$val];
  //    }
  //  }

}