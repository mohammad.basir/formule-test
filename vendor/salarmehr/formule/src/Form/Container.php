<?php
namespace Formule\Form;

abstract class Container extends Control
{
  public function __construct($field)
  {
    parent::__construct($field);
  }

  // not used yet!
  public function getInheritable()
  {
    return array_only($this->all(), ['prefix', 'searchable', 'sortable', 'gridable']);
  }

  //todo-said : check this
  /**
   * for skipping empty Containers
   * @return bool
   */
  public function isVisible()
  {
    //در صورتی که فیلد کانتینر باشه
    if ($this->isContainer() && $this->isEmpty()) {
      //بدست آوردن فیلدها از روی آیتم های کانتینر -> اینجا اشکال داره
      $fields = form($this->items)->subrender(ary($this->items));
      foreach ($fields->items as $field) {
        //اگه کانتینره تابع برمیگرده به خودش
        if ($field->isContainer() && $field->isEmpty()) {
          return $field->isVisible();
        }
        else {
          //اگه کانتیر نیست چک میشه اگه آیتماش تو ویزیبل نبود فالس بر می گردو برعکس
          $flat = $fields->flat();
          if (!in_array(array_keys($flat), $this->form->visible)) {
            return false;
          }
          else {
            return true;
          }
        }
      }
    }
  }
} 