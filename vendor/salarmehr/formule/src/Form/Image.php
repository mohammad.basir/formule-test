<?php namespace Formule\Form;

class Image extends File
{
  public function __construct($field)
  {
    parent::__construct($field);
    $this->image = true;
    $this->validator[] = 'photo';
  }

  public function render()
  {
    $this->accept = $this->get('accept', '.png,.gif,.jpg,.jpeg,.bmp');

    return parent::render();
  }
}