<?php

namespace Formule\Form;

class Date extends Field
{
  public $format = '%Y/%m/%d';
  public $timeCfg = 'false';
  public $icon = 'fa-calendar';

  public function __construct($field)
  {
    parent::__construct($field);
    $this->cls[] = 'date';
    $this->cls[] = 'input-group';
    $this['validator'][] = 'lifetime';
    $this->label = $this->label ?: __('formule::word.date');
  }

  public function renderFilter()
  {
    $minField = clone $this;
    $minField->cls[] = 'col-sm-3';
    //    $minField->value = @$this->value['min'];
    $minField->value = @$this->data->get("$this->id.min");
    $minField->label = '';
    $minField->setName($this->name . '.min');
    $minField->id = $this->id . '.min';

    $min = $minField->render();

    $maxField = clone $minField;
    //    $maxField->value = @$this->value['max'];
    $maxField->value = @$this->data->get("$this->id.max");

    $maxField->setName($this->name . '.max');
    $maxField->id = $this->id . '.max';
    $max = $maxField->render();

    return view('formule::range', ['field' => $this] + compact('min', 'max'))->render();
  }

  public function render()
  {
    return view('formule::date', ['field' => $this])->render();
  }

  public function renderValue($value = null)
  {
    $val = $value ?: $this->value;
    if ($val) {
      return region('IR') ? toSolar($val) : $val;
    }
  }

  public function formatCell($value = null)
  {
    $val = $value ?: $this->value;
    if ($val) {
      return solarTag($val);
    }
  }

}