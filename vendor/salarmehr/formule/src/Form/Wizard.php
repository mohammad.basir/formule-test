<?php
namespace Formule\Form;

class Wizard extends Tabset
{
  static public $tabId = 0;

  protected function renderView()
  {
    return view('formule::wizard', ['field' => $this])->render();
  }
}