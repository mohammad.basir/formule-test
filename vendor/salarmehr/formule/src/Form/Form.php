<?php namespace Formule\Form;

use Formule\Traits\ControllerTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Salarmehr\Ary;

class Form
{
  // rendering modes
  const DISPLAY = 'display';
  const EDIT = 'edit';
  const FILTER = 'filter';
  const QUERY = 'filter';

  // these form params are casted to children containers. refer to Control class documentation.
  static public $inheritables = [
    'data',
    'form',
    'flat',
    'formable',
    'grid',
    'gridable',
    'labelPrefix',
    'mode',
    'model',
    'prefix',
    'readonly',
    'ng-disabled',
  ];
  public $display = 'page'; // page,inline,print
  public $backTitle;
  public $editUrl;

  private $errors = [];
  private $flat = false;
  private $grid = false;

  public $action;
  public $buttons = [];
  public $caption;
  public $class;// form level css class
  public $data = [];
  public $destroyable = true;
  public $generalInfo = true;
  public $hideNullFields = false;
  public $layout;
  public $method;
  public $oldData = [];
  public $printable;
  public $removable;
  public $requiredFields = [];
  public $submitable = true;
  public $submitIcon;
  public $submitTitle;
  public $title;

  /** @var string Back URL if user a use click on cancel button */
  public $backUrl;

  /** @var mixed|null  Only used to add initial values to data property, if the form has `File` control, the model will be used to fetch the corresponding File model */
  public $model = null;

  /*
   * mass items attributes: an array of items' ID what will make affect the corresponding items visiblity and
   * ediiablility editable and readonly are mutual inclusive and can not user together visible and editable: null means
   * all forms items are editable unless `readonly` property is truthy on the item
   * at item level you only use hidden and readonly as all items are visible and ediable by default
   */
  public $visible;
  public $editable;
  public $readonly;
  public $hidden;

  /**
   * A class to render and validate forms (edit or search)
   *
   * @param array  $items  a multi-dimensional array denoting the form hierarchical structure.
   * @param array  $params Form specific options
   *                       ? $model the value that is used to fill the form
   *                       string $cls the css class that is added to form top tag
   *                       string $layout overwrite the layout that form is placed in
   *                       string $submitTitle title of submit button e.g. "Register" (default equal to
   *                       formule::word.submit translation) string $submitIcon  font awesome icon fo submit button e.g
   *                       "fa-pencil" (default null) boolean $submitable make submit button invisible is set to false
   *                       (default true) bool $removable showing delete button (default true) bool $generalInfo
   *                       پارامتری برای نمایش دادن یا نمایش ندادن جنرال اینفو array $buttons additional buttons at the
   *                       button bar of the form array $requiredFields     در مورد فیلدهایی که وضعیت اجباری بودنشان
   *                       صریحا مشخص نشده است اگر شناسشان در requiredFields باشد اجباری خواهند شد. array $editable
   *                       editable is the opposite of readonly attribute. چک کردن فیلدهایی که قابل مشاهده یا قابل
   *                       ویرایش هستند. به شکل پیش فرض همه فیلدها دو قابلیت را دارند. array $visible
   *
   * @param string $mode   'filter'|'edit'|'display'|'query'
   * @throws \Exception
   * @see  \Formule\Form\Control class from docs
   */
  public function __construct($items = [], $params = [], $mode = self::EDIT)
  {

    if (!method_exists(app(), 'runningInConsole')) {
      exception('
         You need a bootstrapped Laravel service container before using Formule.
         e.g. `(require __DIR__ . \'/../bootstrap/app.php\')->make(\Illuminate\Foundation\Console\Kernel::class)->bootstrap();`
      ');
    }

    $this->setOldData();

    if (is_string($items)) {
      $items = self::getItems($items, $params, $mode);
    }

    // all public properties of form are assumed to be configurable via params argument
    $properties = getProperties($this);

    foreach ($params as $param => $value) {
      if (!in_array($param, $properties)) {
        trigger_error("'$param' is not a valid config of the form class");
      }
      $this->{$param} = $value;
    }

    if ($mode == self::DISPLAY) {
      $this->mode = self::EDIT;
      $this->readonly = true;
    }

    $this->items = $items;
    $this->mode = $mode;
    $this->data = $this->processData();

    if (!app()->runningInConsole() && session('errors')) {
      $this->errors = session('errors')->getMessages();
    }
  }

  /**
   * Preparing form data by merging Model data with user provided data (when validation fails)
   *
   * @return Ary data that are used to fill the form.
   */
  private function processData()
  {
    // $data = request()->old(); // don't rename 'data' to 'old'!
    $formData = ControllerTrait::normalize($this->oldData);
    $modelData = $this->model ? $this->model->toArray() : [];

    $data = (array)$this->data; // @todo data should be array always, it receivies null sometimes that needs to be figured out
    $data = array_replace($modelData + $data, $formData);
    $data = self::humanize($data);

    // converting data array to a two-dimensional one.
    $data = ary(\f\arr::dotArray($data));

    $this->method = $this->mode == self::FILTER ? 'get' : ($data->has('id') ? 'put' : 'post');
    $this->id = $data->id;

    if ($this->method == 'put' && $this->generalInfo) {
      $this->items = self::addGeneralInfo($this->items, $data->id, $data->cDate, $data->uDate);
    }

    return $data;
  }

  /**
   * Adds cDate, uDate and id fields to form items.
   * @param      $items
   * @param bool $id
   * @param bool $cDate
   * @param bool $uDate
   * @return array|bool
   */
  static function addGeneralInfo($items, $id = true, $cDate = true, $uDate = true)
  {
    $info = [];
    if ($id) {
      $info[] = [
        'id'       => 'id',
        'type'     => 'number',
        'label'    => __('formule::word.id'),
        'readonly' => true,
        'findable' => true,
      ];
    }

    if ($cDate) {
      $info[] = [
        'id'       => 'cDate',
        'type'     => 'datetime',
        'label'    => __('formule::word.cDate'),
        'readonly' => true,
      ];
    }

    if ($uDate) {
      $info[] = [
        'id'          => 'uDate',
        'type'        => 'datetime',
        'label'       => __('formule::word.uDate'),
        'columnLabel' => __('formule::word.uDate'),
        'readonly'    => true,
      ];
    }

    if (count($info)) {
      if (\f\arr::isAssoc($items)) {
        return [
          'type'  => 'fieldset',
          'items' => [
            [
              'type'  => 'fieldset',
              'items' => $info,
            ],
            $items,
          ],
        ];
      }
      else {

        return array_merge($info, $items);
      }
    }

    return $items;

  }

  /**
   * @param Control $field
   * @return mixed
   * @internal param array|string $items is string uses cnst, if associative array uses array keys and if indexed array
   *           use array value.
   */
  public static function inValidator(Control $field)
  {
    $items = $field['items'];

    if (is_string($items)) {
      return implode(',', array_keys(app('cnst')[$items]));
    }

    if (\f\arr::isAssoc($items) || $field['associative']) {
      return implode(',', array_keys($items));
    }

    return implode(',', $items);
  }

  /**
   * Fetch Form items from a file.
   * @param       $name
   * @param array $params
   * @return mixed
   * @throws \Exception
   */
  public static function getItems($name, $params = [])
  {

    $itemsPath = config('formule.formsPath') ?? app_path('Forms');
    $path = $itemsPath . '/' . Str::studly($name) . 'Form.php';

    if (!file_exists($path)) {
      throw new \Exception("The file definition of the the custom field '$name' could not be found in '$path'");
    }

    if (!is_object($params)) {
      $params = ary($params);
    }

    $model = $params['model'] ?? ary(@$params['data']);

    return include $path;
  }

  public function setOldData()
  {
    if (!app()->runningInConsole() && request()->hasSession() && $data = request()->old()) {
      $this->oldData = $data;
    }
  }

  #region RENDERING

  /**
   * this function converts form validators to laravel validators
   * @param $data
   * @return mixed
   */
  public function validator($data)
  {
    $fields = $this->flat();
    $messages = [];
    $rules = [];
    $labels = [];

    foreach ($fields as $field) {
      $validator = ($field->validator);
      if (!$validator || !$field->id) {
        continue;
      }
      $label = $field->label;
      if ($field->parentLabel) {
        $label .= '(' . __('formule::word.in') . ')';
      }
      if ($field->mode == self::FILTER && in_array(strtolower($field->type), ['date', 'number', 'datetime'])) {
        $maxId = $field->id . '.max';
        $minId = $field->id . '.min';
        $rules[$maxId] = $rules[$minId] = implode('|', (array)$validator);
        $labels[$minId] = __('formule::word.minimum') . $label;
        $labels[$maxId] = __('formule::word.maximum') . $label;
        foreach ((array)$field->msg as $rule => $msg) {
          $messages["$maxId.$rule"] = $messages["$minId.$rule"] = $msg;
        }
      }
      else {
        $id = $field->id;
        if ($field->mode == self::FILTER && in_array(strtolower($field->type),
            ['select', 'radio', 'checkbox', 'boolean'])
        ) {
          $rules[$id] = 'array|' . implode('|', (array)$validator);
        }
        else {
          $rules[$id] = implode('|', (array)$validator);
        }
        $labels[$id] = $label;
        foreach ((array)$field->msg as $rule => $msg) {
          $messages["$id.$rule"] = $msg;
        }
      }
      // Converts validator,messages and label to Laravel format.
    }

    //        ~r($data, $rules, $messages, $labels);

    return Validator::make($data, $rules, $messages, $labels);
  }

  /**
   * Retruns all items in a flat array
   * @return array
   */
  public function flat()
  {
    $this->flat = true;

    return $this->render();
  }

  /**
   * create the filter form for a html grid
   * @return array
   */
  public function render()
  {
    // renders tabs and fieldsets recursively
    $result = $this->subRender(ary(
        [
          'items' => $this->items,
          'mode'  => $this->mode,
          'flat'  => $this->flat,
          'grid'  => $this->grid,
          'data'  => ary($this->data),
          'form'  => $this,
        ])
    );

    return is_string($result) && app()->isLocal() ? tidy($result) : $result;
  }

  /**
   * @param Ary $control
   * @return array
   * @throws \Exception
   */
  public function subRender(Ary $control)
  {
    // places associative array in another array
    $result = [];
    $items = itemize($control['items']);

    foreach ($items as $item) {
      if (is_string($item)) {
        $item = [
          'type' => 'html',
          'html' => $item,
        ];
      }

      // for saving the original item
      $item = ary($item);
      $original = clone $item;


      // keeping parent label for validation errors. e.g. فیلد جنسیت در بخش اطلاعات فردی اشتباه است.
      $item->parentLabel = $control->label;

      // e.g. if the parent tab is readonly all of its child should be readonly.

      $item = $item->union(array_only($control->all(), self::$inheritables));
      // finding field class and creating a control object.
      $field = self::makeField($item);

      if ($field->isReadonly()) {
        $field->readonly = true;
      }

      if (in_array($field->name, $this->requiredFields)) {
        $field->required = true;
        $field->validator[] = 'required';
      }

      $field->model = $this->model;
      if ($field->isField()) {
        $field->originalItems = $original;
      }

      if ($field->displayable === false && $this->mode == self::DISPLAY) {
        continue;
      }

      // skipping hidden fields
      if ($field->isHidden()) {
        continue;
      }

      if (!$control->flat) {
        //this skips nonformable items and $field->readonly is for not skipping readonly items e.g. details in programComment
        if ($field->mode == self::EDIT && !$field->isFormable() && !$original->readonly) {
          continue;
        }
        if ($this->mode == self::FILTER && !$field->isSearchable()) {
          continue;
        }
        if ($field->type == 'password' && $field->mode == self::FILTER) {
          continue;
        }
      }

      if ($control->flat) {
        if (!$field->isContainer() && !$field->isField() && (!$this->mode == 'grid' || $field->type != 'action')) {
          continue;
        }
        $result = array_merge($result, $this->getFields($field));
      }
      elseif ($field->isField() && $field->isReadonly()) {
        if ($this->hideNullFields && $this->mode == self::DISPLAY && !exists($this->value)) {
          continue;
        }
        $result[] = $field->renderReadonly();
      }
      elseif ($field->mode == self::FILTER) {
        $result[] = $field->renderFilter();
      }
      else {
        $result[] = $field->render();
      }
    }

    if ($control->flat) {
      return $result;
    }

    return implode(' ', $result);
  }
  #endregion

  #region AUXILIARY

  /**
   * receives items and make a field from it
   * @param Ary $field
   * @return Control
   * @throws \Exception
   */
  static public function makeField(Ary $field)
  {
    $field = Control::preprocess($field);
    $class = title_case($field->type);

    $types = config('formule.types', []);
    if (in_array($field, $types)) {
      return new $types[$field];
    }

    $fullPath = '\App\Formule\\' . $class;
    if (class_exists($fullPath)) {
      return new $fullPath($field);
    }

    $fullPath = '\Formule\Form\\' . $class;
    if (class_exists($fullPath)) {
      return new $fullPath($field);
    }

    throw Exception('Invalid Formule control type: ' . $field);
  }

  /**
   * fetch the fields that should be shown on the grid as a column.
   * @param      bool
   * @return array
   * @throws \Exception
   * @internal param $items
   */
  function getFields($control)
  {
    $result = [];

    // a development phase only check
    if (array_key_exists($control['id'], $result)) {
      throw new \Exception("{$control['id']} id is used more than once in the table definition");
    }

    if ($control->isField() || $control->type == 'action') {
      return [$control->id => $control];
    }
    //    elseif (isset($control['items']) && $control->gridable !== false) {
    //      $result = ;
    //    }

    return $this->subRender($control);
  }

  /**
   * Returns items for grid columns
   * @return array
   */
  public function grid()
  {
    //for not being in edit mode
    $this->mode = 'grid';

    return $this->flat();
  }

  /**
   * @return string
   */
  public function __toString()
  {
    return $this->show('inline')->render();
  }

  /**
   * This method returns the form in a page
   * @param string $display
   * @return mixed
   * @throws \Throwable
   */
  public function show($display = 'page')
  {
    $this->display = $display;
    $title = $this->processTitle();
    title($title);

    return view('formule::form', [
      'form'        => $this,
      'html'        => $this->render(),
      'action'      => $this->action,
      'backUrl'     => $this->backUrl ? $this->backUrl : $this->action,
      'buttons'     => $this->buttons,
      'id'          => $this->data->id,
      'method'      => $this->method, // put is detected by params value
      'readonly'    => $this->readonly,
      'submitTitle' => $this->submitTitle,
      'title'       => $title,
      'backTitle'   => $this->backTitle ?? ($this->mode == self::EDIT ? __('formule::word.cancel') : __('formule::word.back')),

      'removable'  => $this->removable,
      'submitable' => $this->submitable,
      'editable'   => $this->editable,
      'printable'  => $this->printable,
      'submitIcon' => $this->submitIcon,
      'cls'        => $this->class,
      'errors'     => $this->errors,
      'display'    => $this->getDisplayLayout(),
      'layout'     => $this->layout, // used only if display is 'page'
    ]);
  }

  /**
   * Generating the main form title.
   * @return string
   */
  private function processTitle()
  {
    $title = $this->caption;
    if ($this->title) {
      $action = $this->method == 'post' ? __('formule::word.add') : __('formule::word.edit');
      if (lang('fa')) {
        return $action . ' ' . $this->title;
      }

      return $this->title . ' ' . $action;
    }

    return $title;
  }

  static public function humanize($data)
  {
    if (method_exists($data, 'toArray')) {
      $data = $data->toArray();
    }

    $data = (array)$data;
    foreach ($data as $k => $v) {
      if (is_array($v)) {
        $data[$k] = self::humanize($v);
      }
      elseif ($v && preg_match('#(.+)Date$#u', $k) && region('IR')) {
        $data[$k] = toSolar($v);
      }

      if (is_numeric($v)) {
        $data[$k] = (string)$data[$k];
      }
    }

    return $data;
  }

  /**
   * Which view file should we place the `<form>` content
   * @return string
   */
  private function getDisplayLayout(): string
  {
    if ($this->display == 'print' || (request('print') && $this->readonly && $this->printable)) {
      return 'formule::layout.print';
    }

    if ($this->display == 'inline') {
      return 'formule::layout.inline';
    }

    return 'formule::layout.page';
  }
  #endregion
}