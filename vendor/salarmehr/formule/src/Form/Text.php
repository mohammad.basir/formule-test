<?php namespace Formule\Form;


class Text extends Input
{
  public function render()
  {
    return view('formule::text', ['field' => $this])->render();
  }
}
