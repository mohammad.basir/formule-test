<?php namespace Formule\Form;


class Tab extends Container
{
  public function __construct($field)
  {
    parent::__construct($field);
  }

  public function render()
  {
    return $this->form->subRender($this);
  }
}