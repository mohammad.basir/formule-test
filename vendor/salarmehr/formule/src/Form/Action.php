<?php
namespace Formule\Form;

/*
 * Grid action column
 */
class Action extends Control
{
  function __construct($items)
  {
    parent::__construct($items);
    if ($this->gridable === null) {
      $this->gridable = true;
    }

    $this->displayable = false;
    $this->formable = false;
    $this->searchable = false;
    $this->sortable = false;
    $this->column = false;
    $this->type = 'action';
  }
}