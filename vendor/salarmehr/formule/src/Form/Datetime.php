<?php namespace Formule\Form;

class Datetime extends Date
{
  public $format = '%Y/%m/%d %H:%M';
  public $timeCfg = 'true';
  public $icon = 'fa-clock-o';

  public function __construct($field)
  {
    parent::__construct($field);
  }

  public function render()
  {
    return parent::render();
  }
}