<?php
namespace Formule\Form;


class Star extends Number
{
  public function __construct($field)
  {
    parent::__construct($field);
    $field->min = $field->min ?: 0;
    $field->max = $field->max ?: 5;
    $this->validator [] = "numeric|min:{$field->min}|max:{$field->max}";
  }
}