<?php
namespace Formule\Form;

/**
 * @property array cls
 * @property array attrs
 */
class Button extends Field
{

  public function __construct($field = [])
  {
    parent::__construct($field);
    $this->cls[] = 'formule-btn';
    $this->kind = $this->submit ? 'submit' : ($this->reset ? 'reset' : 'button');
  }

  public function render()
  {
    $tag = $this->has('href') ? 'a' : 'button';
    return view('formule::button', ['field' => $this, 'tag' => $tag])->render();
  }
}