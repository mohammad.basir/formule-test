<?php namespace Formule\Form;


class Number extends Text
{

  public function __construct($field)
  {
    parent::__construct($field);
    $this->validator [] = 'numeric';
  }

  public function renderFilter()
  {
    $minField = clone $this;
    $minField->value = @$this->data->get("$this->id.min");
    $minField->cls[] = 'col-sm-3';
    $minField->label = '';
    $minField->setName($this->name . '.min');
    //for not showing unit three time
    $minField->skipUnit = true;
    $min = $minField->render();

    $maxField = clone $minField;
    $maxField->value = @$this->data->get("$this->id.max");
    $maxField->setName($this->name . '.max');
    $maxField->skipUnit = true;
    $max = $maxField->render();

    return view('formule::range', ['field' => $this] + compact('min', 'max'))->render();
  }
}