<?php namespace Formule\Form;

class File extends Field
{
  public function __construct($field)
  {
    if (!exists($field['id'])) {
      throw new \Exception('File control must have an id');
    }

    parent::__construct($field);
    //    $field->maxSize = config($field->maxSize, 'mehr.maxSize');//todo

    if (str_contains($this->name, '[')) {
      $this->name = $this->attrs['name'] = $this->attrs['id'] = str_replace('.', '_', $this->id);
    }

    $this->file = $this->getFileModel();
  }

  public function getFileModel()
  {
    $form = $this->form;

    if ($this->simple) {
      $id = $form->oldData[$this->id] ?? $this->value;
      return \App\Models\File::find($id);
    }
    elseif (exists(@$form->oldData['files'])) {
      foreach ($form->oldData['files'] as $id => $file) {
        if ($file['field'] == $this->id) {
          return \App\Models\File::findOrFail($id);
        }
      }
    }
    elseif ($this->model) {
      if ($modelFile = $this->model->getFile($this->id)) {
        return $modelFile;
      }
      elseif ($modelFile = $this->model->getFile($this->name)) {
        return $modelFile;
      }
    }
  }

  public function render()
  {
    return view('formule::upload.single', ['field' => $this])->render();
  }

  public function renderFilter()
  {
    return (new Boolean($this))->renderFilter();
  }

  public function renderReadonly()
  {
    if ($this->file) {
      return view('formule::upload.single-info', ['field' => $this])->render();
    }
  }
}