<?php

namespace Formule\Form;

use Illuminate\Support\Facades\DB;
use Salarmehr\Ary;

/**
 *  closure function for all items except 'formatCell', 'query' receive two params: the current field in form and  the
 *  value of current field in forms
 *
 * @property  array        $attrs
 * @property  array|string $cls                     the css classes that is assigned to the main html tag (e.g. <input
 *            class='foo'>)
 * @property  array        $items                   contains items in type of fieldset or tabset and by default it will
 *            be fieldset
 * @property  bool         $emptyItem               `true` adding an empty item for select or not
 * @property  bool         $findable                `true` if tue the form will be considered for quick find
 * @property  bool         $formable                `true` makes the item show in the form of create and edit
 * @property  bool         $formatCell              receive an closure to format the values in the database and it can
 *            be with html files e.g. `function ($value, $row) {return cnst($value, 'smsReport');},` in formatCell we
 *            use a function that receive two params 1. value that show the value of current item 2. row that is an
 *            array that have other values of current record
 * @property  bool         $gridable                `true` makes the item to show in grid or
 * @property  bool         $hidden                  `false` makes the item invisible (as if it does not exists)
 * @property  bool         $multiple                `false` is for select type of fields that we want to be multiple
 *            and
 *            by default is null
 * @property  bool         $raw                     `false` (readonly fields) the hidden tag is not inserted. (so no
 *            value will be sent to server)
 * @property  bool         $readonly                `false` makes the item readonly (the output of formatCell is used)
 * @property  bool         $renderCell              `false`
 * @property  bool         $renderCellSingle        `false`
 * @property  bool         $renderValue             `false` receive an \Closure to render the values in the database
 * @property  bool         $renderValueSingle       `false`
 * @property  bool         $required                `false` makes the item required for validation
 * @property  bool         $searchable              `true` makes the item in grid is searchAble
 * @property  bool         $sortable                `true` make column sortable on the grid
 * @property  bool|array   $mgs                     an array of error message e.g. ['required' => "انتخاب جنسیت الزامی
 *            است."]
 * @property  bool|string  $validator               `false` a laravel rule string. e.g.
 * @property  null         $joinTable               the table that we what to bo joined in find function of Grid for
 *            example finding entities by their types of programs by their entities
 * @property  null         $prefix                  adds a prefix to the item's id and name
 * @property  null         $value                   this item have the value of current item and by default is null
 * @property  string       $column                  is the name of column of current item in database and by default is
 *            equal to $name
 * @property  string       $columnLabel             sets label of column that we want to show in the grid and by
 *            default
 *            it's equal to $label
 * @property  string       $columnRaw               this item receive an query string and execute it and sets the value
 *            of item with given value by query
 * @property  string       $joinTableName           related to $joinTable the column that we want to check in join of
 *            grid's find function name
 * @property  string       $label
 * @property  string       $mode                    `filter` rendering mode: filter or edit
 * @property  string       $msg                     `false`
 * @property  string       $name                    sets the name of item e.g. 'firstName'
 * @property  string       $ng_hide
 * @property  string       $ng_model                e.g. 'nid|unique:user,nid,' . $id
 * @property  string       $ng_show                 is for fields in forms that we want to show when an item is
 *            selected
 * @property  string       $placeholder             sets the string that we want to show in field an disappear by
 *            clicking on it
 * @property  string       $type                    `test` sets the type of item
 * @property  string       $unit                    عبارتی کوتاه که پس از فیل قرار می‌گیرد مثلا: نفر، ریال و ...
 * @property mixed         form
 */
class Control extends Ary
{
  public function __construct($field = [])
  {
    parent::__construct($field);
    $this->attrs = $this->attrs ?? [];

    # prefixing the name if on filter mode.
    $id = $this->id;
    if ($this->isField()) {
      if ($this->prefix) {
        $id = "$this->prefix.$id";
        $this->id = $id;
      }
      elseif ($this->mode == 'filter') {
        $id = "filter.$id";
        $this->readonly = false;
      }
      $this->value = $this->guessValue();
      $this->setName($id);
    }


    # extracting simple value from all method that are set to a closure
    foreach ($this->all() as $k => &$v) {
      if (!in_array($k, ['renderCell', 'query', 'hidden']) && $v instanceof \Closure) {
        $this[$k] = $v($this, $this->value);
      }
    }
    # validator stuffs.
    //this is for adding other validators to field
    $this->validator = (array)$this->validator;

    if ($this->required && $this->mode != 'filter') {
      $this->validator[] = 'required';
    }
    #this is for showing * in form
    if (in_array('required', $this->validator)) {
      $this->required = true;
    }
    $this['msg'] = (array)$this->msg + (array)$this->msg;
    // the value is set to 'default' or 'value' of the
    // field and is formatted by render function if presents.
    $this->value = $this->get('render') ?: $this->value;

    $this->cls = (array)$this->cls ?: [];
    $this->attrs['readonly'] = $this->attrs['disabled'] = $this->get('readonly');
  }

  /**
   * Check if the control may keep some info (usually provided by user.)
   * @return mixed
   */
  public function isField()
  {
    return $this->has('id') && !$this->isContainer();
  }

  /**
   * Check to see if this control can contains others
   * @return mixed
   */
  public function isContainer()
  {
    return in_array($this->type, ['fieldset', 'tabset', 'tab', 'table']);
  }


  private function guessValue()
  {
    $value = $this->value ?: null;

    if ($this->data && $this->data->has($this->id)) {
      $value = $this->data->get($this->id);
    }

    # setting default value if creating and no other value is set (perhaps the second part is redundant)
    if (
      $this->mode == 'edit' && $this->form->method == 'post'
      and
      $value === '' || $value === null
    ) {
      if (($func = $this->default) instanceof \Closure) {
        $this->default = $func($this, $this->value);
      }
      $value = $this->default;
    }

    return $value;
  }

  public function setName($id, $multiple = false)
  {
    $this->attrs['id'] = $this->attrs['name'] = $this->name = dotToArray($id);
    if ($multiple) {
      $this->attrs['multiple'] = true;
      $this->attrs['id'] = $this->attrs['name'] = $this->name . '[]';
    }
  }

  /**
   * Receiving a raw
   * @param Ary $field
   * @return Ary|static
   */
  public static function preprocess(Ary $field)
  {
    // اگر فیلد پراپرتی type دارد، این فیلد برابر با تایپ اصلی یا یک زیرنوع است.
    // زیر نوع‌های آرایه‌هایی ساده هستند که مشخصاتشان با مشخصات نوع اصلی ادغام می‌شود.
    // مثلا اگر نوع اصلی عددی باشد زیرنوع‌ها می‌توانند تلفن، نمره، شماره ملی و... باشد.
    if (isset($field['type'])) {
      $type = $field['type'];

      $subtypes = config('formule.subtypes') ?? [];
      if (array_key_exists($type, $subtypes)) {
        $validator = '';
        $sub = $subtypes[$field['type']];
        if (isset($field['validator'])) {
          $validator = $field['validator'];
        }
        if (isset($sub['validator'])) {
          $validator .= '|' . $sub['validator'];
        }
        $field = $field->union($sub);
        $field['validator'] = $validator;

        // replacign
        $type = $sub['type'];
      }
    }
    elseif ($field->has('html')) {
      $type = 'html';
    }
    elseif ($field->has('items')) {
      $type = 'fieldset';
    }
    else {
      if (isset($field->items[0]) && is_string($field->items[0])) {
        $type = 'html';
        $field->html = $field->items[0];
      }
      else {
        $type = 'text';
      }
    }
    $field['type'] = $type;

    return $field;
  }

  #region ASSERTIONS


  /**
   * Check to see if the field (container or select) is empty.
   * @return mixed
   */
  public function isEmpty()
  {
    return count($this->items);
  }

  public function attrs($attrs = []): string
  {
    $input = $attrs + (array)$this->attrs;
    $output = [];
    foreach ($input as $k => $v) {

      // skip false flag attributes.
      if ($v === false) {
        continue;
      }

      // adding truly flag attributes.
      if ($v === true) {
        $output[] = $k;
      }
      // adding key-value attributes.
      elseif ($v !== null) {
        $output[] = "$k='$v'";
      }
    }

    // adding ng attributes.
    foreach ($this->all() as $k => $v) {
      if ($k === 'ng-model' && $v) {
        if (is_array($this->value)) {
          $value = json_encode($this->value);
        }
        elseif ($this->value) {
          $value = "\"$this->value\"";
        }
        else {
          $value = '""';
        }
        $name = dotToArray($v);
        if ($this->type == 'boolean') {
          $value = $this->value ? 'true' : 'false';
        }
        $output[] = "ng-model='$name'";
        $output[] = "ng-init='$name=$value'";
      }
      // ng-show and ng-hide are added to form-group tag , this true in in_array is necessary for things like in_array(0,['ng-show'])
      elseif (!in_array($k, ['ng-show', 'ng-hide'], true) && str_contains($k, 'ng-') || in_array($k, ['autofocus', 'max', 'min', 'maxlength'])) {
        $output[] = "$k=\"$v\"";
      }
    }

    return implode(' ', $output);
  }

  public function cls($moreCls = [])
  {
    return 'class="' . implode(' ', array_merge((array)$moreCls, (array)$this->cls)) . '"';
  }

  public function renderFilter()
  {
    return $this->render();
  }

  public function isSearchable()
  {
    if (is_bool($this->searchable)) { // also make `attachments` container non-searchable
      return $this->searchable;
    }

    if ($this->isContainer()) {
      return true;
    }

    return $this->isQueryable();
  }

  /**
   * This helper method encapsulates common conditions for gridablity and searchablity.
   * @return bool
   */
  public function isQueryable()
  {
    return
      $this['column'] !== false
      //      and !isset($this['column']) || !isset($this['columnRaw'])
      and !str_contains($this['id'], '[')
      //      and !str_contains($this['id'], '.')
      and !in_array($this['type'], ['html', 'hidden', 'file', 'image', 'action', 'help']) && !$this->isContainer()
      and !$this->isEmpty() || !in_array($this['type'], ['select', 'radio', 'checkbox']) || !$this->isContainer()
      and !isset($this['hidden']) || $this['hidden'] !== true;
    //      and !isset($this['gridable']) || $this['gridable'] !== false;
  }

  /**
   * Should this field be shown as a grid in table
   * @return bool
   */
  public function isGridable()
  {
    if (is_bool($this->gridable)) { // also make `attachments` container non-gridable
      return $this->gridable;
    }

    if ($this->has('column') && !$this->column) {
      return false;
    }

    return $this->isQueryable() and !strpos($this->id, '.') || $this->jsonColumn;
  }

  #endregion

  public function isFormable()
  {
    return $this->formable !== false;
  }

  public function selectString($table = null, $where = false, $jsonColumn = false)
  {

    if ($this->has('column')) {
      if (!$this->column) {
        return; // the field only add a condition to where clause.
      }
      $selectQuery = "$this->column as $this->id";
      if ($where) {
        return $this->column;
      }
    }
    elseif ($this->has('columnRaw')) {
      $selectQuery = DB::raw("{$this->columnRaw} as {$this->id}");
      if ($where) {
        return $this->columnRaw;
      }
    }
    elseif (strpos($this->id, '.') && $where) {
      $selectQuery = $table . '.' . str_replace('.', '->', $this->id);
    }
    elseif (strpos($this->id, '.') && $jsonColumn) {
      $selectQuery = DB::raw("JSON_UNQUOTE($table." . $this->wrapJsonSelector(str_replace('.', '->', $this->id)) . ") as '$this->id'");
    }
    elseif (!strpos($this->id, '.')) {
      $selectQuery = "{$table}.$this->id";
    }

    return $selectQuery;
  }

  public function __toString()
  {
    $result = $this->render();

    return app()->isLocal() ? tidy($result) : $result;
  }

  /**
   * Returns the header of a field for its grid column
   */
  public function getHeader()
  {
    $header = $this->get('columnLabel', $this->label);

    if ($this->unit) {
      $header .= " ($this->unit)";
    }

    return $header;
  }

  /**
   * Wrap a single string in keyword identifiers.
   *
   * @param  string $value
   * @return string
   */
  protected function wrapValue($value)
  {
    if ($value === '*') {
      return $value;
    }

    if ($this->isJsonSelector($value)) {
      return $this->wrapJsonSelector($value);
    }

    return '`' . str_replace('`', '``', $value) . '`';
  }

  /**
   * Wrap the given JSON selector.
   *
   * @param  string $value
   * @return string
   */
  protected function wrapJsonSelector($value)
  {
    $path = explode('->', $value);

    $field = $this->wrapValue(array_shift($path));

    $path = collect($path)->map(function ($part) {
      return '"' . $part . '"';
    })->implode('.');

    return sprintf('%s->\'$.%s\'', $field, $path);
  }

  /**
   * Determine if the given string is a JSON selector.
   *
   * @param  string $value
   * @return bool
   */
  protected function isJsonSelector($value)
  {
    return str_contains($value, '->');
  }

  public function isHidden()
  {
    // if the control is hidden at form level we return ture
    if ($this->from && mece($this->id, $this->form->hidden, $this->form->visible)) {
      return true;
    }

    $value = $this->hidden;

    if (($value instanceof \Closure)) {
      return $value($this);
    }

    return $value;
  }

  public function isReadonly()
  {
    // if the control is hidden at form level we return ture
    if ($this->form && mece($this->id, $this->form->readonly, $this->form->editable)) {
      return true;
    }

    $value = $this->readonly;

    if (($value instanceof \Closure)) {
      return $value($this);
    }

    return $value;
  }

  public function isVisible()
  {
    return !$this->isHidden();
  }

}