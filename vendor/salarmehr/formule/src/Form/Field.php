<?php namespace Formule\Form;

abstract class Field extends Control
{
  public function __construct($field)
  {
    parent::__construct($field);
    if (!in_array($this->type, ['button'])) {
      $this->cls[] = "form-control";
      //      $this->cls[] = "col-sm-9";
    }

    if ($this->get('multiple') || $this->type == 'checkbox') {
      $this->setName($this->name, true);
    }
    //    $this->attrs['aria-describedby'] = "{$this->name}-help";
    $this->label = $this->generateLabel();
    $this->attrs['readonly'] = $this->readonly;
    $this->attrs['width'] = gettype($this->width) == 'integer' ? $this->width . 'px' : $this->width;
    $this->attrs['placeholder'] = $this->placeholder ?: $this->label;
  }

  public function renderFilter()
  {
    $this->mode = 'filter';

    return $this->render();
  }

  public function getSortField()
  {
    $sortItems = [
      'none' => '➖',
      'asc'  => '🔺',
      'desc' => '🔻',
    ];

    return new Select(ary([
      'items' => $sortItems,
      'name'  => $this['name'] . 'Sort',
    ]));
  }

  public function renderReadonly()
  {
    $type = $this->type;
    $this->type = 'info';
    $field = field($this);

    // adding a hidden field if the form is not in readonly mode and field is not raw.
    if (!$field->raw && !$field->form->readonly) {
      $field->hiddenField = Hidden::getHiddenTag($field->name, $field->value);
    }

    if (method_exists($this, 'renderCell') && !in_array($type, ['date', 'datetime'])) {
      $field->richValue = $this->renderCell();
    }
    if (method_exists($this, 'renderValue') && !in_array($type, ['date', 'datetime'])) {
      $field->richValue = $this->renderValue();
    }

    return $field->render();
  }

  protected function renderCellSingle($value)
  {
    return $this->renderValueSingle($value);
  }

  protected function generateLabel()
  {
    $id = \f\str::decamlize($this->id);

    return $this->label ?: (config('formule.titleCase') ? title_case($id) : $id);
  }
}