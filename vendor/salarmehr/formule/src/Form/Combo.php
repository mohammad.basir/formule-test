<?php
/**
 * Created by PhpStorm.
 * User: Reza
 * Date: 18/03/2016
 * Time: 09:42
 */

namespace Formule\Form;

class Combo extends Select
{
  public function __construct($field)
  {
    $this->combo = true;
    parent::__construct($field);
    $this['items'] = [];

    if ($this->mode == 'edit' && $this->url) { // not sure why mode is checked here
      $this->attrs['data-ajax--url'] = $this->url;
    }
    if (!in($field->type, 'program', 'page') && can('create', 'App\Models\\' . title_case($field->type))) {
      $this->addBtn = true;
    }
  }

  /**
   * Format array response for client side combo implementation
   * @param        $result
   * @param string $text
   * @param string $id
   * @return array
   */
  public static function formatResponse($result, $text = 'text', $id = 'id')
  {
    $name = 'text';
    $value = 'id';

    // returning result for semantic ui
    if (request('format') == 'ui') {
      $name = 'name';
      $value = 'value';
    }

    $response = [];
    foreach ($result as $v) {
      $v = (array)$v;
      $response[] = [$value => $v[$id], $name => $v[$text]];
    }

    return ['results' => $response];
  }
}