<?php
namespace Formule\Form;

class Attachments extends Field
{
  protected  $maxSize;
  function __construct($items)
  {
    parent::__construct($items);
    $this->searchable = false;
    $this->gridable = false;
    $this->id = $this->id ?: 'files';
    $this->maxSize = config('formule.image.maxSize');
  }

  public function render()
  {
    // handling request if some validation errors occurs
    $items = [];
    if (isset($this->form->oldData['files']) && $oldData = $this->form->oldData['files']) {
      foreach ($oldData as $id => $file) {
        if ($file['field'] == $this->id) {
          $items[] = \App\Models\File::findOrFail($id);
        }
      }
    }
    else {
      if ($this->model) {
        $items = $this->model->getFiles($this->id);
      }
      else {
        $items = [];
      }
    }
    //این متغیر مقدار field را در اکشن storeFile@filecontroller مشخص می کند که باید برابر با آی دی پیوست باشد
    $formField = $this->id ?: 'files';
    $fieldset = [
      'type'  => 'fieldset',
      'label' => $this->label ?: __('formule::word.attachments'),
      'class' => $formField . '-upload-box',
      'items' => [
        'html' => view('formule::upload.multiple', [
            'maxSize' => $this->maxSize,
            'files' => $items,
            'help'  => $this->help,
            'field' => $formField, // show which form field files belongs (image, files, attachments, fields)
          ]
        )->render(),
      ],
    ];

    return (new Form($fieldset, ['generalInfo' => false]))->render();
  }

  public function renderReadonly()
  {
    return view('formule::attachments', ['files' => $this->model->getFiles($this->id)])->render();
  }
}
