<?php
namespace Formule\Form;


class Email extends Text
{

  function __construct($field = [])
  {
    parent::__construct($field);
    $this->label = $this->label ?: __('formule::word.email');
    $this->validator [] = 'email';
  }

  public function render()
  {
    return parent::render();
  }
}