<?php

namespace Formule\Form;

class Table extends Container
{

  public function render()
  {
    $this['items'] = itemize($this['items']);
    $tableHtml = '';
    foreach ($this['items'] as $i => $row) {
      $rowHtml = '';
//if($row['type']=='row'){
      foreach ($row as $column) {
        $column = array_merge($column,(array_only($this->all(), Form::$inheritables)));
        $itemHtml = field($column)->render();
        $rowHtml .= '<td>' . $itemHtml . '</td>';
      }
      $tableHtml .= '<tr>' . $rowHtml . '</tr>';
//}
    }

    $this['renderedHtml'] = "<table>$tableHtml </table>";
    return view('formule::table', ['field' => $this])->render();
  }

}