<?php

namespace Formule\Form;


class Captcha extends Text
{

  public function __construct($field)
  {
    parent::__construct($field);
    $this->label = $this->label ?: __('formule::word.captcha');
    $this->setName('captcha');
    $this->validator [] = 'captcha';
    $this->validator [] = 'required';
    $this->required = true;
    $this->id = 'captcha';
  }

  public function render()
  {
    return parent::render();
  }

}
