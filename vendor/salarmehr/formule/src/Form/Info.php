<?php namespace Formule\Form;

class Info extends Field
{
  public function __construct($field)
  {
    parent::__construct($field);
  }

  public function render()
  {
    return view('formule::info', ['field' => $this])->render();
  }
}