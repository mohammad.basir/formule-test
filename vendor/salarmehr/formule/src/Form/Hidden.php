<?php namespace Formule\Form;


class Hidden extends Text
{
  public function render()
  {
    return self::getHiddenTag($this->name, $this->value);
  }

  public static function getHiddenTag($name, $value)
  {
    return "<input  type='hidden' name='{$name}' value='{$value}'>";
  }

}