<?php
namespace Formule\Form;

class Tabset extends Container
{
  static public $tabId = 0;

  public function __construct($field)
  {
    parent::__construct($field);
  }

  public function render()
  {
    $this['items'] = itemize($this['items']);

    foreach ($this['items'] as $i => &$tab) {
      // hiding non-formable tab when in non-filter mode
      if (
        $this->mode == 'filter' && @$tab['searchable'] === false
        or
        $this->mode == 'edit' && @$tab['formable'] === false
        or
        (($value = @$tab['hidden']) instanceof \Closure) ? $value($this) : $value
      ) {
        unset($this['items'][$i]);
      }

      $tab['type'] = 'tab';
      $tab['data'] = $this->data;
      if (!isset($tab['id'])) {
        $tab['id'] = self::$tabId++;
      }
      if ($i == 0) {
        $tab['cls'][] = 'active';
      }

      $tab = field($tab);
      $tab = $tab->union(array_only($this->all(), Form::$inheritables));

      $tab['renderedHtml'] = $this->form->subRender($tab);
      if (!trim($tab['renderedHtml'])) {
        unset($this['items'][$i]);
      }
    }
    if ($this['items']) {
      return $this->renderView();
    }
  }

  protected function renderView()
  {
    return view('formule::tabset', ['field' => $this])->render();
  }

}
