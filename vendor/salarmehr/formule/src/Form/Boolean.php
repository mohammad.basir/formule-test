<?php
namespace Formule\Form;

class Boolean extends Field
{

  public function __construct($field)
  {
    parent::__construct($field);
    $this->cls[] = 'switch';
    $this->selectItems = [
      ''  => '(مهم نیست)',
      '1' => 'بله',
      '0' => 'خیر',
    ];
  }

  public function render()
  {
    return view('formule::boolean', ['field' => $this])->render();
  }

  public function renderFilter()
  {
    if ($this->booleanFilter) {
      return $this->render();

    }
    $field = field($this->all());
    $field->type = 'select';

    $field['items'] = $this->selectItems;
    $field->setName($this->id, true);
    $field['id'] = $this->id;
    // ------- an old way to do this -------
    //    $field = [
    //      'type'  => 'select',
    //      'label' => $this->label,
    //      'id'    => $this->id,
    //      'readonly'=>$this->readonly,
    //      'items' => [
    //        ''  => '(مهم نیست)',
    //        '1' => 'بله',
    //        '0' => 'خیر',
    //      ],
    //    ];

    //when the items has prefix and a new instance is created from that the id will be duplicated.
    return $field->renderFilter();
  }

  public function renderCell($value = null)
  {
    $value = $value !== null ? $value : $this->value;
    if ((string)$value === '1') {
      return '✔';
    }
    if ((string)$value === '0') {
      return '✖';
    }

    return $value;
  }

  public function renderReadonly()
  {
    $field = field($this->all());
    $field->type = 'select';
    $field['items'] = $this->selectItems;
    $field->setName($this->id, true);
    $field['id'] = $this->id;

    return $field->renderReadOnly();
  }

  public function renderCellSingle($value)
  {
    return $this->selectItems[$value];
  }
}