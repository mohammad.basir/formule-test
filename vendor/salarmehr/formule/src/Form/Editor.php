<?php
namespace Formule\Form;


class Editor extends Textarea
{
    public function __construct($field)
    {
        parent::__construct($field);
        $this->cls[] = 'rich';
    }
}
