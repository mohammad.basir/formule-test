<?php
namespace Formule\Form;

class Checkbox extends Radio
{
  public function __construct($field)
  {
    parent::__construct($field);
    $this->validator[] = 'array';
  }
}