<?php

namespace Formule\Traits;

use App\Http\Controllers\Controller;
use App\Models\BaseModel;
use Formule\Form\Form;
use Illuminate\Http\RedirectResponse;
use Salarmehr\Ary;

trait ControllerTrait
{
  static public function humanize($data): array
  {
    return Form::humanize($data);
  }

  static public function normalize($data, $ignore = [])
  {
    foreach ($data as $k => $v) {

      if ($v and preg_match('#(.+)Date$#u', $k) || $k == 'date' && region() == 'IR') {
        $data[$k] = toIso($v);
        continue;
      }

      if (is_array($v)) {
        $v = array_filter($data[$k], function ($v) {
          return exists($v);
        });

        if (\f\arr::isAssoc($v)) {
          $data[$k] = self::normalize($v);
        }
        continue;
      }

      if ($v === '') {
        $data[$k] = null;
        continue;
      }

      if (is_string($v) && !in_array($k, $ignore)) {
        $data[$k] = \f\str::normalizeWord($v);
      }
    }

    return $data;
  }

  /**
   * Run a hyperSave on model and use its return value to create a response
   * @param      BaseModel $model
   * @param null           $params
   * @return Controller|mixed
   * @throws \Exception
   */
  public function superSave($model, $params = [])
  {
    $params['request'] = $params['request'] ?? request()->all();

    $result = $model->hyperSave($params);

    try {
      return $this->redirectForm($result);
    } catch (\Exception $e) {
      if (app()->env == 'local') {
        throw $e;
      }

      return $this->danger($e->getMessage());
    }
  }

  /**
   * Redirect form page regarding "result" object
   * @param $result
   * @return mixed
   */
  protected function redirectForm(Ary $result)
  {
    if ($result->success === false && $result->messages) {
      return redirect()
        ->back()
        ->withInput()
        ->withErrors($result->messages);
    }
    elseif ($result->success === true && $result->messages) {
      return $this->success($result->redirect, $result->messages);
    }
  }

  public function easyDelete(BaseModel $model, $redirect = null, bool $force = false)
  {
    try {
      if ((!$force && $model->delete()) or ($force && $model->forceDelete())) {
        $redirect == $redirect ?: $model->baseUrl;

        if ($isProgram = $model instanceof Program) {
          $message = $force ? 'برنامه به شکل کامل حذف شد.' : 'برنامه به عنوان حذف شده علامت گذاری شد.';
        }

        return $isProgram ? $this->success($redirect, $message) : $this->success($redirect);
      }

      return $this->danger();
    } catch (\Exception $e) {
      #if model is in mode of softDelete and couldn't delete it then soft delete it
      if ($force) {
        $model->setForceDeleting(false);

        return $this->easyDelete($model, $redirect, false);
      }

      if (app()->env == 'local') {
        return $this->danger($e->getMessage());
      }

      return $this->danger('متاسفانه به دلیل وجود داده وابسته نمیتوان این مورد را حذف کرد.');
    }

  }

  /**
   * Authorizes a given action for a guest.
   * @param       $ability
   * @param array $arguments
   * @return \Illuminate\Auth\Access\Response|null
   */
  public function certify($ability, $arguments = [])
  {
    $user = auth()->user() ? auth()->user() : new \App\Models\User;

    return $this->authorizeForUser($user, $ability, $arguments);
  }

  /**
   * Manually run validation process of a form.
   * @param $items
   * @return mixed
   */
  protected function validateForm(array $items)
  {
    $validator = form($items)->validator(request()->all());

    if ($validator->fails()) {
      return redirect()
        ->back()
        ->withInput()
        ->withErrors($validator);
    }
  }

  protected function gridParams(Request &$request)
  {
    if ($request->has('query')) {
      $request->merge(json_decode($request->get('query'), true));
    }
  }

  protected function setBackUrl()
  {
    $backUrl = back()->getTargetUrl();
    $back = session()->get('backUrl');
    if (!str_contains($backUrl, '/login') && !str_contains($backUrl, '/password/change') && $back != $backUrl) {
      session()->put('backUrl', $backUrl);
    }
  }

  private function checkRequest($arr, $check)
  {
    $flatArr = \f\arr::dotArray($arr);
    //the checking part
    $result = array_only($flatArr, $check);
    //تبدیل آرایه دو بعدی نقطه دار به چند بعدی
    foreach ($result as $k => $v) {
      assignArrayByPath($result, $k, $v, '.');
    }
    //deleting elements that have . in their keys
    foreach ($result as $k => $v) {
      if (str_contains($k, '.')) {
        unset($result[$k]);
      }
    }

    return $result;
  }

  #region ALERTS

  /**
   * adds various alert message types to response.
   * @param null       $redirect     string|RedirectResponse redirect url or object
   * @param            $message      array|string  message
   * @param bool       $withInput
   * @return mixed
   */
  public function message($redirect = null, $message = null, $withInput = false)
  {
    if (is_string($message)) {
      $message = [['text' => $message, 'type' => 'danger']];
    }

    if ($redirect) {
      $redirect = $redirect instanceof RedirectResponse ? $redirect : redirect($redirect);
      $response = $redirect->with('messages', $message);
    }
    else {
      $response = redirect()->back()->with('messages', $message);
    }

    return $withInput ? $response : $response->withInput();
  }

  /**
   * a helper method to show a successful operation. (200)
   * @param null   $redirect
   * @param string $message
   * @return mixed
   */
  public function success($redirect = null, $message = ' دستور شما با موفقیت انجام شد.')
  {
    return $this->message($redirect, [['text' => $message, 'type' => 'success']]);
  }

  /**
   * a helper method to report an invalid request (400)
   * @param string $message
   * @param null   $redirect
   * @return mixed
   */
  public function danger(
    $message = 'متاسفانه خطایی رخ داده است، لطفا بعد دوباره امتحان کنید. در صورت رفع نشدن موضوع لطفا با سرپرست سامانه تماس بگیرید.',
    $redirect = null
  )
  {
    return $this->message($redirect, [['text' => $message, 'type' => 'danger']], false);
  }

  /**
   * a helper method to show server failure. (500)
   * @param null $message
   * @param null $redirect
   * @param int  $status
   * @return mixed
   */
  public function failure($message = null, $redirect = null)
  {
    if (!$message) {
      $message = [
        [
          'text' => 'متاسفانه خطایی رخ داده است، لطفا بعد دوباره امتحان کنید. در صورت رفع نشدن موضوع لطفا با سرپرست سامانه تماس بگیرید.',
          'type' => 'danger',
        ],
      ];
    }

    return $this->message($redirect, $message, true);
  }

  public function info($redirect, $message)
  {
    return $this->message($redirect, [['text' => $message, 'type' => 'info']], 200);
  }

  public function warning($message = 'خطایی در اجرای دستور رخ داد.', $redirect = null)
  {
    return $this->message($redirect, [['text' => $message, 'type' => 'warning']], 500, true);
  }

  public function unauthorized($redirect = null)
  {
    return $this->message($redirect, [['text' => 'شما مجوز دسترسی به این مورد را ندارید.', 'type' => 'warning']], 401);
  }
  #endregion
}