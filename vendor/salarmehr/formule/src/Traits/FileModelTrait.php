<?php

namespace Formule\Traits;

trait FileModelTrait
{
  public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);
    if (is_file($this->path)) {
      list($this->width, $this->height) = getimagesize($this->path);
      if ($this->width) {
        $this->isImage = true;
      }
    }

  }

  public function isMedia()
  {
    return $this->isImage() || $this->isVideo();
  }

  /**
   * Check if a file is image
   * @return bool
   */
  public function isImage()
  {
    return $this->type == 'image';
  }

  /**
   * Check if a file is video
   * @return bool
   */
  public function isVideo()
  {
    return $this->type == 'video';
  }

  /**
   * Check if a file is audio
   * @return bool
   */
  public function isAudio()
  {
    return $this->type == 'audio';
  }

  function getSrcAttribute()
  {
    return $this->src('show');
  }

  function src($action = 'show')
  {
    return url("file/{$this->id}/$action/{$this->name}");
  }

  function getDisplayPathAttribute()
  {
    $this->path(1200);
  }

  public function existence()
  {
    return file_exists($this->path);
  }

  public function getInfoAttribute()
  {
    return new \Symfony\Component\HttpFoundation\File\File($this->path);
  }


  public function scopeField($query, $access)
  {
    return $query->where('field', $access);
  }

  public function fileable()
  {
    return $this->morphTo();
  }

  public function save(array $options = [])
  {
    if (!$this->user) {
      $this->user = cuser()->id;
    }
    parent::save($options);
  }

  public function assignToModel(BaseModel $model, $field = null)
  {
    $this->fill([
      'fileableType' => $model->getMorphClass(),
      'fileableId'   => $model->id,
    ]);
    if ($field) {
      $this->field = $field;
    }

    return $this->save();
  }

  public function path(int $size, string $format = 'jpg', string $effect = null, int $quality = 60)
  {

    // not allowing very large images.
    if ($effect) {
      $quality = 90;
    }
    $size = $size > stg('image.maxSize') || $size < 1 ? (stg('image.maxSize') ?: $size) : $size;

    // not resizing small images.
    if ($this->size < 50000) {
      return $this->path;
    }
    $path = cfg('fs.cacheDir') . "{$this->hash}-$size-$effect.$format";

    // if the thumbnail is created previously skip creating it again.
    if (file_exists($path)) {
      return $path;
    }


    /** @var \Intervention\Image\Image $img */
    $img = app('image')->make($this->path);

    //RESIZING
    // size show the max dimension size
    if ($this->width > $this->height) {
      $img->resize($size, null, function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
      });
    }
    else {
      $img->resize(null, $size, function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
      });
    }

    //BLURRING
    if ($effect == 'blur') {
      $img->blur(70);
    }

    $img->save($path, $quality);

    return $path;
  }

}