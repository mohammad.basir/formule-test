<?php

namespace Formule\Traits;

use Cache;
use Formule\Form\Select;
use Illuminate\Database\Query\Expression;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

trait ModelTrait
{
  /*
  Add this to your base model manually as const is not allowed in traits
    const CREATED_AT = 'cDate';
    const UPDATED_AT = 'uDate';
    const DELETED_AT = 'dDate';

  Not constant but yet manual copy please! I had a php error.
  public static $snakeAttributes = false;
  protected $guarded = ['id', 'cDate', 'uDate'];
    */

  #region overwrites

  /**
   * Get the default foreign key name for the model.
   *
   * @return string
   */
  public function getForeignKey()
  {
    return Str::snake(class_basename($this));
  }

  /**
   * Retrieve the fully qualified class name from a slug.
   *
   * @param  string $class
   * @return string
   */
  public static function getActualClassNameForMorph($class)
  {
    return '\App\Models\\' . title_case($class);
  }

  /**
   * Get the polymorphic relationship columns.
   *
   * @param  string $name
   * @param  string $type
   * @param  string $id
   * @return array
   */
  protected function getMorphs($name, $type, $id)
  {
    $type = $type ?: $name . 'Type';
    $id = $id ?: $name . 'Id';

    return [$type, $id];
  }
  #endregion

  #region scopes
  public function scopeFilter($query, $fields, $filters = [])
  {
    return $this->filter($query, $fields, $filters);
  }

  public function scopeEllipsis($query, $column, $length = 100, $alias = null)
  {
    $alias = $alias ?: $column;

    return $query->addSelect([DB::raw("CONCAT(LEFT($column, $length), IF(LENGTH($column)>$length, '…', '')) as $alias")]);
  }

  /**
   * Similar to atPresent scope but additionally returns true if both start and end are null.
   * generally is be used for long term intervals.
   * @param        $query
   * @param string $start
   * @param string $end
   * @return mixed
   */
  public function scopeCurrent($query, $start = 'startDate', $end = 'endDate')
  {
    return $query->where(function ($query) use ($start, $end) {
      $query->atPresent($start, $end);
      $query->orWhere(function ($query) use ($start, $end) {
        $query->whereNull($start);
        $query->whereNull($end);
      });
    });
  }

  public function scopeAtPresent($query, $start = 'startDate', $end = 'endDate')
  {
    return $query->where($start, '<=', DB::raw('now()'))
      ->where(function ($query) use ($end) {
        $query
          ->orWhere($end, '>=', DB::raw('now()'))
          ->orWhereNull($end);
      });
  }

  /**
   * @deprecated
   * This determines the foreign key relations automatically to prevent the need to figure out the columns.
   *
   * @param \Illuminate\Database\Query\Builder $query
   * @param string                             $relation_name
   * @param string                             $operator
   * @param string                             $type
   * @param bool                               $where
   * @return \Illuminate\Database\Query\Builder
   */
  public function scopeModelJoin($query, $relation_name, $operator = '=', $type = 'left', $where = false)
  {
    $relation = $this->$relation_name();
    $table = $relation->getRelated()->getTable();
    $one = $relation->getQualifiedParentKeyName();
    $two = $relation->getForeignKey();

    if (empty($query->columns)) {
      $query->select($this->getTable() . ".*");
    }
    foreach (\Schema::getColumnListing($table) as $related_column) {
      $query->addSelect(new Expression("`$table`.`$related_column` AS `$table.$related_column`"));
    }

    return $query->join($table, $one, $operator, $two, $type, $where); //->with($relation_name);
  }

  #endregion

  /**
   * Get a model by id or return its argument if it is not an integer
   * it usually used when you want to get a model but
   * skip fetching if it was fetched previously.
   * @param      $input
   * @param bool $inArray
   * @return mixed
   */
  static public function grab($input, $inArray = false)
  {
    if (gettype($input) != 'object' && ((int)$input <> 0 || gettype($input) == 'array')) {
      $input = self::find($input);
    }
    if ($inArray) {
      return placeInArray($input);
    }

    return $input;
  }

  public function hasColumn($name)
  {
    return in_array($name, $this->getTableColumns());
  }

  public function getTableColumns()
  {
    return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
  }

  /**
   * Get the table associated with the model.
   *
   * @return string
   */
  public function getTable()
  {
    if (isset($this->table)) {
      return $this->table;
    }

    return str_replace('\\', '', Str::snake(class_basename($this)));
  }

  public function url(...$action)
  {
    $base = $this->morphClass ?: strtolower((new \ReflectionClass($this))->getShortName());

    return url("$base/{$this->getKey()}", $action);
  }

  /**
   * A helper function setting where clause for numeric and temporal columns.
   * @param      $query
   * @param      $name
   * @param      $filter
   * @param bool $aggregative
   */
  static private function compare(&$query, $name, $filter, $aggregative = false)
  {
    if ($aggregative) {
      if (isset($filter['min']) && exists($filter['min'])) {
        $query->having($name, '>=', $filter['min']);
      }
      if (isset($filter['max']) && exists($filter['max'])) {
        $query->having($name, '<=', $filter['max']);
      }
    }
    else {
      if (isset($filter['min']) && exists($filter['min'])) {
        $query->where($name, '>=', $filter['min']);
      }
      if (isset($filter['max']) && exists($filter['max'])) {
        $query->where($name, '<=', $filter['max']);
      }
    }
  }

  /**
   * applies user provided criteria on query
   * @param Builder $query
   * @param array   $fields flat columns
   * @param array   $filters
   * @return mixed
   */
  static public function filter($query, $fields, $filters = [])
  {
    foreach ((array)$filters as $column => $filter) {
      if (!exists($filter) || !isset($fields[$column])) {
        $dataFields = [];
        if (in($column, 'data', 'audience', 'formData')) {
          foreach ($filter as $id => $value) {
            $dataFields += ["$column.$id" => $value];
          }
          self::filter($query, $fields, $dataFields);
        }
        continue;
      }
      $field = $fields[$column];
      $select = $field->selectString($query->getQuery()->from, true);

      # some fields add new where conditions to query (in filter mode.)
      if ($field->has('query')) {
        $where = $field->query;
        $where($query, $filter);
        continue;
      }
      if ($field instanceof Select) {
        $field['type'] = 'select';
      }

      switch ($field['type']) {
        case 'text' :
        case 'editor' :
        case 'textarea' :
          $query->where($select, 'like', '%' . $filter . '%');
          break;
        case 'select' :
        case 'radio' :
        case 'checkbox' :
          if (strpos($field->id, '.')) {
            //formatting query
            $arr = explode('->', $select);
            $json = json_encode($filter);
            $query->whereRaw("JSON_CONTAINS($arr[0]->\"$.$arr[1]\",'$json')");
          }
          else {
            $query->whereIn(DB::raw($select), (array)$filter);
          }
          break;
        case 'boolean' :
          $query->where($select, '=', $filter);
          break;
        case 'number' :
          self::compare($query, $select, $filter, $field['aggregative']);
          break;
        case 'date' :
        case 'datetime':
          $date = toIso($filter);
          self::compare($query, $select, $date);
          break;
      }
    }

    return $query;
  }

  /**
   * filter request from unwanted fields (fields that are not in the form array)
   * @param $request
   * @param $allowedFields
   * @return \Salarmehr\Ary
   */
  private function sanitize($request, array $allowedFields)
  {
    // todo-said این تابع خیلی کثیف به نظر می‌رسه و باید بسیار ساده‌تر بشه پیاده‌سازی کرد
    $flatArr = \f\arr::dotArray($request);
    //the checking part
    $result = array_only($flatArr, $allowedFields);
    //adding min and max items to result
    foreach ($allowedFields as $item) {
      if (isset($flatArr[$item . '.min'])) {
        $result += [$item . '.min' => $flatArr[$item . '.min']];
      }
      if (isset($flatArr[$item . '.max'])) {
        $result += [$item . '.max' => $flatArr[$item . '.max']];
      }
    }
    //تبدیل آرایه دو بعدی نقطه دار به چند بعدی
    foreach ($result as $k => $v) {
      assignArrayByPath($result, $k, $v, '.');
    }
    //deleting elements that have . in their keys
    foreach ($result as $k => $v) {
      if (str_contains($k, '.')) {
        unset($result[$k]);
      }
    }
    //this is for adding items with empty array values of $arr such as audience
    foreach ($request as $k => $v) {
      if ($v === []) {
        $result += [$k => $v];
      }
    }
    //adding files to data because they aren't in form items
    if (isset($request['files']) && $request['files']) {
      $result['files'] = $request['files'];
    }

    return $result;
  }

  public function hyperSave($params = null)
  {
    /*****************
     * Setting default values
     ****************/

    $p = ary($params + [
        'successMsg' => trans('formule::msg.doneSubmit'),
        'redirect'   => false, // after save redirect to this url
        'form'       => false, // a \Formule\Form instance
        'data'       => [],    // to be merged with request
        'request'    => false, // data provided by the end user.

        'only'   => false, // only included from request
        'except' => false, // excluded from request,

        'alert'            => false, // the notification that should be sent after saving,
        'files'            => false,
        'before'           => false,
        'after'            => false, // a closure that is called after model is saved.
        'cache'            => null, // the cache key that should be removed after successful saving
        'manualValidators' => [],
      ]
    );

    $data = $p->data; // data from database or php

    /*****************
     * Validating
     ****************/

    //only merging the fields that are expected from user
    if ($p->form && $p->request) {
      $data += $this->sanitize($p->request, array_keys($p->form->flat()));
      $validator = $p->form->validator($data);

      //doing manual validator stuffs and adding error messages to messageBag if validator fails
      $messageBag = $validator->getMessageBag();

      foreach ((array)$p->manualValidators as $closure) {
        if ($closure instanceof \Closure) {
          if ($msg = $closure($this)) {
            itemize($msg);
            foreach ($msg as $k => $v)
              $messageBag->add($k, $v);
          }
        }
      }

      $messages = $validator->messages()->getMessages();
      if ($messages || $validator->fails()) {
        return ary(['success' => false, 'messages' => $messages]);
      }
    }

    /*****************
     * Saving
     ****************/

    if ($p->except) {
      $data = array_except($data, $p->except);
    }
    elseif ($p->only) {
      $data = array_only($data, $p->only);
    }

    $data = array_except($data, ['files']);

    $this->fill($data);

    DB::beginTransaction();
    if ($p->before) {
      ($p->before)($this);
    }

    $this->save();

    if ($p->request) {
      $this->storeFiles(request(), null, $p->files);
    }

    DB::commit();

    /*****************
     * Cache refreshing
     ****************/

    /*****************
     * Notifying
     ****************/
    if ($p->alert) {
      $p->alert->send();
    }

    /*****************
     * Post-actions
     ****************/

    if ($p->after) {
      $afterRedirect = ($p->after)($this);
    }

    /*****************
     * Refreshing cache
     ****************/

    if ($p->cache) {
      Cache::delete($p->cache);
    }

    /*****************
     * Redirecting
     ****************/
    $afterRedirect = isset($afterRedirect) ? ($afterRedirect ==false ? null : $afterRedirect) : null;
    $p->redirect = isset($p->redirect) ? ($p->redirect ==false ? null : $p->redirect) : null;

    $redirect = $afterRedirect ?? $p->redirect ?? $this->url();

    return ary(['success' => true, 'messages' => $p->successMsg, 'redirect' => $redirect]);
  }

  public function storeFiles($allowedFields = null, $only = [])
  {
    $allowedFields = $allowedFields ?: array_keys((array)$this->fileFields);
    #region STORING FILES
    //sample files in a request:
    // files[id][field]
    // files[id][details]
    $modelFilesIds = $only ? $this->files()->whereIn('field', (array)$only)->pluck('id')->toArray() : $this->files()->pluck('id')->toArray();
    $requestFiles = (array)request()->get('files');

    // deleting allowed missing files.
    $removedFiles = array_diff($modelFilesIds, array_keys($requestFiles));
    if(exists($removedFiles)) {
      \App\Models\File::whereIn('id', $removedFiles)->whereIn('field', $allowedFields)->delete();
    }
    // updating current files or adding new files
    foreach ($requestFiles as $id => $f) {
      if (!in_array($f['field'], $allowedFields)) {
        continue;
      }
      $file = File::findOrFail($id);
      $file->field = $f['field'];
      $file->details = @$f['details'];
      $file->expirationDate = null;
      $file->fileableId = $this->id;
      $file->access = $this->fileFields[$file->field];
      $file->save();
      $this->files()->save($file);
    }
    //          $this->files()->saveMany(File::whereIn('id', $requestFilesIds)->get()->all());
    #endregion
  }

  public function files()
  {
    return $this->morphMany(\App\Models\File::class, 'fileable');
  }


}