<?php

namespace Formule\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;

trait UserModelTrait
{
  #region Attributes
  /**
   * this concat firstName and lastName of user and returns ih
   * @return string
   */
  public function getFullNameAttribute(): string
  {
    return $this->firstName . " " . $this->lastName;
  }

  /**
   * this returns formalName of User
   * @return string
   */
  public function getFormalNameAttribute(): string
  {
    return $this->gender == 'm' ? __('formule::word.mr') : __('formule::word.ms') . ' ' . $this->fullName;
  }

  /**
   * به اول formalName سرکار یا جناب اضافه میکنه
   * @return string
   */
  public function getLongFormalNameAttribute(): string
  {
    return ($this->gender == 'm' ? 'جناب ' : 'سرکار ') . $this->formalName;
  }

  public function getInitials($format = 'i.i.'): string
  {
    $F = $this->firstName;
    $L = $this->lastName;
    $f = mb_substr($F, 0, 1);
    $l = mb_substr($L, 0, 1);

    switch ($format) {
      case 'i.i.': // R. S.
        return "$f. $l.";
      case 'ii': // RS
        return "$f$l";
      case 'i.f': // R. Salarmehr
        return "$f. $L";
      case 'fi.': // Reza S.
        return "$F $l.";
    }

    throw new \Exception("Invalid initials format [$format]");
  }

  public function getInitialsAttribute()
  {
    return $this->getInitials('i.i.');
  }
  #endregion

  #regions relations
  public function logins(): HasMany
  {
    return $this->hasMany('App\Models\Login', 'userId', 'id');
  }
  #endregion
}