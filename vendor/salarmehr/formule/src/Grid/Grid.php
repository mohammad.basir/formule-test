<?php

namespace Formule\Grid;

use Formule\Form\Form;
use Formule\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Facades\Excel;

class Grid
{
  public static $allUserItems = [];
  public $items = [];
  public $columns = [];
  public $labels = [];
  public $table;
  public $selects;
  public $filters;
  public $format;
  private $data = null;
  private $query = null;

  /**
   * Grid constructor.
   * @param $items
   * @param $data
   * @param $params
   *  destroyable=false to show delete column
   *  showable=true to show view column
   *  editable=true to show edit column
   *  idColumn=true to show edit column
   *  cDateColumn=true to show edit column
   *  uDateColumn=true to show edit column, if applicable
   *
   */
  public function __construct($items, $data, $params)
  {
    $this->params = ary($params);
    $this->pageTitle = $this->params['title'];
    $this->model = $this->params['model'];
    if ($data instanceof Builder) {
      $this->mode = 'query';
      $this->query = $data;
      $this->table = $data->getQuery()->from;
      $this->items = Form::addGeneralInfo(
        $items,
        $this->params->get('idColumn', true),
        $this->params->get('cDateColumn', true),
        $this->params->get('uDateColumn', Schema::hasColumn($this->table, 'uDate'))
      );

      $this->addExtraUserColumns($this->params->userItems);
      $this->flat = (new Form($this->items))->grid();
      $this->flat = $this->addGeneralActions($this->flat);
    }
    elseif (is_array($data)) {
      $this->mode = 'data';
      $this->data = $data;
      $this->items = $items;
      $this->flat = (new Form($this->items))->grid();

    }
    $this->flat = ($visible = $this->params->visible) ? array_only($this->flat, array_merge($visible, ['_show', '_edit', '_destroy'])) : $this->flat;

    // DETERMINING THE REQUESTED OUTPUT FORMAT
    $this->format = request()->get('format');
    if (!$this->format || !in_array($this->format, $this->getExportFormats(true))) {
      $this->format = 'display';
    }
    //    if (method_exists($query, 'getTable')) {
    //      $this->table = $query->getTable();
    //    }
    //    elseif (method_exists($query, 'getQuery')) {
    //      $this->query = $query->getQuery();
    //      $this->table = $this->query->from;
    //    }
    //    else {
    //      $this->table = $query->from;
    //    }


    //    $this->flat = Form::getFlatFields($this->items, true);
    foreach ($this->flat as $id => $item) {

      if ($item->isSearchable()) {
        $this->filters[] = $id;
      }

      if ($item->type == 'action' && $this->format == 'display' && strpos($item['id'], '.') == false) {
        $this->actions[$item->id] = $item;
        $this->columns[] = $id;
      }
      elseif ($item->isGridable()) {
        if ($item['type'] != 'action') {
          $this->labels[] = $item['label'];
        }

        $this->columns[] = $id;

        if ($item->isQueryable() && $this->query && (!strpos($item->id, '.') or $item->jsonColumn)) {
          $this->selects[] = $item->selectString($this->table, false, $item->jsonColumn);
        }

        if ($item->sortable !== false) {
          //          $item['sortField'] = $item->getSortField()->render();
        }
      }
    }
  }

  private function addGeneralActions($items)
  {
    $base = $this->params->get('base');

    if ($this->params->get('destroyable', false)) {
      $destroy = field([
        'type'       => 'action',
        'id'         => '_destroy',
        'label'      => __('formule::word.delete'),
        'renderCell' => function ($id) use ($base) {
          return view('formule::button.delete', ['action' => "$base/$id", 'iconOnly' => true])->render();
        },
      ]);
      $items = array_prepend($items, $destroy, '_destroy');
    }

    if ($this->params->get('editable', false)) {
      $edit = field([
        'type'       => 'action',
        'id'         => '_edit',
        'label'      => __('formule::word.edit'),
        'renderCell' => function ($id) use ($base) {
          return field([
            'type'  => 'button',
            'icon'   => 'fa-pencil',
            'title' => __('formule::word.edit'),
            'href'  => "$base/$id/edit",
          ]);
        },
      ]);
      $items = array_prepend($items, $edit, '_edit');
    }

    if ($this->params->get('showable', true)) {
      $show = field([
        'type'       => 'action',
        'id'         => '_show',
        'label'      => __('formule::word.view'),
        'renderCell' => function ($id) use ($base) {
          return field([
            'type'  => 'button',
            'icon'   => 'fa-eye',
            'title' => __('formule::word.view'),
            'href'  => "$base/$id",
          ]);
        },
      ]);
      $items = array_prepend($items, $show, '_show');
    }

    return $items;
  }

  /**
   * a simple method that returns all available output formats.
   * @param bool $onlyExtensions
   * @return mixed
   */
  private function getExportFormats($onlyExtensions = false)
  {
    $formats = [
      'display' => __('formule::word.display'),
      //      'xls'     => 'اکسل ۹۵+ (XLS)',
      'xlsx'    => 'Excel 2007+ (XLSX)',
      'csv'     => 'CSV',
      'json'    => 'JSON',
      'html'    => 'Web page',
      'pdf'     => 'PDF',
    ];

    //    $formats = [
    //      'xls'  => [
    //        'label' => 'اکسل ۹۵+',
    //        'icon'  => 'file excel outline',
    //      ],
    //      'xlsx' => [
    //        'label' => 'اکسل ۲۰۰۷+',
    //        'icon'  => 'file excel outline',
    //      ],
    //      'json' => [
    //        'label' => 'جسون',
    //        'icon'  => 'file code outline',
    //      ],
    //      'html' => [
    //        'label' => 'صفحه وب',
    //        'icon'  => 'file code outline',
    //      ],
    //      'pdf'  => [
    //        'label' => 'پی‌دی‌اف',
    //        'icon'  => 'file pdf outline',
    //      ],
    //      'csv'  => [
    //        'label' => 'جداشده با ویرگول',
    //        'icon'  => 'file text outline',
    //      ],
    //    ];

    if ($onlyExtensions) {
      return array_keys($formats);
    }

    return field([
      'type'      => 'select',
      'id'        => 'format',
      'items'     => $formats,
      'emptyItem' => false,
      'label'     => __('formule::word.exportFormat'),
      'cls'       => 'item',
    ])->render();
  }

  /**
   * send grid in various formats to the end-users
   * counterpart of Form::show() method
   * @return mixed
   */
  public function show()
  {
    $form = null;
    $columns = array_only($this->flat, $this->columns);
    if ($this->mode == 'query') {
      $form = (new Form($this->items, ['data' => request()->get('filter'), 'visible' => $this->params->visible], 'filter'))->render();
    }

    // FETCHING VALUES
    $rows = $this->data ?: $this->fetch();
    $results = [];
    // FORMATTING THE VALUES
    foreach ($rows as $i => $row) {
      //      foreach ($row as $name => &$value) {
      //        $value = $this->renderCell($name, $value, $row);
      //      }
      foreach ($this->flat as $id => $column) {
        if ($column->type != 'action' && $column->isGridable() && $column->generated !== true and !strpos($column['id'], '.') || $column->jsonColumn) {
          $results[$i][] = $this->renderCell($column, $row[$column->id], $row);
        }
        elseif ($column->gridable !== false and $column->type == 'action' || $column->generated === true) {
          if ($column->has('renderCell')) {
            $func = $column->get('renderCell');
            $results[$i][] = $func($row['id'], $row);
          }
        }
      }
      if (isset($row['dDate']) && $row['dDate']) {
        $results[$i]['class'] = 'deleted';
      }
    }

    // RETURNING VIEW - this request()->ajax() is for not returning other formats in ajax requests
    if ($this->format == 'display' or request()->ajax()) {
      title($this->pageTitle);
      $params = $this->params;
      $formats = $this->getExportFormats();
      $title = $this->pageTitle;
      $action = $this->params['base'];
      $method = 'GET';
      //      if ($this->query instanceof Builder) {
      //        $results = ary($results)->take(150)->toArray();
      //      }
      if (request()->ajax()) {
        $view = 'formule::grid.tbody';
        $this->format = 'display';
      }
      elseif ($this->params->justTable) {
        $view = 'formule::grid.table';
        $this->format = 'display';
      }
      else {
        $view = 'formule::grid.grid';
      }

      $findFields = [];
      foreach ($this->flat as $field) {
        if ($field->findable && !str_contains($field->id, '.')) {
          $findFields[] = $field->label;
        }

      }
      $findColumnsTitles = implode('، ', $findFields);
      $mode = $this->mode;
      $msg = $this->params['msg'];
      $cls = $this->params->cls;
      $layout = $this->params->layout;

      //      $count = $this->query->get()->count();

      return view($view, compact(
        'action',
        'cls',
        'columns',
        'findColumnsTitles',
        'form',
        'formats',
        'layout',
        'method',
        'mode',
        'msg',
        'params',
        'results',
        'title'
      ));
    }

    // RETURNING JSON
    if ($this->format == 'json') {
      header("Content-Disposition: attachment; filename='{$this->pageTitle}.json'");

      return json_encode($rows, JSON_UNESCAPED_UNICODE);
    }

    // RETURNING XML,XLST,CSV,HTML
    //    $this->labels = array_pluck($columns, 'label');
    $results = array_prepend($results, $this->labels);

    $title = removeEmojis($this->pageTitle);
    //removing action columns
    $results = $this->removeActions($results);

    return Excel::create($title, function ($excel) use ($results, $title) {
      $excel
        ->setTitle($title)
        ->setCreator(utf16(__('formule::word.productName') . ': ' . cuser()->fullName))
        ->setCompany(__('formule::word.companyName'))->setDescription('این فایل به وسیله سامانه یکپارچه فرهنگی مهر تولید شده است.')
        // excel does not support long sheet titles
        ->sheet(mb_substr($title, 0, 31), function ($sheet) use ($results) {
          $sheet
            ->fromArray($results, null, 'A1', false, false)
            ->setAutoFilter()
            ->row(1, function ($row) {
              $row->setFontWeight('bold');
            });
        });
    })->export($this->format);

  }

  /**
   * alias for grid show function
   * @deprecated
   * @return mixed
   */
  public function render()
  {
    return $this->show();
  }

  /**
   * fetch the grids data from db
   * @return mixed
   */
  private function fetch()
  {
    if (request('find')) {
      $this->find($this->query, request('find'));
    }
    else {
      ModelTrait::filter($this->query, $this->flat, request()->get('filter', []));
      //$this->query->filter($this->flat, request()->get('filter', []));
    }
    $query = $this->query->select($this->selects);
    if (Schema::hasColumn($query->getQuery()->from, 'dDate')) {
      $query->addSelect($query->getQuery()->from . '.dDate');
    }
    $this->total = $query->count();
    $rows = $query->limit(150)->get()->toArray();

    return $rows;
  }

  public function find($query, $find)
  {
    // اگر جدول شامل کاربر باشد برای فراهم کردن جستجوی سریع با نام و نام خانوادگی به ازاری هر ستون کاربری با جدول کاربران پیوند انجام می‌شود.
    $table = $query->getQuery()->from;
    foreach ($this->flat as $field) {
      if ($field->type == 'user' && $field->findable) {
        $query = $query->leftJoin("user as {$field->id}User", "$table.$field->id", '=', "{$field->id}User.id");
      }
      if ($field->joinTable) {
        $query = $query->leftJoin("{$field->joinTable} as {$field->id}Table", "$table.$field->id", '=', "{$field->id}Table.id");
      }
    }

    $query->where(function ($query) use ($find, $table) {
      foreach ($this->flat as $field) {
        if (!$field->findable || str_contains($field->id, '.')) {
          continue;
        }
        $select = $field->selectString($table, true);
        $query->orWhere($select, 'like', '%' . $find . '%');
        if (in($field->id, 'firstName', 'lastName') && Schema::hasColumn($table, 'firstName') && Schema::hasColumn($table, 'lastName') or $field->type == 'user') {
          $joinedTable = $field->type == 'user' ? "{$field->id}User" : 'user';
          $query->orWhere(DB::raw("concat($joinedTable.firstName,' ',$joinedTable.lastName)"), 'like', "%$find%");
        }
        if ($field->joinTable) {
          $query->orWhere($field->joinedTableColumn ? "{$field->id}Table.{$field->joinedTableColumn}" : "{$field->id}Table.name", 'like', "%$find%");
        }
      }
    });
  }

  /**
   * make cell values human-readable if necessary.
   * @param $column
   * @param $value
   * @param $row
   * @return mixed
   */
  private function renderCell($column, $value, $row)
  {
    //    if ($value === null || $value === '') {
    //      return $value;
    //    }

    $cell = $column;

    if ($this->format == 'display') {

      if ($cell->has('renderCell')) {
        $func = $cell->get('renderCell');

        return $func($value, $row);
      }
      if (method_exists($cell, 'renderCell')) {
        return $cell->renderCell($value, $row);
      }

    }
    //for the renderValue in the item
    if ($cell->has('renderValue')) {
      $func = $cell->get('renderValue');

      return $func($value, $row);
    }
    //for renderValue in the class of item's type
    if (method_exists($cell, 'renderValue')) {
      return $cell->renderValue($value, $row);
    }

    return $value;
  }

  /**
   * this function removes the show , edit and delete action from output of grid
   * @param $result
   * @return mixed
   */
  public static function removeActions($result)
  {
    foreach ($result as &$row) {
      foreach ($row as $key => $item) {
        if (is_object($item)) {
          $row = array_except($row, $key);
        }
      }
    }

    return $result;
  }

  // If this grid has relation to user table we can add some user columns to it if required.
  private function addExtraUserColumns($userItems)
  {
    if ($userItems) {
      #this join is handled in views for handling it here and not in view some changes are needed
      //        $userColumn = $userColumn ?: 'user';
      //        $this->query->join('user', "{$this->table}.{$userColumn}", '=', 'user.id');
      $userFormItems = array_only(form(self::$allUserItems)->flat(), (array)$userItems);

      $userFields = [];
      foreach ($userFormItems as $id => $item) {
        $userFields[] = $item->originalItems->toArray();
      }
      $container = ['label' => __('formule::word.userDetails'), 'type' => 'fieldset', 'items' => $userFields];

      if (isset($this->items['type']) && $this->items['type'] == 'fieldset') {
        $this->items['items'] [] = $container;
      }
      else {
        $this->items[] = $container;
      }
    }
  }

  public static function setAllUserItems(array $items)
  {
    self::$allUserItems = $items;
  }
}