<?php //declare(strict_types=1);

use Carbon\Carbon;
use farhadi\IntlDateTime;
use Formule\Form\Combo;
use Formule\Form\Form;
use Formule\Grid\Grid;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

#region Sundry helpers
/**
 * reformats an array in a specific format
 * @param      $array
 * @param      $parents
 * @param bool $light
 * @return array
 */
function tableToTree(array $array, array $parents, bool $light = false): array
{
  $result = [];
  $p = $parents;
  foreach ($array as $k => $row) {
    $row = (array)$row;
    $result[$row[$p[0]]][$row[$p[1]]][] = $light ? $row[$p[2]] : $row;
  }
  return $result;
}

/**
 * Produce a valid path by appending all sent parameters to each other.
 * @return mixed
 */
function path()
{
  $paths = [];

  foreach (func_get_args() as $arg) {
    if ($arg !== '') {
      $paths[] = $arg;
    }
  }

  return preg_replace('#[/\\\]+#', DIRECTORY_SEPARATOR, join(DIRECTORY_SEPARATOR, $paths));
}

/**
 * if value is closure runs it and returns it
 * @param $var
 * @return array|mixed|value
 */
function val($var)
{
  if (is_array($var)) {
    return array_map('value', $var);
  }

  return value($var);
}

/**
 * receives a dotted string and returns it in html array format
 * for example parent.name to parent[name]
 * @param $input
 * @return mixed|string
 */
function dotToArray($input)
{
  return preg_replace('/\.([^.]+)/', "[$1]", $input);

  //  if (strpos($input, '.') === false) {
  //    return $input;
  //  }
  //  $input = str_replace_first('.', '[', $input);
  //  $input = str_replace('.', '][', $input);
  //
  //  return $input . ']';

}

/**
 * places associative array in an index array
 * @param $items
 * @return array
 */
function itemize($items)
{
  return (array)\f\arr::toIndexArray($items);
}

/**
 * A simple shorthand for in_array function
 * @param       $needle
 * @param array ...$haystack
 * @return bool
 */
function in($needle, ...$haystack): bool
{
  return in_array($needle, $haystack);
}

/**
 * a shortcut for throwing exception
 * @param string         $message
 * @param int            $code
 * @param Exception|null $previous
 * @throws Exception
 */
function exception(string $message = "App Exception", int $code = 0, Exception $previous = null)
{
  throw new \Exception($message, $code, $previous);
}

/**
 * Check if a number interval has intersection with and array of intervals or a single other pair.
 * @param       $start
 * @param       $end
 * @param array ...$args can be ether a pair of other intervals (3nd and 4th arguments) or an array of min/max pair of
 *                       intervals. e.g. (1,3,4,2) or (1,3,[[2,4],[3,5],[3,51])
 * @return bool|int|string the id of intersected interval or `false` if all no intersection.
 */
function hasOverlap($start, $end, ...$args)
{
  if (!is_array($args[0])) {
    $targets = [[$args[0], $args[1]]];
  }
  else {
    $targets = $args[0];
  }

  foreach ($targets as $i => list($start2, $end2)) {
    if (($end2 === null || $end2 === false || $end2 > $start) && ($end === null || $end === false || $end > $start2)) {
      return $i;
    }
  }

  return false;
}

/**
 * Similar to hasOverlap but accept date strings as input.
 * @param       $start
 * @param       $end
 * @param array ...$args
 * @return bool|int|string
 */
function hasTemporalOverlap($start, $end, ...$args)
{
  $start = strtotime($start);
  $end = strtotime($end);

  if (!is_array($args[0])) {
    $targets = [[$args[0], $args[1]]];
  }
  else {
    $targets = $args[0];
  }

  foreach ($targets as $i => &$target) {
    if (hasOverlap($start, $end, strtotime($target[0]), strtotime($target[1])) !== false) {
      return $i;
    }
  }

  return false;
}

/**
 * converts a dotted array to Multidimensional array
 * @param        $arr
 * @param        $path
 * @param        $value
 * @param string $separator
 */
function assignArrayByPath(&$arr, $path, $value, string $separator = '.')
{
  $keys = explode($separator, $path);

  foreach ($keys as $key) {
    $arr = &$arr[$key];
  }

  $arr = $value;
}

/**
 * returns the first non-NULL value of a list, or NULL if there are no non-NULL values.
 * @param array ...$args
 * @return mixed
 */
function coalesce(...$args)
{
  return \f\dev::coalesce($args);
}

/**
 * getting the height of an image file
 * @param $path
 * @return mixed
 */
function height($path)
{
  if (is_file($path) && $info = getimagesize($path)) {
    return $info[1];
  }
}

/**
 * getting the width of an image file
 * @param $path
 * @return mixed
 */
function width($path)
{
  if (is_file($path) && $info = getimagesize($path)) {
    return $info[0];
  }
}

function removeEmojis($string)
{
  return preg_replace('#[^\pL\pZ\pN]#u', '', strip_tags($string));
}

function removeEmojis2($string)
{
  return preg_replace('/([0-9|#][\x{20E3}])|[\x{00ae}|\x{00a9}|\x{203C}|\x{2047}|\x{2048}|\x{2049}|\x{3030}|\x{303D}|\x{2139}|\x{2122}|\x{3297}|\x{3299}][\x{FE00}-\x{FEFF}]?|[\x{2190}-\x{21FF}][\x{FE00}-\x{FEFF}]?|[\x{2300}-\x{23FF}][\x{FE00}-\x{FEFF}]?|[\x{2460}-\x{24FF}][\x{FE00}-\x{FEFF}]?|[\x{25A0}-\x{25FF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{FE00}-\x{FEFF}]?|[\x{2900}-\x{297F}][\x{FE00}-\x{FEFF}]?|[\x{2B00}-\x{2BF0}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F6FF}][\x{FE00}-\x{FEFF}]?/u', '', strip_tags($string));
}

/**
 * adds \\ before brackets
 * @param string $string
 * @return string
 */
function javascriptName(string $string): string
{
  $string = str_replace('[', '\\\\[', str_replace(']', '\\\\]', $string));

  return $string;
}

/**
 * checks a value in an array with this format
 * [
 * 'publish'=>[1,3,5]
 * 'full'=>[4,9]
 * ]
 * in this example if $needle==4 the function returns 'full'
 * @param array $haystack
 * @param       $needle
 * @return bool|int|string
 */
function findKeyByValue(array $haystack, $needle)
{
  return \f\arr::findKey($haystack, $needle);
}

/**
 * mutually exclusive (ME) and collectively exhaustive (CE)
 * receiving a needle and an associative array of truthies or falsies values, decides whether the needle is true or
 * false
 * @param $needle
 * @param $truthies array|bool
 * @param $falsies  array|bool
 * @return bool
 */
function mece($needle, $truthies, $falsies): bool
{
  if (is_array($truthies) && is_array($falsies)) {
    trigger_error('Only one of truthies or falsies params can be array', E_ERROR);
  }

  if (!is_array($truthies) && !is_array($falsies)) {
    return (bool)$truthies;
  }

  $return = is_array($truthies); // does the needle will be searched on truthies or falsies
  $haystack = $return ? $truthies : $falsies;

  return in_array($needle, $haystack) ? $return : !$return;
}

function getProperties($class, $access = \ReflectionProperty::IS_PUBLIC): array
{
  $properties = (new \ReflectionObject($class))->getProperties($access);

  return array_column($properties, 'name');
}

function tpl($file, $vars = [])
{
  ob_start();
  extract($vars);
  require $file;
  $output = ob_get_contents();
  ob_end_clean();

  return $output;
}

#endregion

#region Views and rendering

#region L18n

function lang($check = null)
{
  $lang = \Locale::getPrimaryLanguage(\Locale::getDefault());

  if ($check) {
    return $lang == strtolower($check);
  }

  return $lang;
}

function direction($direction = null)
{
  $dir = lang('fa') ? 'rtl' : 'ltr';
  if ($direction) {
    return $dir == $direction;
  }

  return $dir;
}

function region($check = null)
{
  $region = \Locale::getRegion(\Locale::getDefault());

  if ($check) {
    return $region == strtoupper($check);
  }

  return $region;
}

function getLocales()
{
  return ResourceBundle::getLocales('');
}

/**
 * format a date according to the app locale
 */
function d($date)
{
  $date = (string)$date;
  if (config('app.locale') == 'en_IR') {
    return toSolar($date);
  }

  return date('d/m/Y', strtotime($date));
}

#endregion

#region urls and paths

/**
 * returns path of a file in bower directory
 * @param $file
 * @return string
 */
function bower(string $file): string
{
  if (app()->env == 'local') {
    return asset('bower_components/' . $file);
  }

  return asset('dist/' . $file);
}


#endregion

/**
 * Returns humane readalbe text of a constants.
 * @param       $cat
 * @param array $value the value of cnst
 * @return mixed|string
 */
function cnst(string $cat, ...$value)
{
  if (!$value) {
    return app('constants')[$cat];
  }
  if ($value[0] && isset(app('constants')[$cat]) && isset(app('constants')[$cat][$value[0]])) {
    return app('constants')[$cat][$value[0]];
  }

  return __('formule::word.unknown');

}

/**
 * جمع یک لغت را باز می گرداند
 * @param $wordGroup
 * @return mixed|string
 */
function plural($wordGroup)
{
  if (gettype($wordGroup) != 'string') {
    return $wordGroup;
  }
  if (str_contains($wordGroup, ' ') !== false) {
    return preg_replace('# #u', '‌های ', $wordGroup, 1);
  }

  return $wordGroup . '‌ها';
}

/**
 * ریال اضافه می کند
 * @param $n
 * @return string
 */
function mn(string $n)
{
  return $n . ' ریال';
}


/**
 * Get either a Gravatar URL or complete image tag for a specified email address.
 *
 * @param string     $email The email address
 * @param int|string $s     Size in pixels, defaults to 80px [ 1 - 2048 ]
 * @param string     $d     Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
 * @param string     $r     Maximum rating (inclusive) [ g | pg | r | x ]
 * @param bool|boole $img   True to return a complete IMG tag False for just the URL
 * @param array      $atts  Optional, additional key/value attributes to include in the IMG tag
 * @return String containing either just a URL or a complete image tag
 * @source http://gravatar.com/site/implement/images/php/
 */
function get_gravatar(string $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = [])
{
  $url = 'http://www.g ravatar.com/avatar/';
  $url .= md5(strtolower(trim($email)));
  $url .= "?s=$s&d=$d&r=$r";
  if ($img) {
    $url = '<img src="' . $url . '"';
    foreach ($atts as $key => $val) {
      $url .= ' ' . $key . '="' . $val . '"';
    }
    $url .= ' />';
  }

  return $url;
}

/**
 * returns the current User Model
 * @return \App\Models\User|\Formule\Nil|\Illuminate\Contracts\Auth\Authenticatable
 */
function cuser()
{
  if (auth()->check()) {
    return auth()->user();
  }

  return new \Formule\Nil();
}

/**
 * redirects user back notice that this does not do authorize
 * @return \Illuminate\Http\RedirectResponse
 */
function authorize()
{
  $backUrl = auth()->check() ? route('dashboard') : route('login');

  return redirect($backUrl)->with('messages', [['text' => 'شما اجازه دسترسی به این صفحه را ندارید.', 'type' => 'warning']]);
}

/**
 * Generate a human friendly date (e.g. two day ago)
 * @param $time string ISO date string
 * @return string
 */
function hdate($time): string
{
  return Carbon::createFromTimestamp(strtotime($time))->diffForHumans();
}

/**
 * places $var in array
 * @param $var
 * @return array
 */
function placeInArray($var)
{
  if (gettype($var) == 'object') {
    return [$var];
  }

  return (array)$var;
}

/**
 * cleans a html code
 * @param $html
 */
function tidy($html)
{
  $options = [
    'show-body-only'      => true,
    'indent'              => true,
    'vertical-space'      => true,
    'drop-empty-elements' => false,
    'wrap'                => 0,
    //            'output-xhtml' => true,
  ];

  return tidy_parse_string($html, $options)->value;
}

/**
 * checks a function is called or not
 * @param $function
 * @return bool
 */
function isCalled($function)
{
  return in_array($function, array_pluck(debug_backtrace(), 'function'));
}

/**
 * Check if the current request originated from the server itself.
 * @return bool
 */
function isLocal(): bool
{
  return $_SERVER['REMOTE_ADDR'] == '127.0.0.1';
}

/**
 * Check if a user request parameter is provided e.g. un-sent values: null,'',[], ['',null]
 * @param $param
 * @return bool
 */
function exists($param): bool
{
  return \f\dev::exists($param);
}

function utf16($string)
{
  return mb_convert_encoding($string, 'UCS-2');
}

/**
 * for migrating information form data column in database
 * @param $models
 * @param $column
 * @param $name
 */
function copyInfoColumn($models, $column, $name)
{
  foreach ($models as $model) {
    $data = $model->data;
    if (array_key_exists($column, (array)$data)) {
      $model[$name] = $data[$column];
      unset($data[$column]);
      $model->data = $data;
      $model->save();
    }
  }
}

#endregion

#region Date and Time
Carbon::setLocale('fa');

/**
 * detect calendar type (Solar/Gregorian) and format/convert it to the Gregorian ISO format
 * @param      $time
 * @param bool $detectCalendar
 * @return array|string converted gregorian date
 */
function toIso($time, $detectCalendar = false)
{
  if (is_array($time)) {
    return array_map('toIso', $time);
  }

  // if only year is given we convert it to the first day of the corresponding year.
  if (preg_match('#^\d+$#', $time)) {
    $time .= '-01-01';
  }

  $calendar = $detectCalendar && date('Y', strtotime($time)) > 1500 ? 'gregorian' : 'persian';
  $date = new IntlDateTime($time, null, $calendar);

  return $date->classicFormat('Y-m-d H:i:s');
}

/**
 * A utility function to Convert an ISO date to solar hijri.
 * @param      $time
 * @param bool $split
 * @return array|string
 */
function toSolar($time, bool $split = false)
{
  if (is_array($time)) {
    return array_map('toSolar', $time);
  }

  if (!exists($time)) {
    return $time;
  }

  $date = new IntlDateTime($time);
  $date->setCalendar('persian');

  $format = date('H:i:s', strtotime($time)) == "00:00:00" ? 'yyyy/MM/dd' : 'yyyy/MM/dd HH:mm:ss';

  if ($split) {
    return ['time' => $date->format('H:mm:ss'), 'date' => $date->format('YYYY-MM-dd')];
  }

  return $date->format($format);
}

/**
 * returns a styled format of dateTile
 * @param null $time
 * @return string|void
 */
function solarTag($time = null): string
{
  if (!$time) {
    return '';
  }
  $solar = toSolar($time);

  return "<time class=date datetime='$time' title='$time'>$solar</time>";
}

/**
 * returns a standard format of dateTime
 * @param int  $timestamp
 * @param bool $dashed
 * @return bool|string
 */
function isoDate(int $timestamp = null, bool $dashed = false)
{
  $timestamp = $timestamp ?? time();
  $format = $dashed ? "Y-m-d-H-i-s" : "Y-m-d H:i:s";

  return date($format, $timestamp);
}

/**
 * A shorthand for Carbon object creation
 * @param null $time
 * @param null $tz
 * @return Carbon
 */
function t($time = null, $tz = null): Carbon
{
  return new \Carbon\Carbon($time, $tz);
}

function calendar($calendar, $time)
{
  $date = new IntlDateTime($time);
  $date->setCalendar($calendar);
  return $date->classicFormat('Y-m-d H:i:s');
}

#endregion

#region Formule/Laravel helpers

/**
 * receives items and make a field from it
 * @param $field
 * @return \Formule\Form\Control
 */
function field($field)
{
  return Form::makeField(ary($field));
}

/**
 * receives items and query and returns rendered grid
 * @param array $items
 * @param       $query
 * @param array $params
 * @return View
 */
function grid(array $items, $query, $params): View
{
  $grid = new Grid($items, $query, $params);

  return $grid->show();
}

/**
 * returns a object of form that in _toString function will be rendered
 * @param array|string $items
 * @param array        $params
 * @param string       $mode
 * @return Form
 */
function form($items, array $params = [], string $mode = 'edit'): Form
{
  return new Form($items, $params, $mode);
}

/**
 * returns a json response that will be used in clientSide
 * @param null   $response
 * @param int    $responseCode
 * @param string $type
 */
function jsonResponse($response = null, int $responseCode = 200, string $type = 'application/json; charset=utf-8')
{
  http_response_code($responseCode);
  header('Content-Type: ' . $type);
  echo json_encode($response, JSON_UNESCAPED_UNICODE);
  exit();
}

/**
 * returns a formatted response for combos in clientSide
 * @param        $result
 * @param string $text
 * @param string $id
 * @return array
 */
function comboResponse(array $result, $text = 'text', $id = 'id'): array
{
  return Combo::formatResponse($result, $text, $id);
}

/**
 * a shortcut for Gate::allows
 * @param string $ability
 * @param array  $arguments
 * @return bool
 */
function can(string $ability, $arguments = []): bool
{
  return Gate::allows($ability, $arguments);
}

/**
 * A helper method to add cDate and uDate when creating new tables via migration.
 * @param \Illuminate\Database\Schema\Blueprint $table
 * @param bool                                  $uDate
 * @param bool                                  $cDate
 * @param bool                                  $dDate
 */
function addTimestamps(\Illuminate\Database\Schema\Blueprint $table, bool $uDate = true, bool $cDate = true, bool $dDate = false)
{
  if ($cDate) {
    $table->timestamp('cDate')->default(DB::raw('CURRENT_TIMESTAMP'));
  }
  if ($uDate) {
    $table->timestamp('uDate')->nullabel()->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
  }
  if ($dDate) {
    $table->timestamp('dDate')->nullabel();
  }
}


/**
 * returns string for ng-show or ng-hide for multiple values
 * @param string $ngModel
 * @param array  $values
 * @param bool   $assoc
 * @return string
 */
function ngInArray(string $ngModel, array $values, bool $assoc = false)
{
  $array = json_encode($values);
  if ($assoc) {
    $array = str_replace('"', '\'', $array);

    return "$array.indexOf($ngModel)!=-1";
  }

  return "$array.indexOf(+$ngModel)!=-1";
}

function meta($meta = null, $value = null, $name = 'name')
{
  if ($meta) {
    app('seotools.metatags')->addMeta($meta, $value, $name);
  }

  return app('seotools');
}

function title($title)
{
  return meta()->setTitle($title);
}


/**
 * Use this function to including any assets that need to be processed before publishing e.g. minification
 * @param string $file
 * @return string
 */
function stuff(string $file): string
{
  if (app()->env == 'local') {
    return asset($file);
  }

  return asset('dist/build/' . $file);
}

/**
 * checks overlap of start and end dates with query
 * @param        $startDate the start date that will be checked with other dates
 * @param        $endDate   the start date that will be checked with other dates
 * @param        $query
 * @param string $startDateColumn
 * @param string $endDateColumn
 * @return bool
 */
function hasTemporalOverlapByQuery($startDate, $endDate, $query, $startDateColumn = 'endDate', $endDateColumn = 'endDate')
{
  $dates = [];
  //formatting data of councils for hasOverlap function
  foreach ($query->get() as $k => $v) {
    $dates[$v['id']] = [$v['startDate'], $v['endDate']];
  }

  //  if(!$query->count()){
  //    return false;
  //  }
  if ($id = hasTemporalOverlap($startDate, $endDate, $dates)) {
    return $query->find($id);
  }

  return false;
}

/**
 * @param       $name
 * @param array $params
 * @return mixed
 * @throws Exception
 * @deprecated use string for the first params of form() method instead.
 */
function items($name, $params = [])
{
  return Form::getItems($name, $params);
}

/**
 * Intended to be used on in the Laravel migrations.
 * @param $table
 */
function commonDates($table)
{
  $table->timestamp('cDate')->default(\DB::raw('CURRENT_TIMESTAMP'));
  $table->timestamp('uDate')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
}
#endregion