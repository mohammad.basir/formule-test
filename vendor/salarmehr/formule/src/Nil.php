<?php

namespace Formule;

class Nil
{
    function __call($name, $arguments)
    {
        return false;
    }

    function __get($name)
    {
        return null;
    }
}

