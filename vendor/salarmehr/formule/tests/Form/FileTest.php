<?php

namespace Formule\Test\Form;


use Formule\Test\TestCase;

class FileTest extends TestCase
{
  protected function setUp()
  {
    require_once "__DIR__../../sandbox.php";
  }

  public function testRender()
  {
    $file = field(['type' => 'file', 'id' => 'test'])->render();
    $this->assertContains('test-upload-box', $file);
  }

  public function testRenderFilter()
  {
    $file = field(['type' => 'file', 'id' => 'test'])->renderFilter();
    $this->assertContains('مهم نیست', $file);
  }

  public function testRenderReadonly()
  {

  }
}