<?php
/**
 * Created by PhpStorm.
 * User: Mehdi
 * Date: 2/26/2018
 * Time: 4:57 PM
 */

namespace Formule\Test\Form;

use Formule\Test\TestCase;
class InfoTest extends TestCase
{

  public function testRender()
  {
    require_once "__DIR__../../sandbox.php";
    $info = field(['type' => 'info' , 'id'=> 'test'])->render();
    $this->assertContains('formule-control-static', $info);
  }
}
