<?php

namespace Formule\Test\Form;

use Formule\Test\TestCase;

class AttachmentsTest extends TestCase
{
  public function testRender()
  {
    $attach = field(['type' => 'attachments'])->render();
    $this->assertContains("fileupload-buttonbar" , $attach);

  }

  public function testRenderReadonly()
  {

  }
}
