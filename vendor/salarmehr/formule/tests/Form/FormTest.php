<?php

namespace Formule;

use Formule\Test\TestCase;
use Illuminate\Validation\Validator;
use Illuminate\View\View;

class FormTest extends TestCase
{

  function testEmptyForm()
  {
    return $this->assertEquals(form([])->render(), '');
  }

  function testFormWithAHtmlControl()
  {
    return $this->assertEquals(form(['html'])->render(), 'html');
  }

  function testFileLoading()
  {
    return $this->assertStringContainsStringIgnoringCase('named form', form('named')->render());
  }

  public function testAddGeneralInfo()
  {
    $info = form([])::addGeneralInfo([]);
    $this->assertInternalType('array' , $info);
    $this->assertEquals('fieldset' , $info['type']);
  }

  public function testInValidator()
  {
    $items = form([])::inValidator(field(['items' => ['one' , 'two' , 'three'] ]));
    $this->assertEquals('one,two,three' ,$items );

    $items = form([])::inValidator(field(['items' => ['one' => 1 , 'two' => 2 , 'three' => 3] ]));
    $this->assertEquals('one,two,three' ,$items );
  }

  public function testGetItems()
  {

  }

  /**
   * @dataProvider renderProvider
   */
  public function testRender($type , $expected)
  {
    $render = form(['type' => $type , 'id' => 'test' , 'items' => ['item1' , 'item2']])->render();
    $this->assertContains($expected,$render);
  }
  public function renderProvider()
  {
    return  [
      ['radio' , 'radio-inline'],
      ['file' , 'fileinput-button'],
    ];
  }

  /**
   * @dataProvider subRenderProvider
   */
  public function testSubRender($type , $expected)
  {
    $actual = form([])->subRender(ary(['items' =>['type' => $type , 'id' => 'ID']]));
    $this->assertContains($expected , $actual);
  }

  public function subRenderProvider()
  {
    return [
      ['file' , 'fileupload'],
      ['number' , 'form-control'],
    ];
  }

  public function testValidator()
  {
    $form = form([])->validator([]);
    $this->assertInstanceOf(Validator::class , $form);
  }

  public function testFlat()
  {
    $form = form(['id' => 'id' , 'type' => 'number']);
    $this->assertInternalType('array' , $form->flat());
  }

  /**
   * @dataProvider  makeFieldProvider
   */
  public function testMakeField($type , $id , $expected)
  {
    $array = ['type' => $type, 'id' => $id];
    $ary = ary($array);
    $this->assertContains($expected , form([])::makeField($ary));
  }

  public function makeFieldProvider()
  {
    return [
      ['select' , 'selectId' , 'select'],
    ];
  }

  public function testGetFields()
  {
    $fields = form([])->getFields(field([ 'id' => 'ID', 'gridable' =>true]));
    $this->assertInternalType('array', $fields);
    $this->assertContains(['gridable' => true],$fields['ID']);
  }

  public function testGrid()
  {
    $form = form(['id' => 'id' , 'type' => 'number']);
    $this->assertInternalType('array' , $form->grid());
  }

  public function test__toString()
  {

  }

  public function testShow()
  {
    $form = form(['id' => 'id' , 'type' => 'number']);
    $this->assertInstanceOf(View::class , $form->show());
  }

  public function testHumanize()
  {

  }
}