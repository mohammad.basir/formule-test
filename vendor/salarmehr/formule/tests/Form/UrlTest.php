<?php
/**
 * Created by PhpStorm.
 * User: Mehdi
 * Date: 2/26/2018
 * Time: 1:53 PM
 */

namespace Formule\Test\Form;

use Formule\Test\TestCase;
class UrlTest extends TestCase
{

  public function testRender()
  {
    require_once "__DIR__../../sandbox.php";
    $url = field(['type' => 'url'])->render();
    $this->assertContains('form-control', $url);
  }
}
