<?php
/**
 * Created by PhpStorm.
 * User: Mehdi
 * Date: 2/26/2018
 * Time: 2:25 PM
 */

namespace Formule\Test\Form;

use Formule\Test\TestCase;
class TextareaTest extends TestCase
{
  protected function setUp()
  {
    require_once "__DIR__../../sandbox.php";
  }

  public function testRender()
  {
    $text = field(['type' => 'textarea'])->render();
    $this->assertContains("</textarea>" , $text);
  }

  public function testRenderFilter()
  {
    $field = field(['type' => 'textarea']);
    $this->assertContains("type='text'", $field->renderFilter());
  }
}
