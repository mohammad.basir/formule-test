<?php
/**
 * Created by PhpStorm.
 * User: Mehdi
 * Date: 2/26/2018
 * Time: 10:21 AM
 */

namespace Formule\Test\Form;

use Formule\Test\TestCase;
class HtmlTest extends TestCase
{

  public function testRender()
  {
    require_once "__DIR__../../sandbox.php";
    $expected = "<a></a>";
    $actual = field(['type' => 'html' , 'html' => $expected])->render();
    $this->assertEquals($expected , $actual);
  }
}
