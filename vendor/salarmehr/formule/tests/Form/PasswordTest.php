<?php
/**
 * Created by PhpStorm.
 * User: Mehdi
 * Date: 2/26/2018
 * Time: 1:36 PM
 */

namespace Formule\Test\Form;

use Formule\Test\TestCase;
class PasswordTest extends TestCase
{

  public function testRender()
  {
    require_once "__DIR__../../sandbox.php";
    $password = field(['type' => 'password'])->render();
    $this->assertContains('form-control', $password);
  }
}
