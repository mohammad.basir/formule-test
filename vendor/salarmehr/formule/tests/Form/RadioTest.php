<?php
/**
 * Created by PhpStorm.
 * User: Mehdi
 * Date: 2/26/2018
 * Time: 3:57 PM
 */

namespace Formule\Test\Form;

use Formule\Test\TestCase;
class RadioTest extends TestCase
{

  protected function setUp()
  {
    require_once "__DIR__../../sandbox.php";
  }

  public function testRender()
  {
    $select = field(['type' => 'radio' , 'id' => 'radioId' , 'items' => ['first' , 'second']])->render();
    $this->assertContains('radio-inline', $select);
  }

  public function testRenderFilter()
  {
    $select = field(['type' => 'radio' , 'id' => 'radioId' , 'items' => ['first' , 'second']])->renderFilter();
    $this->assertContains('multiple', $select);
  }
}
