<?php
/**
 * Created by PhpStorm.
 * User: Mehdi
 * Date: 2/26/2018
 * Time: 1:47 PM
 */

namespace Formule\Test\Form;

use Formule\Test\TestCase;
class EmailTest extends TestCase
{

  public function testRender()
  {
    $email = field(['type' => 'email'])->render();
    $this->assertContains('form-control', $email);
    $this->assertContains('email', $email);
  }
}
