<?php

namespace Formule;

use Formule\Test\TestCase;

class ControlTest extends TestCase
{
  public function testIsContainer()
  {
    $field = field(['type' => 'date']);
    $this->assertFalse($field->isContainer());

    $field = field(['type' => 'fieldset']);
    $this->assertTrue($field->isContainer());
  }

  public function testIsField()
  {
    $field = field(['type' => 'date']);
    $this->assertFalse($field->isField());

    $field = field(['type' => 'date', 'id' => 'foo']);
    $this->assertTrue($field->isField());

    $field = field(['type' => 'table', 'id' => 'foo']);
    $this->assertFalse($field->isField());
  }

  public function testSetName()
  {
    $expected = 'userName';
    $field = field(['type' => 'text']);
    $field->setName($expected);
    $this->assertEquals($expected, $field->attrs['name']);
  }

  public function testPreprocess()
  {
    $field = field(['Hello World!']);
    $this->assertEquals($field->type, 'html');

    $field = field(['items' => []]);
    $this->assertEquals($field->type, 'fieldset');

    $field = field('Hello World!');
    $this->assertEquals($field->type, 'html');
  }

  public function testIsEmpty()
  {

  }

  public function testAttrs()
  {
    //    $id =  'dateTime';
    //    $type =  'date' ;
    //    $placeholder = 'Date Time';
    //    $field = field(['id' => $id]);
    //    $actual = $field->attrs(['type' => $type]);
    //    $this->assertEquals("type='{$type}' name='{$id}' id='{$id}' placeholder='{$placeholder}'" , $actual);

  }

  /**
   * @dataProvider clsProvider
   */
  public function testCls($class, $id, $type, $forDate)
  {

    $field = field(['id' => $id, 'type' => $type]);
    $actual = $field->cls($class);
    if (is_array($class)) {
      $class = implode(' ', $class);
    }
    $this->assertEquals('class="' . $class . ' form-control' . $forDate . '"', $actual);
  }

  public function clsProvider()
  {
    return [
      [['red', 'larg', 'soft'], 'title', 'text', ''],
      [['red', 'larg', ''], 'title', 'text', ''],
      ['', 'title', 'text', ''],
      [['red', 'larg', 'soft'], 'book', 'file', ''],
      [['red', 'larg', 'soft'], 'avatar', 'image', ''],
      [['red', 'larg', 'soft'], 'birthday', 'date', ' date input-group'],
    ];
  }

  public function testRenderFilter()
  {

  }

  /**
   * @dataProvider isSearchableProvider
   */
  public function testIsSearchable($false1, $false2, $true1, $true2)
  {
    $field1 = field(['id' => 'test', $false1 => $false2]);
    $this->assertFalse($field1->isSearchable());

    $field2 = field(['id' => 'test', $true1 => $true2]);
    $this->assertTrue($field2->isSearchable());
  }

  public function isSearchableProvider()
  {
    return [
      ['searchable', false, 'searchable', 'evrything except false'],
      ['hidden', true, 'type', 'text'],
      ['id', '[userId]', 'id', 'userId'],
      ['type', 'html', 'type', 'select'],
      ['type', 'action', 'type', 'radio'],
      ['type', 'file', 'empty', true],
    ];
  }

  /**
   * @dataProvider isQueryableProvider
   */
  public function testIsQueryable($false1, $false2, $true1, $true2)
  {
    $field1 = field([$false1 => $false2]);
    $this->assertFalse($field1->isQueryable());

    $field2 = field([$true1 => $true2]);
    $this->assertTrue($field2->isQueryable());
  }

  public function isQueryableProvider()
  {
    return [
      ['column', false, 'empty', true],
      ['hidden', true, 'type', 'text'],
      ['id', '[userId]', 'id', 'userId'],
      ['type', 'html', 'type', 'select'],
      ['type', 'action', 'type', 'radio'],
      ['type', 'tab', 'hidden', 'false'],
    ];
  }

  /**
   * @dataProvider isGridableProvider
   */
  public function testIsGridable($false1, $false2)
  {
    $field1 = field(['id' => 'test', $false1 => $false2]);
    $this->assertFalse($field1->isGridable());
  }

  public function isGridableProvider()
  {
    return [
      ['gridable', false],
      ['id', 'test.'],
      ['type', 'image'],
      ['column', false],
    ];
  }

  public function testIsFormable()
  {
    $field = field(['formable' => false]);
    $this->assertFalse($field->isFormable());

    $field = field(['formable' => true]);
    $this->assertTrue($field->isFormable());
  }

  public function testSelectString()
  {

  }

  public function test__toString()
  {
    //    $field = field(['id' => 'test']);
    //    $this->expectOutputString();
    //    print $field;
  }

  /**
   * @dataProvider getHeaderProvider
   */
  public function testGetHeader($id, $label, $expected)
  {
    $field = field(['id' => $id, 'label' => $label]);
    $this->assertEquals($expected, $field->getHeader());
  }

  public function getHeaderProvider()
  {
    return [
      ['id', 'label', 'label'],
      ['id', '', 'id'],
      ['', 'label', 'label'],
      ['', '', ''],
    ];
  }

  public function testIsHidden()
  {
    $field = field(['hidden' => true]);
    $this->assertTrue($field->isHidden());

    $field = field(['hidden' => false]);
    $this->assertFalse($field->isHidden());
  }

  public function testIsReadonly()
  {

  }

  public function testIsVisible()
  {
    $field = field(['hidden' => true]);
    $this->assertFalse($field->isVisible());

    $field = field(['hidden' => false]);
    $this->assertTrue($field->isVisible());
  }
}