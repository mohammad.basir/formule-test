<?php

namespace Formule\Test\Form;

use Formule\Test\TestCase;
class BooleanTest extends TestCase
{

  protected function setUp()
  {
    require_once "__DIR__../../sandbox.php";
  }

  public function testRender()
  {
    $bool = field(['type' => 'boolean'])->render();
    $this->assertContains('switch', $bool);
  }

  public function testRenderFilter()
  {
    $bool = field(['type' => 'boolean'])->renderFilter();
    $this->assertContains('مهم نیست', $bool);
  }

  public function testRenderCell()
  {
    $bool = field(['type' => 'boolean'])->renderCell(1);
    $this->assertEquals('✔', $bool);

    $bool = field(['type' => 'boolean'])->renderCell(0);
    $this->assertEquals('✖', $bool);
  }

  public function testRenderReadonly()
  {

  }

  public function testRenderCellSingle()
  {
    $bool = field(['type' => 'boolean'])->renderCellSingle(1);
    $this->assertEquals('بله', $bool);

    $bool = field(['type' => 'boolean'])->renderCellSingle(0);
    $this->assertEquals('خیر', $bool);
  }
}
