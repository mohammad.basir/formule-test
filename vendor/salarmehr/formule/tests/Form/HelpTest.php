<?php
/**
 * Created by PhpStorm.
 * User: Mehdi
 * Date: 2/27/2018
 * Time: 11:24 AM
 */

namespace Formule\Test\Form;

use Formule\Test\TestCase;
class HelpTest extends TestCase
{

  public function testRender()
  {
    require_once  "__DIR__../../sandbox.php";
    $help = field(['type' => 'help' , 'id' => 'test' , 'help' => ['help'] ])->render();
    $this->assertContains('formule-fields-help' , $help);
  }
}
