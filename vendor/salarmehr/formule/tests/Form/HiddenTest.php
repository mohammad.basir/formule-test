<?php
/**
 * Created by PhpStorm.
 * User: Mehdi
 * Date: 2/26/2018
 * Time: 1:12 PM
 */

namespace Formule\Test\Form;

use Formule\Test\TestCase;
class HiddenTest extends TestCase
{

  public function testRender()
  {
    require_once "__DIR__../../sandbox.php";
    $text = field(['type' => 'hidden'])->render();
    $this->assertContains('type', $text);
    $this->assertContains('name', $text);
    $this->assertContains('value', $text);
  }

  public function testGetHiddenTag()
  {
    $name = 'token';
    $value = '123456';
    $expected = "<input  type='hidden' name='{$name}' value='{$value}'>";
    $actual = field(['type' => 'hidden'])->getHiddenTag($name,$value);
    $this->assertEquals($expected , $actual);
  }
}
