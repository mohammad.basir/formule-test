<?php
/**
 * Created by PhpStorm.
 * User: Mehdi
 * Date: 2/26/2018
 * Time: 2:52 PM
 */

namespace Formule\Test\Form;

use Formule\Test\TestCase;
class SelectTest extends TestCase
{
  protected function setUp()
  {
    require_once "__DIR__../../sandbox.php";
  }

  public function testRenderFilter()
  {
    $select = field(['type' => 'select' , 'id' => 'test'])->renderFilter();
    $this->assertContains("multiple", $select);
  }

  public function testRender()
  {
    $select = field(['type' => 'select'])->render();
    $this->assertContains("</select>", $select);
  }

  public function testRenderCell()
  {

  }

  public function testRenderValue()
  {

  }
}
