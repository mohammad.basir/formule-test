<?php
/**
 * Created by PhpStorm.
 * User: Mehdi
 * Date: 2/26/2018
 * Time: 1:06 PM
 */

namespace Formule\Test\Form;

use Formule\Test\TestCase;
class TextTest extends TestCase
{

  public function testRender()
  {
    require_once "__DIR__../../sandbox.php";
    $text = field(['type' => 'text'])->render();
    $this->assertContains('form-control', $text);
  }
}
