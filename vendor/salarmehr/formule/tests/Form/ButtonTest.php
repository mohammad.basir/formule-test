<?php

namespace Formule\Test\Form;

use Formule\Test\TestCase;

class ButtonTest extends TestCase
{

  public function testRender()
  {
    $button = field(['type' => 'button' , 'id' => 'test'])->render();
    $this->assertContains("</button>", $button);

    $button = field(['type' => 'button' , 'id' => 'test' , 'href' =>'href'])->render();
    $this->assertContains("</a>", $button);
  }
}
