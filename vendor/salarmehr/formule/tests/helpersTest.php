<?php

namespace Formule;

use Formule\Test\TestCase;

class HelpersTest extends TestCase
{

  /**
   * @dataProvider toSolarProvider
   * @param $gregorian
   * @param $solar
   */
  public function testToSolar($gregorian, $solar)
  {
    $expected = toSolar($gregorian);
    $this->assertEquals($expected, $solar);
  }

  public function toSolarProvider()
  {
    return [
      ['2019-01-06 11:28:53', '1397/10/16 11:28:53'],
      [null, null],
    ];
  }

  // ساعت رو درست تبدیل نمیکنه
  public function testToIsoFormatting()
  {
    $expected = '2000-01-02 02:03:00';
    $actual = toIso('2000/01/02 02:03', true);
    $this->assertEquals($expected, $actual);
  }

  public function testToIsoConversion()
  {
    $input = '2000/1/2 2:3';
    $actual = toIso($input, true);
    $expected = '2000-01-02 02:03:00';
    $this->assertEquals($expected, $actual);
  }

  public function testSolarTag()
  {
    $time = time();
    $container = solarTag($time);
    $this->assertContains('</time>', $container);
    $this->assertContains("datetime='{$time}'", $container);
    $this->assertContains("title='{$time}'", $container);
    $this->assertContains(toSolar($time), $container);
  }


  /**
   * @dataProvider isoDateProvider
   */
  public function testIsoDate($expected, $input, $dashed)
  {
    $actual = isoDate($input, $dashed);
    $this->assertEquals($expected, $actual);
  }

  public function isoDateProvider()
  {
    return [
      ['2019-02-03 10:38:15', strtotime('2019-02-03 10:38:15'), false],
      ['2019-02-03-10-38-15', strtotime('2019-02-03 10:38:15'), true],
    ];
  }


  public function testTableToTree()
  {

  }

  public function testBower()
  {
    $this->assertEquals('http://localhost/dist/example', bower('example'));
  }

  // فکر کنم provider نیاز داره
  //  public function testCnst(){
  //
  //  }

  /**
   * @dataProvider pluralProvider
   */
  public function testPlural($value, $expected)
  {
    $actual = plural($value);
    $this->assertEquals($expected, $actual);
  }

  public function pluralProvider()
  {
    return [
      ['گل', 'گل‌ها'],
      ['بچه ایران', 'بچه‌های ایران'],
    ];
  }

  // پارامتری که ارسال میشود هر رشته ای می تواند باشد
  public function testMn()
  {
    $this->assertEquals('100 ریال', mn(100));
  }

  //  public function testGet_gravatar(){
  //
  //  }

  //  public function testCuser(){
  //
  //  }

  //  public function testAuthorize(){
  //
  //  }

  //  public function testHdate(){
  //
  //  }


  public function testPlaceInArray()
  {
    $string = 'string';
    $actual = placeInArray($string);
    $this->assertInternalType('array', $actual);
  }

  //  public function testTidy(){
  //
  //  }

  //  public function testField(){
  //
  //  }

  //  public function testIsCalled(){
  //
  //  }

  //  public function testIsLocal(){
  //
  //  }


  //  public function testUtf16(){
  //
  //  }

  //  public function testAddTimestamps(){
  //
  //  }


  //  public function testNgInArray(){
  //
  //  }

  //  public function testCopyInfoColumn(){
  //
  //  }

  //  public function testPath(){
  //
  //  }

  //  public function testVal(){
  //
  //  }

  /**
   * @dataProvider dotToArrayProvider
   */
  public function testDotToArray($value, $expected)
  {
    $actual = dotToArray($value);
    $this->assertEquals($expected, $actual);
  }

  public function dotToArrayProvider()
  {
    return [
      ['one', 'one'],
      ['one.', 'one.'],
      ['one.two', 'one[two]'],
      ['one.two.three', 'one[two][three]'],
    ];
  }

  /**
   * @dataProvider inProvider
   */
  public function testIn($value, $trueStack, $falseStack)
  {
    $true = ['Peter', 'Joe', 'Glenn', 'Cleveland'];
    $this->assertTrue(in($value, $trueStack));
    $this->assertFalse(in($value, $falseStack));
  }

  public function inProvider()
  {
    return [
      ['Peter', 'Peter', 'Joe', 'Glenn', 'Cleveland', 'Alex', ['Peter', 'Joe', 'Glenn', 'Cleveland']],
      ['Peter', 'Peter', 'Joe', 'Glenn', ['Cleveland', ['Alex']], ['Peter', 'Joe', 'Glenn', 'Cleveland']],
    ];
  }

  public function testException()
  {

  }

  public function testHasOverlap()
  {

  }

  public function testHeightAndWidth()
  {
    $path = realpath(__DIR__ . '/..') . '\1.temp.jpg';
    if (is_file($path)) {
      $this->assertGreaterThan(0, height($path));
      $this->assertGreaterThan(0, width($path));
    }
  }

  /**
   * @dataProvider meceProvider
   */
  public function testMece($needle, $trueTruthies, $trueFalsies, $falseTruthies, $falseFalsies)
  {
    $this->assertTrue(mece($needle, $trueTruthies, $trueFalsies));
    $this->assertFalse(mece($needle, $falseTruthies, $falseFalsies));
  }

  public function meceProvider()
  {
    return [
      ['Peter', 'Alex', 'Joe', ['Alex'], 'Peter'],
      ['Peter', 'Peter', 'Peter', 'Alex', ['Peter']],
      ['Peter', ['Peter'], 'Joe', 'Alex', ['Peter', 'Joe']],
      ['Peter', ['Peter', 'Alex'], 'Alex', '', ['Peter']],
      ['Peter', 'Alex', ['Joe'], ['Alex', 'Joe'], 'Peter'],
    ];
  }

  public function testLang()
  {
    $lang = \Locale::getPrimaryLanguage(\Locale::getDefault());
    $this->assertTrue(lang($lang));
  }

  public function testDirection()
  {
    if (lang('fa')) {
      $this->assertTrue(direction('rtl'));
    }
    else {
      $this->assertTrue(direction('ltr'));
    }
  }

  public function testRegion()
  {
    $region = \Locale::getRegion(\Locale::getDefault());
    $this->assertTrue(region($region));
  }


}