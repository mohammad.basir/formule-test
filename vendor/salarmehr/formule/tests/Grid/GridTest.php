<?php

namespace Formule;

use Formule\Test\TestCase;
use Illuminate\View\View;
use Formule\Grid\Grid;

class GridTest extends TestCase
{

  public function testShow()
  {
    $grid = new Grid([],['type' => 'number'],[]);
    $this->assertInstanceOf(View::class , $grid->show());
  }

  public function testRender()
  {
    $grid = new Grid([],['type' => 'number'],[]);
    $this->assertInstanceOf(View::class , $grid->render());
  }

  public function testFind()
  {

  }

  public function testRemoveActions()
  {

  }

  public function testSetAllUserItems()
  {

  }
}
