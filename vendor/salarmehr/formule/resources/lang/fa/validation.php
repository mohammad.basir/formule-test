  <?php

return [

  /*
  |--------------------------------------------------------------------------
  | Validation Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines contain the default error messages used by
  | the validator class. Some of these rules have multiple versions such
  | as the size rules. Feel free to tweak each of these messages here.
  |
  */

  "accepted"             => "«:attribute» باید پذیرفته شده باشد.",
  "active_url"           => "آدرس «:attribute» معتبر نیست",
  "after"                => "«:attribute» باید زمانی بعد از :date باشد.",
  "alpha"                => "«:attribute» باید شامل حروف الفبا باشد.",
  "alpha_dash"           => "«:attribute» باید شامل حروف الفبا و عدد و خظ تیره(-) باشد.",
  "alpha_num"            => "«:attribute» باید شامل حروف الفبا و عدد باشد.",
  "array"                => "«:attribute» باید شامل آرایه باشد.",
  "before"               => "«:attribute» باید زمانی قبل از :date باشد.",
  "between"              => [
    "numeric" => "«:attribute» باید بین :min و :max باشد.",
    "file"    => "«:attribute» باید بین :min و :max کیلوبایت باشد.",
    "string"  => "«:attribute» باید بین :min و :max کاراکتر باشد.",
    "array"   => "«:attribute» باید بین :min و :max آیتم باشد.",
  ],
  "boolean"              => "فیلد «:attribute» فقط میتواند صحیح و یا غلط باشد",
  "confirmed"            => "«:attribute» با تاییدیه مطابقت ندارد.",
  "date"                 => "«:attribute» یک تاریخ معتبر نیست.",
  "date_format"          => "«:attribute» با الگوی :format مطاقبت ندارد.",
  "different"            => "«:attribute» و :other باید متفاوت باشند.",
  "digits"               => "«:attribute» باید :digits رقم باشد.",
  "digits_between"       => "«:attribute» باید بین :min و :max رقم باشد.",
  "email"                => "فرمت «:attribute» معتبر نیست.",
  "exists"               => "«:attribute» انتخاب شده، معتبر نیست.",
  "filled"               => "فیلد «:attribute» الزامی است",
  "image"                => "«:attribute» باید تصویر باشد.",
  "in"                   => "«:attribute» انتخاب شده، معتبر نیست.",
  "integer"              => "«:attribute» باید نوع داده ای عددی (integer) باشد.",
  "ip"                   => "«:attribute» باید IP آدرس معتبر باشد.",
  'json'                 => 'The «:attribute» must be a valid JSON string.',
  "max"                  => [
    "numeric" => "«:attribute» نباید بزرگتر از :max باشد.",
    "file"    => "«:attribute» نباید بزرگتر از :max کیلوبایت باشد.",
    "string"  => "«:attribute» نباید بیشتر از :max کاراکتر باشد.",
    "array"   => "«:attribute» نباید بیشتر از :max آیتم باشد.",
  ],
  "mimes"                => "«:attribute» باید یکی از فرمت های :values باشد.",
  "min"                  => [
    "numeric" => "«:attribute» نباید کوچکتر از :min باشد.",
    "file"    => "«:attribute» نباید کوچکتر از :min کیلوبایت باشد.",
    "string"  => "«:attribute» نباید کمتر از :min کاراکتر باشد.",
    "array"   => "«:attribute» نباید کمتر از :min آیتم باشد.",
  ],
  "not_in"               => "«:attribute» انتخاب شده، معتبر نیست.",
  "numeric"              => "«:attribute» باید شامل عدد باشد.",
  "regex"                => "«:attribute» یک فرمت معتبر نیست",
  "required"             => "«:attribute» وارد نشده است.",
  //  "required_if"          => "فیلد «:attribute» هنگامی که :other برابر با :value است، الزامیست.",
  "required_if"          => "«:attribute» با توجه به مقادیر واردشده الزامی است.",
  "required_if_not_in"   => "«:attribute» با توجه به مقادیر واردشده الزامی است.",
  "required_if_in"       => "«:attribute» با توجه به مقادیر واردشده الزامی است.",
  "required_unless"      => "«:attribute» با توجه به مقادیر واردشده الزامی است.",
  'required_with'        => '«:attribute» با توجه به مقادیر واردشده الزامی است.',
  "required_with_all"    => "«:attribute» الزامی است زمانی که :values موجود است.",
  "required_without"     => "«:attribute» الزامی است زمانی که :values موجود نیست.",
  "required_without_all" => "«:attribute» الزامی است زمانی که :values موجود نیست.",
  "same"                 => "«:attribute» و :other باید مانند هم باشند.",
  "not_same"             => "«:attribute» و :other نباید مانند هم باشند.",
  "size"                 => [
    "numeric" => "«:attribute» باید برابر با :size باشد.",
    "file"    => "«:attribute» باید برابر با :size کیلوبایت باشد.",
    "string"  => "«:attribute» باید برابر با :size کاراکتر باشد.",
    "array"   => "«:attribute» باسد شامل :size آیتم باشد.",
  ],
  "string"               => "The «:attribute» must be a string.",
  "timezone"             => "فیلد «:attribute» باید یک منطقه صحیح باشد.",
  "unique"               => "«:attribute» قبلا انتخاب شده است.",
  "url"                  => "فرمت آدرس «:attribute» اشتباه است.",
  "lifetime"             => "فیلد «:attribute» در بازه زمانی صحیحی قرار ندارد.",
  "captcha"              => "تصویر امنیتی صحیح نیست.",


  /*
   * Mehr custom attributes
   */
  'zip'                  => "«:attribute» معتبر نیست. شماره پستی تنها از ده رقم مجاز تشکیل می‌شود.",
  'nid'                  => "«:attribute» معتبر نیست.",
  'phone'                => "فرمت «:attribute» معتبر نیست. شماره تلفن یازده رقمی است. مانند: 02133433696",
  'mobile'               => "فرمت «:attribute» معتبر نیست. شماره موبایل دقیقا یازده رقم باشد. مانند: 09121312212",
  "after_equal"          => "«:attribute» نباید پیش از :date باشد.",
  "before_equal"         => "«:attribute» نباید پس از :date باشد.",
  "mandatory"            => "«:attribute» وارد نشده است.",
  "mandatory_if"         => "«:attribute» با توجه به مقادیر واردشده الزامی است.",
  "photo"                => " پرونده‌ی انتخاب شده برای «:attribute». وجود ندارد یا تصویر نیست.",

  /*
  |--------------------------------------------------------------------------
  | Custom Validation Language Lines
  |--------------------------------------------------------------------------
  |
  | Here you may specify custom validation messages for attributes using the
  | convention "attribute.rule" to name the lines. This makes it quick to
  | specify a specific custom language line for a given attribute rule.
  |
  */

  'custom'     => [
  ],

  /*
  |--------------------------------------------------------------------------
  | Custom Validation Attributes
  |--------------------------------------------------------------------------
  |
  | The following language lines are used to swap attribute place-holders
  | with something more reader friendly such as E-Mail Address instead
  | of "email". This simply helps us make messages a little cleaner.
  |
  */
  'attributes' => [
    "address"                    => "نشانی",
    "age"                        => "سن",
    "available"                  => "موجود",
    "birthplace"                 => "محل تولد",
    "city"                       => "شهر",
    "content"                    => "محتوا",
    "country"                    => "کشور",
    "date"                       => "تاریخ",
    "day"                        => "روز",
    "department"                 => "گروه آموزشی",
    "description"                => "توضیحات",
    "email"                      => "پست الکترونیکی",
    "email2"                     => "پست الکترونیکی دوم",
    "excerpt"                    => "گلچین کردن",
    "firstName"                  => "نام",
    "gender"                     => "جنسیت",
    "gpa"                        => 'معدل',
    "hour"                       => "ساعت",
    "id"                         => "شناسه سیستمی",
    "lastLoginDate"              => "تاریخ آخرین ورود",
    "lastName"                   => "نام خانوادگی",
    "minute"                     => "دقیقه",
    "mobile"                     => "تلفن همراه",
    "month"                      => "ماه",
    "name"                       => "نام",
    "password"                   => "رمز عبور",
    "passwordConfirmation"       => "تاییدیه ی رمز عبور",
    "phone"                      => "تلفن",
    "second"                     => "ثانیه",
    "sex"                        => "جنسیت",
    "size"                       => "اندازه",
    "text"                       => "متن",
    "time"                       => "زمان",
    "title"                      => "عنوان",
    "type"                       => "نوع",
    "username"                   => "شناسه کاربری",
    "year"                       => "سال",
    "zip"                        => "شماره پستی",
    'active'                     => 'فعال',
    'activeStudent'              => 'وضعیت دانشجو',
    'birthdayDate'               => 'تاریخ تولد',
    'captcha'                    => 'تصویر امنیتی',
    'cDate'                      => 'تاریخ ایجاد',
    'college'                    => 'دانشکده',
    'confirmPassword'            => 'تکرار گذرواژه',
    'conditionalTerms'           => 'ترم‌های مشروطی',
    'successiveConditionalTerms' => 'ترم‌های مشروطی متوالی',
    'course'                     => 'دوره',
    'degree'                     => 'مقطع',
    'dormitory'                  => 'وضعیت خوابگاه',
    'electionEndDate'            => 'تاریخ پایان رأی گیری',
    'electionStartDate'          => 'تاریخ شروع رأی گیری',
    'endDate'                    => 'تاریخ پایان',
    'endTerm'                    => 'ترم پایان',
    'enrollmentEndDate'          => 'تاریخ پایان نام‌نویسی',
    'enrollmentStartDate'        => 'تاریخ شروع نام‌نویسی',
    'entity'                     => 'نهاد',
    'executionEndDate'           => 'زمان پایان برنامه',
    'executionStartDate'         => 'زمان آغاز برنامه',
    'fatherName'                 => 'نام پدر',
    'graduationDate'             => 'تاریخ پایان تحصیل',
    'lastDegree'                 => 'اخرین مقطع',
    'lastMajor'                  => 'آخرین رشته تحصیلی',
    'lastUniversity'             => 'آخرین دانشگاه',
    'major'                      => 'رشته',
    'maritalStatus'              => 'وضعیت تاهل',
    'method'                     => 'شیوه بازنشانی',
    'militaryService'            => 'وضعیت خدمت سربازی',
    'minor'                      => 'گرایش',
    'nationality'                => 'ملیت',
    'native'                     => 'بومی',
    'newPassword'                => 'گذرواژه جدید',
    'nid'                        => 'کد ملی',
    'number'                     => 'شماره',
    'passedUnits'                => 'واحد‌های گذرانده شده',
    'physicalStatus'             => 'وضعیت جسمانی',
    'province'                   => 'استان',
    'publishDate'                => 'زمان انتشار',
    'quota'                      => 'سهمیه',
    'religion'                   => 'دین',
    'sid'                        => 'شماره دانشجویی',
    'source'                     => 'شیوه تعریف',
    'startDate'                  => 'تاریخ شروع',
    'startTerm'                  => 'ترم شروع',
    'status'                     => 'وضعیت',
    'studentStatus'              => 'وضعیت دانشجویی',
    'system.email'               => 'ایمیل سیستم',
    'takenUnits'                 => 'واحد‌های اخذ شده',
    'term'                       => 'دوره',
    'uDate'                      => 'تاریخ آخرین ویرایش',
    'user'                       => 'کاربر',
    'volume'                     => 'دوره',
    'url'                        => ' پیوند',
    'link'                       => 'پیوند',
    'latinFirstName'             => 'نام(انگلیسی)',
    'latinLastName'              => 'نام‌خانوادگی(انگلیسی)',
    'receivers'                  => 'گیرندگان',
    'subject'                    => 'موضوع',
    'amount'                     => 'میزان افزایش',
    'endTime'                    => 'زمان پایان',
    'startTime'                  => 'زمان شروع',
    'loginAttempts'              => 'تلاش ناموفق برای ورود',
    'responsibility'             => 'مسئولیت',
    'workAddress'                => 'نشانی محل کار',
  ],
];