<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FormuleUserLoginTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {

    if (Schema::hasTable('login')) {
      echo "Notice: \nThe table file already exists.\n";
      return;
    }

    Schema::create('login', function (Blueprint $table) {
      $table->integer('id', true);
      $table->integer('userId', false);
      $table->timestamp('cDate')->default(DB::raw('CURRENT_TIMESTAMP'));

      $table->char('ip', 15);
      $table->string('string')->nullable();
      $table->char('os')->nullable();
      $table->char('agent')->nullable();
      $table->char('version',10)->nullable();
      $table->char('device')->nullable();
      $table->char('method',20)->nullable();

      $table->foreign('userId')->references('id')->on('user');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('file');
  }
}
