<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FormuleCreateFileTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {

    if (Schema::hasTable('file')) {
      echo "Notice: \nThe table file already exists.\n";
      return;
    }

    Schema::create('file', function (Blueprint $table) {
      $table->integer('id', true);
      $table->integer('fileableId')->nullable();
      $table->char('fileableType', 50)->nullable();
      $table->string('field')->nullable();

      $table->integer('userId');
      $table->foreign('userId', 'FK_file_user')->references('id')->on('user');

      $table->char('hash', 32)->unique();
      addTimestamps($table, false);
      $table->timestamp('expirationDate')->nullable();
      $table->string('name');
      $table->string('type');
      $table->string('location');
      $table->integer('size');
      $table->char('access', 20)->nullable();
      $table->string('details')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('file');
  }
}
