var tabIds = [];
var tabLinks = [];
var currentTab = 0;
var submit;
var tabs;
function renderSteps() {
    var count = tabs[0].children.length;
    tabIds = [];
    tabLinks = [];
    $('#stepsHtml').html("");
    var i = 0;
    $.each(tabs[0].children, function (key, value) {
        var li = $(value);
        var id;
        if (!li.hasClass('ng-hide')) {
            tabIds.push(id = $(value).attr('data-tab'));
            tabLinks.push((new stepLink(i, value.innerText).btn()));
            ++i;
        }
        if (!--count) {
            renderedHtml = tabLinks.join(" » ");
            $('#stepsHtml').html(renderedHtml);
        }
    });
    if (currentTab == 0) {
        $('#tabLink-' + currentTab).addClass('selected');
    }
}

$(document).ready(function () {
    tabs = $('[role="tablist"]');
    submit = $('[type="submit"]');
    submit.hide();
    stepLink = function (id, text) {
        this.id = id;
        this.text = text;
        this.btn = function () {
            return "<a href='#' id='tabLink-" + id + "' onclick='showTab(" + id + ")'>" + text + "</a>";
        }
    };
    tabs.after("<div id='stepsHtml' class='steps'></div>");
    currentTab = 0;
    setTimeout(function () {
        renderSteps();
    }, 500);
    showTab(currentTab, true);
    tabs.hide();
});

function showTab(id, firstTime) {
    if (id == (tabIds.length - 1)) {
        submit.show();
    }
    currentTab = id;
    if (firstTime == false) {
        renderSteps();
    }
    $.each(tabIds, function (key, value) {
        if (value.id != id) {
            $('#tab-' + tabIds[key]).hide();
            $('#tabLink-' + tabIds[key]).removeClass('selected');

        }
    });
    $('#tab-' + tabIds[id]).show();
    $('#tabLink-' + id).addClass('selected');
}

function nextPage() {
    if (currentTab < tabIds.length - 1) {
        showTab(++currentTab, false)
    }
}

function previousPage() {
    if (currentTab > 0) {
        showTab(--currentTab, false)
    }
}
