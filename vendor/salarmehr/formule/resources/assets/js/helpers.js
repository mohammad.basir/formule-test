/**
 * similar to php isset function by you have to send variable name as string.
 * @param variable {String}
 * @returns {boolean}
 */
function isset(variable) {
    return typeof this[variable] !== typeof undefined;
}


/**
 * Checks if a variable (usually a variable that store a field value) has real value.
 * @param val
 * @returns {boolean}
 */
function exists(val) {
    if (!Array.isArray(val)) {
        val = [val];
    }
    val = val.filter(function (v) {
        return v !== null && String(v).trim().length;
    });
    return Boolean(val.length);
}

function debounce(fn, delay) {
    var timer = null;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };
}

function throttle(fn, threshold, scope) {
    threshold || (threshold = 250);
    var last,
        deferTimer;
    return function () {
        var context = scope || this;

        var now = +new Date,
            args = arguments;
        if (last && now < last + threshold) {
            // hold on to it
            clearTimeout(deferTimer);
            deferTimer = setTimeout(function () {
                last = now;
                fn.apply(context, args);
            }, threshold);
        } else {
            last = now;
            fn.apply(context, args);
        }
    };
}

function msg(html, type, title) {
    type = type || 'error';
    // $('body').scrollTop(0);
    // $('.pageErrorBox').slideUp('fast').slideDown().html($html);
    toastr[type](html, title, {
        closeButton: true,
        timeOut: type === 'error' ? 10000 : 5000,
        // extendedTimeOut: 0,
        positionClass: "toast-top-center"
    });
    reposition();
}

// etv jQuery plugins
(function ($) {
    // increase the content or value by num
    $.fn.increment = function (num, max) {
        var val = this.is('input') ? +this.val() : +this.html();
        var newVal = (val >= max) ? max : val + num;
        return this.is('input') ? this.val(newVal) : this.html(newVal);
    };

    $.fn.decrement = function (num, min) {
        num = -num;
        var val = this.is('input') ? +this.val() : +this.html();
        var newVal = (val <= min) ? min : val + num;
        return this.is('input') ? this.val(newVal) : this.html(newVal);
    };

    // similar to .data() method but only return attributes data
    $.fn.info = function () {
        var data = {};
        [].forEach.call(this.get(0).attributes, function (attr) {
            if (/^data-/.test(attr.name)) {
                var camelCaseName = attr.name.substr(5).replace(/-(.)/g, function ($0, $1) {
                    return $1.toUpperCase();
                });
                data[camelCaseName] = attr.value;
            }
        });
        return data;
    };

    $.fn.overflowed = function checkOverflow() {
        var el = this[0];
        console.log(el);

        var curOverflow = el.style.overflow;

        if (!curOverflow || curOverflow === "visible")
            el.style.overflow = "hidden";

        var isOverflowing = el.clientWidth < el.scrollWidth
            || el.clientHeight < el.scrollHeight;

        el.style.overflow = curOverflow;

        return isOverflowing;
    };

    $.fn.funcToggle = function (type, data) {
        var dname = "jqp_eventtoggle_" + type + (new Date()).getTime(),
            funcs = Array.prototype.slice.call(arguments, 2),
            numFuncs = funcs.length,
            empty = function () {
            },
            false_handler = function () {
                return false;
            };

        if (typeof type === "object") {
            for (var key in type) {
                $.fn.funcToggle.apply(this, [key].concat(type[key]));
            }
            return this;
        }
        if ($.isFunction(data) || data === false) {
            funcs = [data].concat(funcs);
            numFuncs += 1;
            data = undefined;
        }

        funcs = $.map(funcs, function (func) {
            if (func === false) {
                return false_handler;
            }
            if (!$.isFunction(func)) {
                return empty;
            }
            return func;
        });

        this.data(dname, 0);
        this.bind(type, data, function (event) {
            var data = $(this).data(),
                index = data[dname];
            funcs[index].call(this, event);
            data[dname] = (index + 1) % numFuncs;
        });
        return this;
    }
}(jQuery));