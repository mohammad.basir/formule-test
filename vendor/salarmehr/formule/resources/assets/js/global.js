"use strict";

$(document).on('click', '[data-confirm]', function (e) {
    var $me = $(this);
    if (confirm($(this).data('confirm'))) {
        $me.trigger('confirm');
    }
    else {
        e.stopImmediatePropagation();
        e.preventDefault();
        $me.trigger('decline');
    }
});

//region utilities
function isset(variable) {
    return typeof variable !== typeof undefined;
}

function debounce(fn, delay) {
    var timer = null;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };
}

function throttle(fn, threshold, scope) {
    threshold || (threshold = 250);
    var last,
        deferTimer;
    return function () {
        var context = scope || this;

        var now = +new Date,
            args = arguments;
        if (last && now < last + threshold) {
            // hold on to it
            clearTimeout(deferTimer);
            deferTimer = setTimeout(function () {
                last = now;
                fn.apply(context, args);
            }, threshold);
        } else {
            last = now;
            fn.apply(context, args);
        }
    };
}

function isTouchDevice() {
    return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
}

/**
 * Checks if a variable (usually a variable that store a field value) has real value.
 * @param val
 * @returns {boolean}
 */
function isSent(val) {
    if (!Array.isArray(val)) {
        val = [val];
    }
    val = val.filter(function (v) {
        return v !== null && String(v).trim().length;
    });
    return Boolean(val.length);
}

function msg(html, type, title) {
    type = type || 'error'
    toastr[type](html, title, {
        closeButton: true,
        timeOut: type === 'error' ? 10000 : 5000,
        // extendedTimeOut: 0,
        positionClass: "toast-top-center",
    });
    reposition();
}

//endregion

//region ajax
$(document).on('submit', '.ajax-modal-form form', function (e) {
    e.preventDefault();
    return ajaxModal(e.target);
});

$(document).on('submit', 'form.ajaxForm', function (e) {
    e.preventDefault();
    return ajaxSubmit(e.target);
});

function ajaxSubmit(form, options) {
    var $form = $(form);
    var data = $form.serializeObject();
    var url = $form.attr('action');
    var method = $form.attr('method') || 'POST';
    data['g-recaptcha-response'] = $("#g-recaptcha-response").val();
    return ajax(method, url, data, $form, options);
}

function ajaxModal(formSelector) {
    var $form = $(formSelector);
    var options = {headers: {modal: true}, fail: false, done: false};
    var $modal = $form.closest('.ajax-modal-form');
    return ajaxSubmit($form, options)
        .done(function (response) {
            $modal.on('hidden.bs.modal', function () {
                if (response.msg) {
                    msg(response.msg, 'success');
                }
                if (response.html) {
                    $('.ajaxCache').html(response.html);
                }
            });
            $modal.modal('hide');
        })
        .fail(function (exception) {
            if (exception.status = 422) {
                $modal.html(exception.responseText);
            }
        });
}

function ajax(method, url, data, overlayElement, options) {
    data = data || {};
    options = options || {};

    var spinner = false;
    var defaults = {
        target: null,
        done: true, // should 'done' callback be called
        fail: true, //should 'faild' callback be called
        always: true, // should 'always' callback be called
        headers: {}
    };

    if (overlayElement === 'spinner') {
        spinner = $('.ajaxSpinner');
        overlayElement = false;
    }
    else if (overlayElement) {
        overlayElement = $(overlayElement);
    }

    options = $.extend(defaults, options);

    // assigning a random hash identifier to the target element so server can address it.
    if (options.target) {
        var hash = Math.random().toString(36).substring(7);
        $(options.target).attr('data-hash', hash);
        data.hash = hash;
    }

    if (overlayElement) {
        overlayElement.LoadingOverlay("show");
    }
    else if (spinner) {
        spinner.show();
    }

    return $.ajax(url, {
        data: data,
        method: method,
        headers: options.headers,
    }).done(function (response) {
        if (options.done) {
            $('.ajaxCache').html(response);
        }
    }).fail(function (exception) {
        if (options.fail) {
            if (exception.status == 422) {
                $('.ajaxCache').html(exception.responseText);
            }
            else if (exception.status == 401) {
                msg(exception.responseText, 'error');
            }
            else if (isLocal()) {
                $('<div>').html(exception.responseText).dialog();
            } else {
                msg('Sorry, your action can not be done. Please try again later or contact '.supportEmail(), 'error');
            }
        }


    }).always(function () {
        if (options.always) {
            if (overlayElement) {
                overlayElement.LoadingOverlay("hide");
            }
            else if (spinner) {
                spinner.hide();
            }
        }
    });
}

// endregion

// any positioning that is not possible via CSS need to be done here.
// this is called on page load and window resizing.
function reposition() {
    console.error('Please define reposition function.');
}

// etv jQuery plugins
(function ($) {
    // increase the content or value by num
    $.fn.increment = function (num, max) {
        var val = this.is('input') ? +this.val() : +this.html();
        var newVal = (val >= max) ? max : val + num;
        return this.is('input') ? this.val(newVal) : this.html(newVal);
    };

    $.fn.decrement = function (num, min) {
        num = -num;
        var val = this.is('input') ? +this.val() : +this.html();
        var newVal = (val <= min) ? min : val + num;
        return this.is('input') ? this.val(newVal) : this.html(newVal);
    };

    // similar to .data() method but only return attributes data
    $.fn.info = function () {
        var data = {};
        [].forEach.call(this.get(0).attributes, function (attr) {
            if (/^data-/.test(attr.name)) {
                var camelCaseName = attr.name.substr(5).replace(/-(.)/g, function ($0, $1) {
                    return $1.toUpperCase();
                });
                data[camelCaseName] = attr.value;
            }
        });
        return data;
    };

    $.fn.overflowed = function checkOverflow() {
        var el = this[0];
        console.log(el);

        var curOverflow = el.style.overflow;

        if (!curOverflow || curOverflow === "visible")
            el.style.overflow = "hidden";

        var isOverflowing = el.clientWidth < el.scrollWidth
            || el.clientHeight < el.scrollHeight;

        el.style.overflow = curOverflow;

        return isOverflowing;
    }
}(jQuery));

/*
 * jQuery Function Toggle Pluing
 * Copyright 2011, Felix Kling
 * Dual licensed under the MIT or GPL Version 2 licenses.
 */

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    }
    else {
        // Browser globals
        factory(jQuery);
    }
})(function ($) {
    $.fn.funcToggle = function (type, data) {
        var dname = "jqp_eventtoggle_" + type + (new Date()).getTime(),
            funcs = Array.prototype.slice.call(arguments, 2),
            numFuncs = funcs.length,
            empty = function () {
            },
            false_handler = function () {
                return false;
            };

        if (typeof type === "object") {
            for (var key in type) {
                $.fn.funcToggle.apply(this, [key].concat(type[key]));
            }
            return this;
        }
        if ($.isFunction(data) || data === false) {
            funcs = [data].concat(funcs);
            numFuncs += 1;
            data = undefined;
        }

        funcs = $.map(funcs, function (func) {
            if (func === false) {
                return false_handler;
            }
            if (!$.isFunction(func)) {
                return empty;
            }
            return func;
        });

        this.data(dname, 0);
        this.bind(type, data, function (event) {
            var data = $(this).data(),
                index = data[dname];
            funcs[index].call(this, event);
            data[dname] = (index + 1) % numFuncs;
        });
        return this;
    };
});

function init() {
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var $toolTippedElements = $('[title]');
        $toolTippedElements.tooltip();
        $toolTippedElements.click(function () {
            $('[title]').tooltip('close');
        });
        $(window).on('beforeunload', function () {
            $('[title]').tooltip('close');
        });

        // dynamic positioning impossible using CSS
        reposition();

        /**
         * Global EVENTS
         */

        $(window)
            .resize(function () {
                reposition();
            });

        if (isTouchDevice() === false) {
            $('[data-toggle="tooltip"]').each(function () {
                $(this).tooltip({
                    container: $('.page-wrapper'),
                    html: true,
                    delay: {"show": 500, "hide": 100},
                    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner text-left"></div></div>',
                    constraints: [
                        {
                            to: 'scrollParent',
                            pin: true
                        }
                    ]
                });
            });
        }

        // this great short snippet disables submit buttons, on form submit
        $('form:not(.ajaxForm)').on('submit', function () {
            var $me = $(this);
            $me.find('[type=submit]').LoadingOverlay("show");
        });

        $('.passwordWidget [name=password]').on('change paste keyup click', function () {

            var value = $(this).val();

            function toggle(regex, cls) {
                if (value.match(regex)) {
                    $('.passwordWidget .criteria ' + cls).addClass('valid');
                }
                else {
                    $('.passwordWidget .criteria ' + cls).removeClass('valid');
                }
            }

            toggle(/\d/, '.number');
            toggle(/[a-zA-Z]/, '.letter');
            toggle(/.{8,}/, '.characters');
        })
            .on('blur', function () {
                $('.passwordWidget .criteria div:not(.valid)').addClass('invalid');
            });

        $('.backToTopBtn').click(function () {
            $('body').scrollTop(0);
        });
    });

}

init();