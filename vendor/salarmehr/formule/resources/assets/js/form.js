/*
 bindWithDelay jQuery plugin
 Author: Brian Grinstead
 MIT license: http://www.opensource.org/licenses/mit-license.php
 http://github.com/bgrins/bindWithDelay
 http://briangrinstead.com/files/bindWithDelay
 Usage:
 See http://api.jquery.com/bind/
 .bindWithDelay( eventType, [ eventData ], handler(eventObject), timeout, throttle )
 Examples:
 $("#foo").bindWithDelay("click", function(e) { }, 100);
 $(window).bindWithDelay("resize", { optional: "eventData" }, callback, 1000);
 $(window).bindWithDelay("resize", callback, 1000, true);
 */

(function ($) {

    $.fn.bindWithDelay = function (type, data, fn, timeout, throttle) {

        if ($.isFunction(data)) {
            throttle = timeout;
            timeout = fn;
            fn = data;
            data = undefined;
        }

        // Allow delayed function to be removed with fn in unbind function
        fn.guid = fn.guid || ($.guid && $.guid++);

        // Bind each separately so that each element has its own delay
        return this.each(function () {

            var wait = null;

            function cb() {
                var e = $.extend(true, {}, arguments[0]);
                var ctx = this;
                var throttler = function () {
                    wait = null;
                    fn.apply(ctx, [e]);
                };

                if (!throttle) {
                    clearTimeout(wait);
                    wait = null;
                }
                if (!wait) {
                    wait = setTimeout(throttler, timeout);
                }
            }

            cb.guid = fn.guid;

            $(this).on(type, data, cb);
        });
    };

})(jQuery);


var config = {
    //customConfig: ''
    language: 'fa',
//                extraPlugins: 'image,uploadimage',
    extraPlugins: 'image',
//            removePlugins: 'image,forms',
    filebrowserImageBrowseLinkUrl: "upload/upload.php",
    toolbarGroups: [
        {
            name: 'clipboard',
            groups: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']
        },
        {name: 'editing', groups: ['Scayt']},
        {name: 'links', groups: ['Link', 'Unlink', 'Anchor']},
        {name: 'insert', groups: ['Image', 'Table', 'HorizontalRule']},
        {name: 'tools', groups: ['Maximize']},
        {name: 'document', groups: ['Source']},
        {name: 'basicstyles', groups: ['Bold', 'Strike', '-', 'RemoveFormat']},
        {
            name: 'paragraph',
            groups: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
        },
        {name: 'styles', groups: ['Styles', 'Format']}
    ]

};

'use strict';

$(function () {
    $('textarea.rich').ckeditor(config);
    // $('.fittext').fitText();
    //var config =
    //{
    //    toolbar: [['Styles', 'Bold', 'Italic', 'Underline', 'SpellChecker', 'Scayt', '-', 'NumberedList', 'BulletedList'],
    //        ['Link', 'Unlink'], ['Undo', 'Redo', '-', 'SelectAll']]
    //
    //};


    //var types = {
    //    image: /(\.|\/)(gif|jpe?g|png)$/i,
    //    pdf: /pdf/i,
    //    tsv: /tsv|txt/i
    //};

    //$.find('.file.rich').fileupload({
    //        url: scope.url || 'file',
    //        acceptFileTypes: accept,
    //        progress: function (e, data) {
    //            var progress = parseInt(data.loaded / data.total * 100, 10);
    //            if (progress == 100) {
    //                element.find('.meter').fadeOut();
    //            } else {
    //                element.find('.meter').css('display', 'inline-block');
    //            }
    //            element.find('.meter > span').css(
    //                'width',
    //                progress + '%'
    //            );
    //        }
    //
    //        ,
    //        done: function (e, data) {
    //            scope.field = data.result;
    //            scope.$apply();
    //        }
    //    }
    //)


//     $('[title]').tooltip({show: {effect: 'slideIn'}});
    $(document).ajaxStart(function () {
        $('.busy-box').fadeIn();
    });
    $(document).ajaxStop(function () {
        $('.busy-box').fadeOut();
    });
//     $(document).ajaxError(function (e, xhr) {
//         if (typeof xhr.responseJSON == 'object' && xhr.status == 422) {
//         }
//         else if (typeof xhr.responseJSON == 'string' && xhr.status == 401) {
//
//             Window.location.href = (xhr.responseJSON);
//         }
//         else if (typeof xhr.responseJSON == 'object' && xhr.status == 423) {
//             notif({
//                 position: 'center',
//                 type: 'error',
//                 msg: xhr.responseText || 'خطایی در سرور رخ داده است.'
//             });
//             setTimeout(function () {
//                 location.reload()
//             }, 5000);
//
//         } else {
//             //notif({
//             //    position: 'center',
//             //    type: 'error',
//             //    msg: xhr.responseText || 'خطایی در سرور رخ داده است.'
//             //});
//         }
//
//     });
// });
//
//
// window.Mehr = window.Mehr || [];
// Mehr.tpl = {};
//
// Mehr.cnst = function (value, category) {
//     if (!value) {
//         return 'نامشخص';
//     }
//     if (!cnst[category]) {
//         console.info("String Field not found: " + category);
//         return 'نامشخص';
//     }
//     var r = cnst[category][value];
//     if (r) {
//         return r;
//     }
//     else {
//         console.info("Constant not found: " + category + ' ' + value);
//         return r || 'نامشخص';
//     }
// };
//
// // todo: does not return gender
// Mehr.tpl.userFirstName = function (row) {
//     var cls = {
//         'm': 'male',
//         'f': 'female',
//         'u': 'unknown'
//     };
//     if (row.gender) {
//         row.gender = row.gender[cls[row.gender]];
//     }
//     var userTpl = _.template('<a class="<%=gender%>" href="/user/<%=id%>"><%=fullName%></a>');
//     return userTpl(row);
// };
// Mehr.tpl.timeStatus = function (row, field) {
//     var cls = {
//         'c': 'current',
//         'p': 'past',
//         'f': 'future'
//     };
//     if (row[field]) {
//         row.cls = cls[row[field]];
//     } else {
//         row.cls = '';
//     }
//     var tpl = _.template('<span class="<%=cls%>">' + Mehr.cnst(row[field], 'timeStage') + '</span>');
//     return tpl(row);
// };
// Mehr.tpl.user = _.template('<a class="<%=gender%>" title="نمایش کاربر" href="/user/<%=id%>"><%=name%></a>');
// Mehr.tpl.userName = _.template('<a class="<%=gender%>" title="صفحه کاربر", href="/user/<%=id%>"><%=name%></a>');
// Mehr.tpl.program = _.template('<a title="صفحه برنامه" href="/program/<%=id%>"><%=name%></a>');
// Mehr.tpl.entity = _.template('<a title="صفحه برنامه"href="/entity/<%=id%>"><%=name%></a>');
// Mehr.tpl.council = _.template('<a href="/entity/<%=entity%>/council/<%=id%>"><%=term%></a>');
// Mehr.tpl.journal = _.template('<a href="/journal/<%=id%>"><%=name%></a>');
// Mehr.tpl.session = _.template('<a href="/session/<%=id%>"><%=name%></a>');
// Mehr.tpl.editBtn = function (url) {
//     return "<a title='ویرایش' class='btn' href='" + url + "'><i class='fa fa-pencil'></i></a>";
// };
// Mehr.tpl.viewBtn = function (url, icon) {
//     var icon = icon || 'fa-eye';
//     return "<a title='نمایش' class='btn' href='" + url + "'><i class='fa " + icon + "'></i></a>";
// };
// Mehr.tpl.downloadBtn = function (title, url) {
//     return "<a title='" + title + "' class='btn' href='/fs/" + url + "'><i class='fa fa-download'></i></a>";
// };
// Mehr.tpl.viewColorBtn = function (url, color) {
//     return "<a title='نمایش' class='btn' href='" + url + "'><i class='fa fa-eye " + color + "'></i></a>";
// };
// Mehr.tpl.userBtn = function (url) {
//     return "<a title='نمایش' class='btn' href='" + url + "'><i class='fa fa-users'></i></a>";
// };
//
// Mehr.tpl.boolean = function (value) {
//     if (value)
//         return "<i  title='دارد' class='fa fa-check green'></i>";
//     else
//         return "<i  title='ندارد' class='fa fa-times red'></i>";
// };
//
// Mehr.tpl.roleType = function (value) {
//     if (value == "entity")
//         return 'مخصوص مدیران نهاد';
//     else
//         return " عمومی ";
// };
//
// Mehr.tpl.priority = function (value) {
//     if (value == 3)
//         return "<i  title='مهم' class='fa fa-sort-up red'></i>";
//     else if (value == 4)
//         return "<i  title='بسیار مهم' class='fa fa-sort-up green'></i>";
//     else
//         return "";
// };
//
// Mehr.tpl.date = function (date, format) {
//     if (date == null) {
//         return 'نامشخص ';
//     }
//     if (!format) {
//         format = 'jYYYY/jMM/jDD HH:mm';
//     }
//     date = moment(date);
//     return '<span class=date>' + date.format(format) + '</span>';
});

/*global jQuery */
/*!
 * FitText.js 1.2
 *
 * Copyright 2011, Dave Rupert http://daverupert.com
 * Released under the WTFPL license
 * http://sam.zoy.org/wtfpl/
 *
 * Date: Thu May 05 14:23:00 2011 -0600
 */

(function ($) {

    $.fn.fitText = function (kompressor, options) {

        // Setup options
        var compressor = kompressor || 1,
            settings = $.extend({
                'minFontSize': Number.NEGATIVE_INFINITY,
                'maxFontSize': Number.POSITIVE_INFINITY
            }, options);

        return this.each(function () {

            // Store the object
            var $this = $(this);

            // Resizer() resizes items based on the object width divided by the compressor * 10
            var resizer = function () {
                $this.css('font-size', Math.max(Math.min($this.width() / (compressor * 10), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize)));
            };

            // Call once to set.
            resizer();

            // Call on resize. Opera debounces their resize by default.
            $(window).on('resize.fittext orientationchange.fittext', resizer);

        });

    };

})(jQuery);

// $('form').submit(function (e) {
//     var self = $(this);
//     $.each($(".mehr-jstree"), function () {
//         var name = this.id;
//         $.each($("#" + this.id).jstree("get_checked", true), function () {
//             self.append('<input type="hidden" name="' + name + '[]" value="' + this.data + '"/>');
//         });
//     });
// //                e.preventDefault();
// });