$(function () {
    $('.table-container').doubleScroll();
    $('.toggle-filterBox').click(function () {
        $('.filterBox').toggle('slideIn');
    });

    var old = $('.formule-form').serialize();
    var handler = function (e) {
        var current = $('.formule-form').serialize();
        if (old != current) {
            old = current;
            $.get(location.pathname, current, function (rows) {
                $('.formule-table tbody').html(rows);
            })
        }
    };
    $('.formule-form').bindWithDelay('click keyup', handler, 1000)
});