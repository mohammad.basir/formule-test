<?php
return [
  'layout'    => 'formule::layout.layout',

  // making field labels, column headers and etc. title case, you always use normal case for words e.g. 'first name', 'ID'
  'titleCase' => false,


  'subtypes'  => [
    'backBtn' => [
      'type'  => 'button',
      'icon'  => 'fa fa-back-arrow ui button',
      'label' => __('formule::word.back'),
    ]
    ,
    'integer' => [
      'type'      => 'number',
      'validator' => 'integer|min:0',
      'label'     => __('formule::word.count'),
    ],
    'price'   => [
      'type'      => 'number',
      'validator' => 'integer|min:0',
      'label'     => '(مبلغ)',
      'unit'      => 'ریال',
    ],
    [
      'label'    => __('formule::word.gender'),
      'id'       => 'gender',
      'required' => true,
      'type'     => 'radio',
      'items'    => [
        'm' => __('formule::word.male'),
        'f' => __('formule::word.female'),
      ],
    ]
    // add your custom types here
  ],
];