@extends('formule::field')

@section('input')
    <textarea
            rows='6'
            cols='60%'
            {!! $field->cls() !!}
            {!! $field->attrs() !!}
    >{{$field->value}}</textarea>
@endsection