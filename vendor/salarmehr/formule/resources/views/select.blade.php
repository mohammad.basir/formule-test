@extends('formule::field')
@section('input')
  <select {!! $field->attrs() !!}
          {!! $field->cls() !!}
          {{--          {{$field->url}}--}}
          except='{{$field->except}}'
  >
    @foreach ($field->get('items', []) as $value => $name)
      <option
        value='{{@$value}}'
        @if(in_array($value, (array)$field->value)) selected @if($field['ng-model']) ng-selected="true" @endif @endif
        @if(in_array($value, $field->get('disabledItem', []))) disabled @endif
      >
        {{$name}}
      </option>
    @endforeach
  </select>

  @if($field->addBtn && $field->form->mode!='filter')
    <a href="{{url($field->type, 'create')}}" title="{{__('formule::word.add')}}" class="create" target="_blank">
      <i class="fa fa-plus-circle"></i>
    </a>
  @endif
@endsection