<?php $filtering = $field->filter || isCalled('renderFilter'); ?>

<?php /*formule-field: parents a field its errors and help */ ?>
<div
  class='formule-field {{$field->hasError}} {{$field->name}}'
  @if($field['ng-show']) ng-show="{!!$field['ng-show'] !!}" @endif
  @if($field['ng-hide']) ng-hide="{!!$field['ng-hide'] !!}" @endif
  @if($field['ng-disabled']) ng-hide="{!!$field['ng-disabled'] !!}" @endif
>
  <div class="formule-field-control">
    @if(!in_array($field->type,['hidden','boolean']) && $field->label)
      <label
        class='formule-field-label'
        for='{{$field->name}}'
      >
        {{$field->label}}{{$field->splitter ?? ':'}}
        @if($field->required && $field->mode == 'edit'&&!$field->readonly)
          <span class="formule-field-required" title="پرکردن این میدان الزامی است.">✳</span>
        @endif
      </label>
    @endif
    @yield('input')

    @if($field->unit && !$field->skipUnit)
      <span class="formule-field-unit"> {{$field->unit}}</span>
    @endif
  </div>

  @unless($filtering)
    @if(isset($errors) && $errors->get($field->name))
      <ul class='formule-field-errors alert alert-danger'>
        @foreach($errors->get($field->name) as $error)
          {{--the regex removes در بخش فلان text--}}
          <li>{{preg_replace('# ?\(.*\)#u','',$error,1)}}</li>
        @endforeach
      </ul>
    @endif

    @if($field->help&&!$field->readonly)
      <div class='alert alert-info help-block formule-fields-help' id='{{$field->name}}-help'>
        @if(is_array($field->help) && \f\arr::isAssoc($field->help))
          <dl>
            @foreach($field->help as $title=>$desc)
              <dt>{{$title}}:</dt>
              <dd>{{$desc}}</dd>
            @endforeach
          </dl>
        @elseif(is_array($field->help))
          <ul>
            @foreach($field->help as $desc)
              <li>{{$desc}}</li>
            @endforeach
          </ul>
        @else
          📓 {!!$field->help!!} <i class='fa fa-arrow-up'></i>
        @endif
      </div>
    @endif
  @endunless

</div>

@vendor('blueimp-file-upload/js/jquery.iframe-transport.js')
@vendor('blueimp-file-upload/js/jquery.fileupload.js')