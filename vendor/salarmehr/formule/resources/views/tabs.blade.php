@vendor('jquery-ui/jquery-ui.min.js')
@push('scripts')
<script>
  $(function () {
    "use strict";
    $('.tabs').tabs();
  });
</script>
@endpush