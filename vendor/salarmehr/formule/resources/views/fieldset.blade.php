<fieldset
  {!! $field->cls() !!}
  {!! $field->attrs() !!}
  @if($field['ng-show']) ng-show="{!!$field['ng-show'] !!}" @endif
  @if($field['ng-hide']) ng-hide="{!!$field['ng-hide'] !!}" @endif
>
  @if($field->has('label'))
    <legend>
    {{$field->get('label','')}}
  </legend>
  @endif
  {!! $field->itemsHtml !!}
</fieldset>