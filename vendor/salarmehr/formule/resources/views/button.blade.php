<{{$tag}}

  @if($tag=='a')
  href="{{$field->href}}"
@if($field->newTab) target="_blank" @endif

@else
  type='{{$field->kind}}'
  value='{{$field->value}}'
@endif

@if($field->title) title='{{$field->title}}' @endif
{!! $field->cls() !!}
{!! $field->attrs() !!}
>

@if($field->icon||$field->emoji)
  <i class="fa {{$field->icon}}">{{$field->emoji}}</i>
@endif

{{$field->label}}

</{{$tag}}>