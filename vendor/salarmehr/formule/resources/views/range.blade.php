@extends('formule::field')
@section('input')
  <div class="formule-range">
    <div class="field">
      {!! $min !!}
    </div>
    <div class="formule-range-divider">{{__('formule::word.to')}}</div>
    <div class="field">
      {!! $max !!}
    </div>
  </div>
@endsection