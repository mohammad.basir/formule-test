@foreach($results as $row)
  <tr @if(isset($row['class'])) class="{{$row['class']}}" <?php unset($row['class']) ?> @endif>
    @foreach($row as $column)
      <td>{!! $column !!}</td>
    @endforeach
  </tr>
@endforeach