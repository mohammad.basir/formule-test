<table>
  <tr>
    @foreach($columns as $column)
      <th>{{$column->getHeader()}}</th>
    @endforeach
  </tr>
  @include("formule::grid.tbody")
</table>