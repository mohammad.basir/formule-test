@extends('formule::layout.page')
@section('gridInfo')
  <div class="formule-grid-topBar">
    <div class="formule-quickSearch formule-btnBar formule-field-control">
      <input type="search"
             name="find"
             value="{{request('find')}}"
             placeholder="{{$findColumnsTitles}}"
             title="{{$findColumnsTitles}}"
             class="formule-f"
      />
      <button type="submit" class="formule-btn-primary"><i class="fa fa-search"></i></button>
    </div>
    <div class="formule-btnBar">
      @if($params->addUrl)
        <a href="{{$params->addUrl}}" class="formule-btn item" title="{{__('formule::word.add')}}">
          <i class="plus icon"></i>
          {{isset($params->addTitle)?$params->addTitle:' '.__('formule::word.add') . ' '}}
        </a>
      @endif
      @if(isset($params->buttons))
        @foreach ($params->buttons as $button)
          {!! $button !!}
        @endforeach
      @endif
      @if($params->filterable!==false)
        <button type="button" class="formule-btn toggle-filterBox">
          <i class="fa fa-printer"></i>
          {{__('formule::word.report')}}
        </button>
      @endif
      @include('formule::button.back')
    </div>
  </div>

@endsection
@section('topBar')
@endsection
@section('filter')
  @if($msg)
    <div class="pageLevelMessage alert alert-warning">
      {!! $msg !!}
    </div>
  @endif
  <div class="filterBox hidden formule-form">
    <div class="filterBox-buttons">
      {!! $formats !!}
      <button type="submit" class="formule-btn" title="">
        <i class="fa fa-filter"></i>
        {{__('formule::word.apply')}}
      </button>
      <a href="{{$params->base}}" class="formule-btn" title="">
        <i class="fa fa-eraser"></i>
        {{__('formule::word.reset')}}
      </a>
    </div>
    {!! @$form !!}
  </div>
@endsection
@section('table')
  @if($params->filterable!==false)
    @yield('filter')
  @endif
  <div class="table-container">
    <table class="formule-table">
      <colgroup>
        @foreach($columns as $column)
          <col width="50"/>
        @endforeach
      </colgroup>
      <thead>
      <tr>
        @foreach($columns as $column)
          <th>{{$column->getHeader()}}</th>
        @endforeach
      </tr>
      <tr>
      {{--@foreach($columns as $column)--}}
      {{--<th>{!! $column['filter'] !!}</th>--}}
      {{--@endforeach--}}
      {{--</tr>--}}
      {{--<tr>--}}
      {{--@foreach($columns as $column)--}}
      {{--<th>{!! $column['sortField'] !!}</th>--}}
      {{--@endforeach--}}
      {{--</tr>--}}
      </thead>
      <tbody>
      @include('formule::grid.tbody')
      </tbody>
    </table>
  </div>
  <style type="text/css">
    .page-content {
      max-width: 100% !important;
    }
  </style>
@endsection

@section('content')
  <div class="formule-grid">
    @if($mode=='query')
      @yield('gridInfo')
    @endif
    @yield('table')
  </div>
  {{--{{count($results)}}از {{$count}}--}}
  @vendor ('jqDoubleScroll/jquery.doubleScroll.js')
  @js('vendor/formule/js/grid.js')
@overwrite