@extends('formule::field')
@section('input')
  @if($field->type=='captcha')
    {!! captcha_img('flat') !!}
  @endif
  <input
    value='{{$field->value}}'
    {!! $field->cls() !!}
    {!! $field->attrs() !!}
    @if($field->type=='star')
      type='number'
      min='{{$field->min ?: 0}}'
      max='{{$field->max ?: 5}}'
    @else
      type='{{$field->type}}'
    @endif
  />
@endsection