<div class="tabs">
  <ul class="" role="tablist">
    @foreach ($field['items'] as $i => $tab)
      <li role="presentation"
          {!! $tab->cls('item') !!}
          {!! $tab->attrs() !!}
          data-tab="{{$tab->id}}"
          @if($tab['ng-show']) ng-show="{!!$tab['ng-show'] !!}" @endif
          @if($tab['ng-hide']) ng-hide="{!!$tab['ng-hide'] !!}" @endif
      >
        <a role="tab" data-toggle="tab" href="#tab-{{$tab->id}}">
          {{$tab['label']}}
        </a>
      </li>
    @endforeach
  </ul>

  <div class="tab-content" @if($field->id) id="{{$field->id}} @endif">
    @foreach ($field['items'] as $i => $tab)
      <div data-tab="{{$tab->id}}" role="tabpanel" {!! $tab->cls("tab-pane") !!} id="tab-{{$tab->id}}">
        {{--@dd($tab)--}}
        {!! $tab['renderedHtml'] !!}
      </div>
    @endforeach
  </div>
</div>