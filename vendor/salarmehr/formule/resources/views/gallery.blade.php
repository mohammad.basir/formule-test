@if(count($files))
  {{--<link rel="stylesheet" href="{{bower('unitegallery/dist/css/unite-gallery.css')}}"/>--}}
@vendor('unitegallery/dist/js/unitegallery.min.js')
@vendor('unitegallery/dist/themes/compact/ug-theme-compact.js')


  @if(isset($galleryHeader))
    <h2>{{$galleryHeader}}</h2>
  @endif

  <div id="{{$id}}-gallery" style="margin: 0 auto;">
    @foreach($files as $file)
      @if($file->isImage())
        <img alt="{{$file->name}}"
             src="{{$file->src('milli')}}"
             data-image="{{$file->src}}"
             data-description="{{$file->details}}">
      @elseif($file->isVideo())
        <img alt="{{$file->name}}"
             src="{{$file->src}}"
             data-type="html5video"
             data-video="{{$file->src}}"
             data-description="{{$file->details}}"
             data-image="{{asset('assets/img/placeholder-video.png')}}"
             data-video{{$file->extension}}="{{$file->src}}"
        >
      @endif
    @endforeach
  </div>

  @push('scripts')
  <script>
    $.ready(function () {
      jQuery("#{{$id}}-gallery").unitegallery({
//      theme_panel_position: "botton",
        carousel_autoplay_direction: 'left',
        carousel_autoplay: true,				//true,false - autoplay of the carousel on start
        gallery_height: 400,
        gallery_width: 800,
      });

    });
  </script>
  @endpush
@endif