@extends('formule::field')

@section('input')
  <div class="formule-pick">
    <input
      value='{{$field->value}}'
      {!! $field->attrs() !!}
      {!! $field->cls() !!}
    >
    <span class="formule-pick-btn formule-btn" id="{{$field->name}}-btn">
            <i class="fa {{$field->icon}}"></i>
    </span>
  </div>
  @push('scripts')
    <script>
        $.ready(function () {
            Calendar.setup({
                inputField: "{{$field->name}}",   // id of the input field
                button: "{{$field->name}}-btn",   // trigger for the calendar (button ID)
                ifFormat: "{{$field->format}}",       // format of the input field
                daFormat: "{{$field->format}}",       // format of the input field
                showsTime: {{$field->timeCfg}},
                timeFormat: "24",
                dateType: @if (region('IR'))'jalali'@else'gregorian' @endif,
                weekNumbers: false
            });
        });
    </script>
  @endpush
@endsection
