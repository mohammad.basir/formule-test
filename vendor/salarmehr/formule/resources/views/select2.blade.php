@vendor('select2/dist/js/select2.full.min.js')
@vendor('select2/dist/js/i18n/fa.js')
@push('scripts')
<script>
  $(function () {
    var entity = $("[name='entity']").val();
    $('select.rich').select2({width: 300});
    $('select[multiple]').select2({width: '70%'});
    $('select.user').select2({
      ajax: {
        data: function (param) {
          return {q: param.term, except: this.attr('except')}
        },
        dataType: 'json',
        delay: 250,
        lang: 'fa',
        cache: true,
        width: 300
      }
    });
    $('select.entity').select2({
      ajax: {
        url: "/entity/combo",
        data: function (param) {
          return {q: param.term, except: this.attr('except')}
        },
        dataType: 'json',
        delay: 250,
        lang: 'fa',
        cache: true,
        width: 300
      }
    });
    $('select.program').select2({
      ajax: {
        url: "/program/combo",
        data: function (param) {
          return {q: param.term, except: this.attr('except')}
        },
        dataType: 'json',
        delay: 250,
        lang: 'fa',
        cache: true, width: 300
      }
    });
    $('select.page').select2({
      ajax: {
        url: "/page/combo",
        data: function (param) {
          return {q: param.term, except: this.attr('except'), eid: this.attr('eid')}
        },
        dataType: 'json',
        delay: 250,
        lang: 'fa',
        cache: true, width: 300
      }

    });
    $('select.location').select2({
      ajax: {
        url: "/location/combo",
        data: function (param) {
          return {q: param.term, except: this.attr('except')}
        },
        dataType: 'json',
        delay: 250,
        lang: 'fa',
        cache: true, width: 300
      }
  })});
</script>
@endpush