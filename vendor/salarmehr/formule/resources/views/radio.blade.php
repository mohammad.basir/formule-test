@extends('formule::field')

@section('input')
  @foreach ($field['items'] as $value => $label)
    <label class='{{$field->type}}-inline' {{$field->ng}} >
      {{$label}}
      @if($field->type=='checkbox' && !\f\arr::isAssoc($field->items))
        <input type='hidden' value='' name='{{$field->name}}'>
      @endif
      <input
        type='{{$field->type}}'
        @if($field->type=='checkbox')
          name='{{$field->name.'[]'}}'
        @else
        name='{{$field->name}}'
        @endif
        value='{{$value}}'
        {!! $field->cls() !!}
        {!! $field->attrs() !!}
        @if($field->type=='checkbox')
        @if(in_array((string)$value , (array)$field->value)) checked @endif
        @else
        @if((string)$value === (string)$field->value) checked @endif
        @endif
      />
    </label>
  @endforeach
@endsection