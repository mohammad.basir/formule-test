@extends('formule::field')

@section('input')
  <label class='{{$field->type}}' {{$field->ng}} >
    <span class="formule-control-static">
      <input type='hidden' value='0' name='{{$field->name}}'>
      <input
        type='checkbox'
        value='1'
        {!! $field->cls() !!}
        {!! $field->attrs() !!}
        @if((string)$field->value === '1') checked @endif
      />
      {{$field->label}}
    </span>
  </label>
@endsection