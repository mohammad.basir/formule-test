@extends('formule::field')

@section('input')

  <span class="formule-control-static"
    {!! $field->attrs() !!}
  >
    @if(isset($field->richValue))
      {!! $field->richValue !!}
    @else
      {!! $field->value !!}
    @endif
  </span>
  @if($field->hiddenField)
    {!! $field->hiddenField !!}
  @endif
@endsection