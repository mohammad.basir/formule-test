<?php
// separating media and non-media files
$documents = $media = [];
foreach ($files as $file) {
  if ($file->isMedia()) {
    $media[] = $file;
  }
  else {
    $documents[] = $file;
  }
}
if (!isset($id)) $id = 'files'
?>
@include('formule::attach',['files'=>$documents])
@include('formule::gallery',['files'=>$media,'id'=>$id])