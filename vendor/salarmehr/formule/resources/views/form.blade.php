@extends($display)
@section('topBar')
  {{--for future use--}}
@endsection

@section('bottomBar')
  <div class="btn-group float-far" role="group">
    @if($method=='put' && $removable && $editable!='none')
      @include('formule::button.delete')
    @endif
  </div>
@endsection

@section('content')

  @include('formule::errors')
  @if($method != 'post')
    <input type="hidden" name="_method" value="{{$method}}">
  @endif
  {{csrf_field()}}
  {!! $html  !!}
  <div class="formule-btns">

    @if($buttons)
      @foreach( itemize($buttons) as $button)
        {!! field($button) !!}
      @endforeach
    @endif

    @include('formule::button.save')
    @if($backUrl)
      @include('formule::button.back',['backUrl'=>$backUrl])
    @endif
    @if($readonly && $printable)
      <a class="formule-btn" href="{{request()->url()."?print=1"}}" target="_blank">
        <i class="fa fa-print"></i>&nbsp;
        {{__('formule::word.print')}}
      </a>
    @endif
    @if($editable)
      <a class="formule-btn" href="{{$form->editUrl}}">
        <i class="fa fa-pencil"></i>&nbsp;
        {{__('formule::word.edit')}}
      </a>
    @endif
  </div>
@overwrite