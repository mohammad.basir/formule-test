<!DOCTYPE html>
<html lang="fa" dir="rtl" ng-app class="no-js">
<head>
  {!! SEO::generate() !!}
  @stack('head')
  <link rel="stylesheet" href="{{bower('jquery-ui/themes/base/all.css')}}"/>
</head>
<body>
@yield('content')
</body>

@stack('vendor')
@stack('scripts')

@if(app('env')=='local')
  @if(request()->exists('outline'))
    <style>
      * {
        outline: 1px dotted rgba(1, 1, 1, .1);
      }
    </style>
  @endif
  @if(request()->exists('vvv'))
    <?php dump(get_defined_vars()); ?>
  @endif
@endif
</html>

