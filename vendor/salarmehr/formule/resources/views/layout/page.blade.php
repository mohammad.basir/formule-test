{{--
Thie layout is used for pages that only contain a form or a grid (when using Form::show() or Grid::show() methods.
In other cases please use Form::render and Grid::render methodes to get the HTML of your form or grid.
--}}

@extends($layout ?? config('formule.layout','formule::layout/layout'))
@includeWhen(!@$formuleAssetsIncluded,'formule::layout.assets')

@section('content')
  @include('formule::layout.inline')
@overwrite