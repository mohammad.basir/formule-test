<div class="formule-formule {{$cls}}">
  <form
    @if(strtolower($method)=='get') method="get" @else method="post" @endif
  novalidate
    action="{{$action}}"
    id="form"
    class="formule-form"
  >
    <h3 class="formule-header">
      {!! $title !!}
    </h3>

    <div class="formule-topBar">
      @yield('topBar')
    </div>
    <div class="formule-content">
      @yield('content')
    </div>
  </form>

  <div class="formule-bottomBar">
    @yield('bottomBar')
  </div>
</div>