@push('scripts')
  @vendor('JalaliJSCalendar/calendar.js')
  @vendor('JalaliJSCalendar/calendar-setup.js')
  @vendor('JalaliJSCalendar/jalali.js')
  @vendor('JalaliJSCalendar/lang/calendar-fa.js')
  @vendor('jquery/dist/jquery.js')
  @vendor('jquery-ui/jquery-ui.min.js')
  @vendor('jqDoubleScroll/jquery.doubleScroll.js')
  @vendor('ckeditor/ckeditor.js')
  @vendor('ckeditor/adapters/jquery.js')
  @vendor('angular/angular.js')
  @js('vendor/formule/js/form.js')


  @if(isset($method)&&strtolower($method)=='get')
    <script>
        var myForm = document.getElementById('form');
        myForm.onsubmit = function () {
            var allInputs = myForm.getElementsByTagName('input');
            var input, i;
            for (i = 0; input = allInputs[i]; i++) {
                if (input.getAttribute('name') && !input.value) {
                    input.setAttribute('name', '');
                }
            }
        };
    </script>
  @endif

  @include('formule::tabs')
  @include('formule::select2')
@endpush

<?php $formuleAssetsIncluded=true ?>