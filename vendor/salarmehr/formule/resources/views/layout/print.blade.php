<html>
<head>
  <link rel="stylesheet" href="{{ asset('assets/css/_base.css') }}"/>
  <link rel="stylesheet" href="{{url('views/layout/form/form.css')}}"/>
</head>
<body style="direction: {{direction()}}; width: 600px; margin:auto;">
<form class="formule-form">
  @yield('content')
</form>
@push('scripts')
  <script>
      $(document).ready(function () {
          $('[role="tablist"]').hide();
          window.print();
      });
  </script>
@endpush
</body>
</html>
