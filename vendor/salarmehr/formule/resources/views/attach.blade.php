@if(count($files))
  <hr/>
  <h3>
    {{$filesTitle ??__('formule::word.attachment') }}
  </h3>
  <div class="row">
    <table class="files-table">
      <thead>
      <tr>
        <td>#</td>
        <td>{{__('formule::word.name')}}</td>
        <td>{{__('formule::word.description')}}</td>
        <td>{{__('formule::word.size')}}</td>
        <td>{{__('formule::word.cDate')}}</td>
      </tr>
      </thead>
      <tbody>
      @foreach($files as $index=>$file)
        @if(!$file->isImage())
          <tr id="table-{{$index}}">
            <td>{{++$index}}</td>
            <td>
              <a href="{{$file->src}}" target="_blank">
                <i class="fa fa-file-o"></i>
                <span id="name-{{$index}}">{{$file['name']}}</span>
              </a>
            </td>
            <td>
              {{$file['details']}}
            </td>
            <td>
              {{\f\fs::fileSize($file->size)}}
            </td>
            <td>
              {!!solarTag($file->cDate)!!}
            </td>
          </tr>

        @endif
      @endforeach
      </tbody>
    </table>
  </div>
  {{--<div class="row">--}}
  {{--@foreach($files as $index=>$file)--}}
  {{--@if($file->imageSize())--}}
  {{--<div class="col-xs-6 col-md-3">--}}
  {{--<a href="{{$file->src('download')}}" target="_blank" class="thumbnail">--}}
  {{--<img src="{{$file->src('milli')}}" class="milli"--}}
  {{--alt="{{$file['name']}}">--}}
  {{--</a>--}}
  {{--</div>--}}
  {{--@endif--}}
  {{--@endforeach--}}
  {{--</div>--}}
@endif
