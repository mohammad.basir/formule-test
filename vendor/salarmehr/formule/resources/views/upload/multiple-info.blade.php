<tr>
  <td>
    {{is_integer($i) ?$i+1:'نو'}}
  </td>
  <td>
    @if($file->isImage())
      <img class="file-thumbnail" src="{{$file->src('micro')}}">
    @else
      <i class="fa fa-file"></i>
    @endif
  </td>
  <td>
    <a href="{{$file->src}}" target="_blank">
      <i class="fa fa-paperclip"></i>
      <span id="name-{{$i}}">{{$file['name']}}</span>
    </a>
  </td>
  <td>
    <input type="hidden" name="files[{{$file->id}}][field]" value="{{$field}}"/>
    <input type="text" name="files[{{$file->id}}][details]" value="{{$file->details}}" placeholder="توضیح"/>
  </td>
  <td>
    {{\f\num::fileSize($file->size)}}
  </td>
  <td>
    {!!solarTag($file->cDate)!!}
  </td>
  <td>
    {!!userTag($file->user)!!}
  </td>
  <td>
    <button type="button" class="btn btn-danger delete file-delete-btn">
      <i class="fa fa-trash"></i>
      <span>{{__('formule::word.delete')}}</span>
    </button>
  </td>
</tr>