@extends('formule::field')
@section('input')
    <span class="{{$field->name}}-upload-box singleFileUpload">
    <div class="singleFileUpload-info">
      @if($field->file)
            @include('formule::upload.single-info',$field->all())
        @endif
    </div>
    <span class="formule-btn fileinput-button" style="display:{{$field->file ?'none':'inline-block'}}">
        <i class="fa fa-plus"></i>
        <span>{{__('formule::word.upload')}}</span>
        <input
                class='fileupload files {{$field->name}}'
                data-name="files[]"
                type="file"
        >
    </span>

        <!-- The global progress state -->
        {{--<span class="col-lg-5 fileupload-progress fade">--}}
        {{--<!-- The global progress bar -->--}}
        {{--<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">--}}
        {{--<div class="progress-bar progress-bar-success" style="width:0%;"></div>--}}
        {{--</div>--}}
        {{--<!-- The extended global progress state -->--}}
        {{--<div class="progress-extended">&nbsp;</div>--}}
</span>
    @if(isset($help))
        <div class="help">
            {{$field->help}}
        </div>
    @endif
@endsection
@push('scripts')
    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    @vendor('blueimp-file-upload/js/vendor/jquery.ui.widget.js')
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    @vendor('blueimp-file-upload/js/jquery.iframe-transport.js')
    <!-- The basic File Upload plugin -->
    @vendor('blueimp-file-upload/js/jquery.fileupload.js')

    <script>
        $(function () {
            var namespace = $('.{{javascriptName($field->name)}}-upload-box');
            namespace.on('click', '.file-delete-btn', function (e) {
                namespace.children('.singleFileUpload-info').html('');
                namespace.children('.fileinput-button').show();
            });
            namespace.find('input.fileupload').fileupload({
                url: "/file/attach?single=1&field={{$field->name}}&simple={{$field->simple}}&fileable={{$field->fileable}}",
                imageMaxWidth: 1800,
                imageMaxHeight: 1800,
                maxFileSize:cfg('fs.maxFileSize'),
                done: function (e, data) {
                    namespace.children('.fileinput-button').hide();
                    namespace.children('.singleFileUpload-info').html(data.result.html);
                },
                complete: function (result, data) {
                },
                fail: function (e, data) {
                    alert('⚠' + data.jqXHR.responseText);
                }
            });
        });
    </script>
@endpush