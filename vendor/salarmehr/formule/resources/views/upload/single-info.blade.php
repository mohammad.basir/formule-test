@if(!$field->readonly)
  @if($field->simple)
    <input type="hidden" name="{{$field->name ?? $field->field}}" value="{{$field->file->id}}"/>
  @else
    <input type="hidden" name="files[{{$field->file['id']}}][field]" value="{{$field->name ?: $field->field}}"/>
  @endif
@endif
@if($field->image)
  <img class='fileInfo-thumbnail' src='{{$field->file->src('nano')}}'/>
@endif
<div class="singleFileUpload-infoColumn">
  <a href='{{$field->file->src("download")}}' target="_blank">
    <i class="fa fa-paperclip"></i>
    {{$field->file['name']}}
  </a>
  <div>
:{{__('formule::word.productName')}}
    {{
    \f\fs::fileSize($field->file->size)}}
  </div>
  <div>
    <button type="button"
            class="btn btn-danger delete file-delete-btn">
{{__('formule::word.delete')}}
    </button>
  </div>
</div>