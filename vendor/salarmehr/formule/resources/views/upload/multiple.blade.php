<div class="{{$field}}-upload-box">
  <div class="row fileupload-buttonbar">
    @if(isset($help))
      <div class="help">
        {{$help}}
      </div>
    @endif
    <div class="col-lg-7">
                            <span class="formule-btn fileinput-button">
                        <i class="fa fa-plus"></i>
                        <span>{{__('formule::word.upload')}}</span>
                                    <input data-url="/file/attach?field={{$field}}" class='fileupload files {{$field}}'
                                           type="file"
                                           data-name="files[]"
                                           multiple>

                    </span>
    </div>
    <!-- The global progress state -->
    <div class="col-lg-5 fileupload-progress fade">
      <!-- The global progress bar -->
      <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
      </div>
      <!-- The extended global progress state -->
      <div class="progress-extended">&nbsp;</div>
    </div>
  </div>
  <table class="files-table">
    <thead>
    <tr>
      <td>#</td>
      <td><i class="fa fa-file"></i></td>
      <td>{{__('formule::word.name')}}</td>
      <td>{{__('formule::word.description')}}</td>
      <td>{{__('formule::word.size')}}</td>
      <td>{{__('formule::word.cDate')}}</td>
      <td>{{__('formule::word.creator')}}</td>
      <td>{{__('formule::word.delete')}}</td>
    </tr>
    </thead>
    <tbody>
    @if($files)
      @foreach($files as $i=>$file)
        @include('formule::upload.multiple-info',['field'=>$field,'i'=>$i])
      @endforeach
    @endif
    </tbody>
  </table>
</div>
@push('scripts')
  <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
  <script src="{{bower('blueimp-file-upload/js/vendor/jquery.ui.widget.js')}}"></script>
  <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
  <script src="{{bower('blueimp-file-upload/js/jquery.iframe-transport.js')}}"></script>
  <!-- The basic File Upload plugin -->
  <script src="{{bower('blueimp-file-upload/js/jquery.fileupload.js')}}"></script>
  <!-- The File Upload processing plugin -->
  {{--<script src="{{bower('blueimp-file-upload/js/jquery.fileupload-process.js')}}"></script>--}}
  <!-- The File Upload image preview & resize plugin -->
  {{--<script src="{{bower('blueimp-file-upload/js/jquery.fileupload-image.js')}}"></script>--}}
  <!-- The File Upload validation plugin -->
  {{--<script src="{{bower('blueimp-file-upload/js/jquery.fileupload-validate.js')}}"></script>--}}
  <!-- The File Upload user interface plugin -->
  {{--<script src="{{bower('blueimp-file-upload/js/jquery.fileupload-ui.js')}}"></script>--}}
  <script>
      $(function () {
          var namespace = '.{{$field}}-upload-box';
          $(document).on('click', namespace + ' .file-delete-btn', function (e) {
              $(e.target).closest('tr').remove();
          });

          $(namespace).find('.fileupload').fileupload({
              imageMaxWidth: 1800,
              imageMaxHeight: 1800,
              maxFileSize:{{$maxSize}},
              done: function (e, data) {
                  var parent = $(e.target).closest(namespace);
                  parent.find('tbody').append(data.result.html);
              },
              complete: function (result, data) {
              },
              fail: function (e, data) {
                  alert('⚠️ ' + data.jqXHR.responseText);
              }
          });
      });
  </script>
@endpush