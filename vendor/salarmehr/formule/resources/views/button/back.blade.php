<a
  class="formule-btn {{@$_cls}}"
  href="{{coalesce(@$backUrl,@$params->backUrl, @$_url , URL::previous())}}">
  {{$backTitle ?? __('formule::word.back')}}
</a>
