@if(!$readonly && $submitable)
  <button type="submit" {{$readonly}} class="formule-btn-primary {{$_cls ?? ''}}">

    @if(!$submitTitle || $submitIcon)
      <i class="fa {{$submitIcon ?? 'fa-save'}}"> </i>
    @endif

    &nbsp;{{$submitTitle ?? __('formule::word.submit')}}
  </button>
@endif