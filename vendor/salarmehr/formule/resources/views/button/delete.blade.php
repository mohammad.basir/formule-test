<form action="{{$action}}"
      onsubmit="return confirm('{{__('formule::msg.delete')}}');"
      method="POST"
>
  {{csrf_field()}}
  <input type="hidden" name="_method" value="delete">
  <button type="submit" class="formule-btn-danger"><i class="fa fa-trash"></i>
    @if(@$iconOnly !==true)
      {{__('formule::word.delete')}}
    @endif
  </button>
</form>
{{--<script >--}}
{{--$('.button')$.ajax({--}}
{{--url: "{{$action}}",--}}
{{--type: 'DELETE'--}}
{{--}--}}
{{--);--}}
{{--</script>--}}
