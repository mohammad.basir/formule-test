<a href="{{$backUrl}}"
   class="formule-btn-primary {{$_cls ?? ''}}">
    <i class="fa fa-pencil"></i>
    {{$editTitle ?? trans('formule::word.edit')}}
</a>