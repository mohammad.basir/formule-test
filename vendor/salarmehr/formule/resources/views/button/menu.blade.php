<div class="list-group-item">

  <a href="{{$url}}" title="" class="item">
    @if(isset($icon))
      <i class="fa {{$icon}}"></i>
    @endif
    <span> {{$text}} </span>
  </a>

  <div class="float-far btn-group">
    @if(isset($create))
      <a href="{{$url}}/create" class="add-btn" title="{{__('formule::word.add')}}">
        <i class="fa fa-plus-circle"></i>
      </a>
    @endif
    @if(isset($count))
      <span class="badge" title="{{isset($title)?$title:""}}">
                    <span>{{$count}}</span>
            </span>
    @endif

  </div>
</div>