@include('formule::tabset')
<a onclick="previousPage()" class="formule-btn">گام قبل</a>
<a onclick="nextPage()" class="formule-btn">گام بعد</a>
@push('scripts')
  <script src="{{url('views/layout/form/wizard.js') }}"></script>
@endpush