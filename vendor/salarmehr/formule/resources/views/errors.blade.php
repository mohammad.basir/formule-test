@if (count($errors))
  <ul class="alert alert-danger formule-errorBox">
    @foreach ($errors as $fieldErrors)
      @foreach ($fieldErrors as $error)
        @if (count($errors) == 1)
          {{ $error }}
        @else
          <li>{{ $error }}</li>
        @endif
      @endforeach
    @endforeach
  </ul>
@endif

@push('scripts')
  <script>
      $(function () {
        @foreach (['danger', 'warning', 'success', 'info'] as $type)
        @foreach((array) session($type) as $msg)
        msg('{{$msg}}', '{{$type}}');
        @endforeach
        {{session()->forget($type)}}
        @endforeach
      });
  </script>
@endpush