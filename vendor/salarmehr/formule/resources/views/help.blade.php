@extends('formule::field')

@section('input')
    @if($field->html)
      <div @if($field->id) id="{{$field->id}}" @endif class="alert alert-info" role="alert">
            {!! $field->html !!}
        </div>
    @endif
@endsection