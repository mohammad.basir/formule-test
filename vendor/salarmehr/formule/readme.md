Formule v0.2
=============

Installation
------------
1. update your composer repositories as:  
   ``` 
     "prefer-stable": true,
      "repositories": [
        {
          "url": "git@gitlab.com:ahoora/tools/formule.git",
          "type": "vcs"
        }
      ],
      
      "require": {
        ...
        "salarmehr/formule": "dev-master"
      },
   ```
2. `composer update`
3. if using Laravel < 5.5 add `\Formule\Laravel\FormuleServiceProvider::class` to `providers` array of `config\app.php` file. In Laravel 5.5 it will be done automatically after running composer update. 
4. run `php artisan vendor:publish  --provider="Formule\Laravel\FormuleServiceProvider"` to add public vendor files and
   formule.conf file 
5. review and update `config/formule.
6. the package adds `bower.json` file for new project. For existing project please add all 
   packages in `vendor/salarmehr/formule/composer.json` to your current package list
7. Some Formule element need third party styles. 
   Make sure these css/less files are loaded somewhere if they are used in your app.
   
  - select2/dist/css/select2.css
  - blueimp-file-upload/css/jquery.fileupload.css
  - JalaliJSCalendar/skins/calendar-system.css
  - unitegallery/dist/css/unite-gallery.css
  - font-awesome/less/font-awesome.less

Updating package
----------------
1. `composer update`
2. `php artisan vendor:publish  --tag=assets --force --provider="Formule\Laravel\FormuleServiceProvider"` to overwrite all css and js files  


Custom Form Controls
---------------
- For a simple pre-configured control register it in the types section of `config/formule.php`
- To create a new control create a class as `\App\Formule\Foo` that will be identified as `type='foo'`
* see: `\Formule\Form\Form::makeField` implementation

View level requirements
-----------------------
1. Page level messages usage example:

```
@if (session('messages') && is_array(session('messages')))
  <div class="pageLevelMessage alert alert-{{ @session('messages')[0]['type']}}">
    @foreach (session('messages') as $msg)
      {!! @$msg['text'] !!}
    @endforeach
  </div>
@endif
```

2. Add `\Locale::setDefault(app()->getLocale());` to your `AppServiceProvider@boot` method.


See samples
-----------
Build `build.sh `
Run `php -S  localhost:4444 -t  ./docs/samples`

or 
```
cd tests
docker-compose up
```

Develop
-------
Create a PHPStorm watcher to run `sync.bash` on the files that are mentioned in this file. 