<?php

namespace App\Http\Controllers;

use App\Report;
use Formule\Grid\Grid;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index()
    {
        $forms =
            [
                [
                'type' => 'fieldset',
                'items' => [
                    [
                        'id' => 'date',
                        'label' => 'تاریخ',
                        'type' => 'datetime',
                        'validator' => 'required',
                    ],
                    [
                        'id' => 'hours',
                        'label' => 'ساعت ',
                        'type' => 'number',
                        'validator' => 'required',
                    ],
                    [
                        'id' => 'description',
                        'label' => 'توضیحات',
                        'hint' => 'توضیحات',
                    ],
                    [
                        'id' => 'is_holiday',
                        'label' => 'تعطیل رسمی؟',
                        'type' => 'boolean',
                    ],
                ],

            ]
            ];

        return view('test', compact('forms'));
    }
}
