composer config repositories.formule vcs git@gitlab.com:ahoora/tools/formule.git
chmod -R o+w storage
chmod -R o+w bootstrap/cache
composer require salarmehr/formule:dev-master@dev -W
cp ../.env.example .env
php artisan key:generate
php artisan vendor:publish  --provider="Formule\Laravel\FormuleServiceProvider"